import Foundation

protocol SelectCategoryViewModelDelegate{

    func didGetCategoriesSuccess(_ success:[GetCategory])
    
    func didGetCategoriesError(_ error:String)
    
}

protocol SelectSubCategoryViewModelDelegate{
    
    func didSaveCategory(_ successMessage:String)
    
    func didGetSubCategoriesSuccess(_ success:GetSubCategory)
    
    func didGetSubCategoriesError(_ error:String)
}


protocol SelectCategoryViewModelProtocol{
    
    var catDelegate:SelectCategoryViewModelDelegate?{get set}
    
    func saveCategory(_ categoryCod:Int ,_ subCategoryCod:Int,
                      _ categoryName:String ,_ subCategoryName:String,
                      _ categoryImage:String)
    
    func getCategory()
    
    func getSubCategory(_ catCod:Int)
}

class SelectCategoryViewModel:SelectCategoryViewModelProtocol{
    
    var catDelegate: SelectCategoryViewModelDelegate?
    var subCatDelegate: SelectSubCategoryViewModelDelegate?
    var service:SelectCategoryServiceProtocol
    
    init(_ service:SelectCategoryServiceProtocol) {
        self.service = service
    }
    
    func saveCategory(_ categoryCod:Int ,_ subCategoryCod:Int,
                      _ categoryName:String ,_ subCategoryName:String,
                      _ categoryImage:String) {
        service.saveCategory(categoryCod,subCategoryCod, categoryName, subCategoryName,categoryImage) { (sucess) in
            self.subCatDelegate?.didSaveCategory(sucess)
        }
    }
    
    func getCategory() {
        guard let validToken = UserDefaults.standard.object(forKey:"userToken") else {catDelegate?.didGetCategoriesError("erro ao recuperar o token"); return}
        let token = validToken as! String
        service.getCatServ(token, { (categories) in
            self.catDelegate?.didGetCategoriesSuccess(categories)
        }) { (err) in
            self.catDelegate?.didGetCategoriesError(err)
        }
    }
    
    func getSubCategory(_ catCod: Int) {
        guard let validToken = UserDefaults.standard.object(forKey:"userToken") else {catDelegate?.didGetCategoriesError("erro ao recuperar o token"); return}
        let token = validToken as! String
        service.getSubCatServ(catCod, token, { (subCategories) in
            self.subCatDelegate?.didGetSubCategoriesSuccess(subCategories)
        }) { (err) in
            self.subCatDelegate?.didGetSubCategoriesError(err)
        }
    }
}
