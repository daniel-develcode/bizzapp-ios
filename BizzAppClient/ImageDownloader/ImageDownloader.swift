import Foundation
import Alamofire
import AlamofireImage
import UIKit

protocol ImageDownloaderProtocol {
    typealias OnGetImageSuccess = ((UIImage,String) -> Void)
    typealias OnGetImageError = ((String) -> Void)
    func downloadImage(_ url:String,
                       _ success:@escaping OnGetImageSuccess,
                       _ error:@escaping OnGetImageError)
}

class ImageDownloader:ImageDownloaderProtocol{
    var cache:[String:UIImage] = [:]
    
    func downloadImage(_ url:String,
                       _ success:@escaping OnGetImageSuccess,
                       _ error:@escaping OnGetImageError) {
        
        let cachedImage = checkCache(url)
        if (cachedImage != nil){
            success(cachedImage!,url)
        }else{
            Alamofire.request(url).responseImage { response in
                if(response.error == nil){
                    var image:UIImage
                    image = UIImage(data: response.data!, scale:1)!
                    self.addImageToCache(url, image)
                    success(image,url)
                }else{
                    error((response.error?.localizedDescription)!)
                }
            }
        }
    }
    
     func checkCache(_ url:String) -> UIImage?{
        return cache[url]
    }
    
    func addImageToCache(_ url:String,_ image:UIImage){
        cache[url] = image
    }
}
