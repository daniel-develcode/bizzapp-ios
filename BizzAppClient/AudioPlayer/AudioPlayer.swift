import Foundation
import UIKit
import AVFoundation

protocol AudioPlayerProtocol{
    func streamAudio(_ stringUrl:String)
    func playPause(_ playPauseBtn:UIButton, _ slider: UISlider?)
    func availableDuration() -> CMTime
}

class AudioPlayer:AudioPlayerProtocol {
    private var avPlayer: AVPlayer!
    private var isPaused:Bool = true
    private var isFinished:Bool = false
    private var stringUrl = ""
    private var playPauseBtn:UIButton!
    private var slider: UISlider?
    
    func streamAudio(_ stringUrl: String) {
        if(stringUrl != self.stringUrl){
            if(playPauseBtn != nil){
                avPlayer.pause()
                avPlayer.seek(to: CMTime.zero)
                playPauseBtn.setImage(#imageLiteral(resourceName: "baseline-play_arrow-24px"), for: .normal)
            }
            if(slider != nil){
                slider?.value = 0
            }
            let url = URL(string: stringUrl)!
            avPlayer = AVPlayer(url: url)
        }
    }
    
    func playPause(_ playPauseBtn:UIButton, _ slider: UISlider?){
        self.playPauseBtn = playPauseBtn
        self.slider = slider
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer.currentItem)
        if(isFinished){
            avPlayer.seek(to: CMTime.zero)
            isFinished = false
        }
        if(isPaused) {
            avPlayer.play()
            avPlayer.volume = 1
            playPauseBtn.setImage(#imageLiteral(resourceName: "baseline-pause-24px 2"), for: .normal)
        }else{
            avPlayer.pause()
             playPauseBtn.setImage(#imageLiteral(resourceName: "baseline-play_arrow-24px"), for: .normal)
        }
        
        if(slider != nil){
            avPlayer.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: nil) { (time) in
                if (self.avPlayer.rate > 0.0){
                    let duration = self.avPlayer.currentItem?.asset.duration
                    let totalSeconds = duration?.seconds
                    let percent = ((time.seconds*100)/(totalSeconds!))*0.01
                    slider!.value = Float(percent)
                }
            }
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
         playPauseBtn.setImage(#imageLiteral(resourceName: "baseline-play_arrow-24px"), for: .normal)
    }
    
    func availableDuration() -> CMTime
    {
        if let range = self.avPlayer?.currentItem?.loadedTimeRanges.first {
            return CMTimeRangeGetEnd(range.timeRangeValue)
        }
        return CMTime.zero
    }
}
