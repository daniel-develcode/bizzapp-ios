import Foundation
import UIKit
import Firebase
import FirebaseAuth

protocol UploadAudioProtocol {
    func saveRecordedMessage(_ url: URL,
                             _ reference: String)
    
    var delegate:UploadAudioDelegate{get set}
}

protocol UploadAudioDelegate {
    func didUploadAudioSuccess(_ stringURL:String,_ reference:String)
    func didUploadAudioError(_ Error:String,_ reference:String)
    func didUploadAudioProgress(_ progress:Double,_ audioReference:String)
}

class UploadAudioService:UploadAudioProtocol{
    var delegate: UploadAudioDelegate
    
    init (_ delegate: UploadAudioDelegate){
        self.delegate = delegate
    }
    
    func saveRecordedMessage(_ url: URL,
                             _ reference: String) {
        
        let data = try? Data(contentsOf: url)
        var riversRef:StorageReference?
        riversRef = Storage.storage().reference().child(reference)
        let metaData = StorageMetadata()
        metaData.contentType = "video/mp4"
        
        let uploadTask = riversRef!.putData(data!, metadata: metaData) { (metadata, error) in
            guard metadata != nil else {
                self.delegate.didUploadAudioError("Ocorreu um erro ao fazer upload", reference)
                return
            }
            riversRef!.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    self.delegate.didUploadAudioError("Ocorreu um erro ao fazer upload", reference)
                    return
                }
                self.delegate.didUploadAudioSuccess(downloadURL.absoluteString,reference)
            }
        }
        uploadTask.observe(.progress) { snapshot in
            self.delegate.didUploadAudioProgress((snapshot.progress?.fractionCompleted)!,reference)
        }
    }
}
