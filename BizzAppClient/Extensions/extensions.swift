import Foundation
import UIKit
import AlamofireImage
import AVKit

extension Double {
    
    func asDistance(unit: String) -> String {
        return "\(String(format: "0.2f", self / 1000))\(unit)"
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
extension UITableView {
    
    func getFormattedSchedDate(_ date:String) -> String{
        let schedDayHour = date.components(separatedBy:"-")[2]
        let schedFormDay = schedDayHour.components(separatedBy: " ")[0]
        let schedFormYear = date.components(separatedBy:"-")[0]
        let schedFormMonth = getMonth(date.components(separatedBy:"-")[1])
        let schedFormHour = schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[0] + ":" + schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[1]
        return schedFormDay + " de " + schedFormMonth + " de " + schedFormYear + " - " + schedFormHour
    }
    
    func getFormattedDate(_ date:String) -> String{
        let schedDayHour = date.components(separatedBy:"-")[2]
        let schedFormDay = schedDayHour.components(separatedBy: " ")[0]
        let schedFormYear = date.components(separatedBy:"-")[0]
        let schedFormMonth = date.components(separatedBy:"-")[1]
        let schedFormHour = schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[0] + ":" + schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[1]
        return schedFormDay + "/" + schedFormMonth + "/" + schedFormYear + " - " + schedFormHour
    }
    
    func getFormattedSolicDate(_ date:String) -> String{
        let schedDayHour = date.components(separatedBy:"-")[2]
        let schedFormDay = schedDayHour.components(separatedBy: " ")[0]
        let schedFormYear = date.components(separatedBy:"-")[0]
        let schedFormMonth = date.components(separatedBy:"-")[1]
        return schedFormDay + "/" + schedFormMonth + "/" + schedFormYear
    }
}

private func getMonth(_ month:String) -> String{
    
    switch month {
    case "01":
        return "janeiro"
    case "02":
        return "fevereiro"
    case "03":
        return "março"
    case "04":
        return "abril"
    case "05":
        return "maio"
    case "06":
        return "junho"
    case "07":
        return "julho"
    case "08":
        return "agosto"
    case "09":
        return "setembro"
    case "10":
        return "outubro"
    case "11":
        return "novembro"
    case "12":
        return "dezembro"
    default:
        return month
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.7, green: 0.7, blue: 0.7, alpha: 1)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        return spinnerView
    }
    class func removeSpinner(_ spinner:UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIImage {
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIViewController{
    func alertMessage(titleAlert:String, messageAlert:String, buttonAlert:String){
        
        let alertText = UIAlertController(title: titleAlert, message: messageAlert, preferredStyle: .alert)
        let alertButton = UIAlertAction(title: buttonAlert, style: .cancel, handler: nil)
        
        alertText.addAction(alertButton)
        
        self.present(alertText, animated: true, completion:nil)
        
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UITextField{
    open override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 3.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIApplication{
    class func getPresentedViewController() -> UIViewController? {
        var presentViewController = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentViewController?.presentedViewController
        {
            presentViewController = pVC
        }
        return presentViewController
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UITableView {
    
    @IBInspectable
    var isEmptyRowsHidden: Bool {
        get {
            return tableFooterView != nil
        }
        set {
            if newValue {
                tableFooterView = UIView(frame: .zero)
            } else {
                tableFooterView = nil
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIViewController {
    func getVersion() -> String{
        let bundle = Bundle.main.infoDictionary
        let version = bundle!["VERSION"] as! String? ?? ""
        return version
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIViewController {
    func getMarkerImage() -> UIImage{
        let image = #imageLiteral(resourceName: "Untitled-1@512h")
        let resizedImage = image.resized(toWidth: 25)
        return resizedImage!
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension UIImage{
    func videoSnapshot(_ videoURL:String,
                       _ success:@escaping ((UIImage) -> Void),
                       _ error:@escaping ((String) -> Void)) {
      
            DispatchQueue.global().async{
                let url = URL(string:videoURL)!
                let asset: AVAsset = AVAsset(url: url)
                let imageGenerator = AVAssetImageGenerator(asset: asset)
                do {
                    let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
                    let validImage = UIImage(cgImage: thumbnailImage)
                    success(validImage)
                } catch let err {
                    error(err.localizedDescription)
                }
            }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////
extension UIImage {
    func resize(width: CGFloat) -> UIImage {
        let height = (width/self.size.width)*self.size.height
        return self.resize(size: CGSize(width: width, height: height))
    }
    
    func resize(height: CGFloat) -> UIImage {
        let width = (height/self.size.height)*self.size.width
        return self.resize(size: CGSize(width: width, height: height))
    }
    
    func resize(size: CGSize) -> UIImage {
        let widthRatio  = size.width/self.size.width
        let heightRatio = size.height/self.size.height
        var updateSize = size
        if(widthRatio > heightRatio) {
            updateSize = CGSize(width:self.size.width*heightRatio, height:self.size.height*heightRatio)
        } else if heightRatio > widthRatio {
            updateSize = CGSize(width:self.size.width*widthRatio,  height:self.size.height*widthRatio)
        }
        UIGraphicsBeginImageContextWithOptions(updateSize, false, UIScreen.main.scale)
        self.draw(in: CGRect(origin: .zero, size: updateSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////
