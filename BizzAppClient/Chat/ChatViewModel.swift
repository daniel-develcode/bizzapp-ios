import Foundation
import UIKit

protocol ChatViewModelDelegate {
    func getMessageSuccess(_ listOfChatInfo:[Chat])
    func getMessageError(_ errorMessage:String)
    
    func sendTextMessageSuccess(_ successMessage:String)
    func sendTextMessageError(_ errorMessage:String)
}

protocol ChatViewModelProtocol {
    var delegate:ChatViewModelDelegate?{get set}
    
    func getMessages(_ userUid:String,
                     _ seqSol:String,
                     _ profUid:String,_ currentChat:[Chat])
    
    func sendTextMessage(_ userUid:String,
                         _ seqSol:String,
                         _ profUid:String,
                         _ messageID:String,
                         _ messageType:Int,
                         _ dic:[String:Any])
}

class ChatViewModel:ChatViewModelProtocol{
    var service: ChatServiceProtocol
    var delegate: ChatViewModelDelegate?
    
    init(_ service:ChatServiceProtocol){
        self.service = service
    }
    
    func getMessages(_ userUid: String, _ seqSol: String, _ profUid: String,_ currentChat:[Chat]) {
        service.getMessagesService(userUid, seqSol, profUid, { (listOfChatInfo) in
            self.delegate?.getMessageSuccess(listOfChatInfo)
        }) { (error) in
            self.delegate?.getMessageError(error)
        }
    }
    
    func sendTextMessage(_ userUid:String,
                         _ seqSol:String,
                         _ profUid:String,
                         _ messageID:String,
                         _ messageType:Int,
                         _ dic:[String:Any]){
        service.sendTextMessageService(userUid, seqSol, profUid,messageID,  dic, messageType,{ (successMessage) in
            self.delegate?.sendTextMessageSuccess(successMessage)
        }) { (errorMessage) in
            self.delegate?.sendTextMessageError(errorMessage)
        }
    }
}
