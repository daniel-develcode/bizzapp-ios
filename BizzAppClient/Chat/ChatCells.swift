import UIKit
import Firebase
import FirebaseAuth
import Messages
import MessageUI
import IQKeyboardManagerSwift
import AVKit

class UserChatCell:UITableViewCell{
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var chatBubbles: UIView!
    @IBOutlet weak var horary: UILabel!
    @IBOutlet weak var date: UILabel!
    
    func setChat(chat:Chat){
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        messageText.text = chat.message
        horary.text = nsDateToHour(chat.createdAt!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class ProfChatCell:UITableViewCell{
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var chatBubbles: UIView!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    func setChat(chat:Chat){
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        messageText.text = chat.message
        horary.text = nsDateToHour(chat.createdAt!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class UserImageChat:UITableViewCell{
    @IBOutlet weak var imageBtn: UIButton!
    var vcView:UIViewController?
    var url:String!
    @IBOutlet weak var chatBubbles: UIButton!
    @IBOutlet weak var horary: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBAction func fullscreen(_ sender: Any) {
        let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
        vc.receivedImage = imageBtn.currentImage
        vc.view.frame = (vcView?.view.bounds)!
        vcView!.addChild(vc)
        UIView.transition(with: (vcView?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.vcView!.view.addSubview(vc.view)}, completion: nil)
    }
    
    func setChat(chat:Chat){
        horary.text = nsDateToHour(chat.createdAt!)
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        let application = UIApplication.shared.delegate as! AppDelegate
        let downloader = application.getImageDownloader()
        url = chat.message!
        downloader.downloadImage(url, { (image,url) in
            UIView.transition(with: self.imageBtn, duration: 0.3, options: .transitionCrossDissolve, animations: {
                if(url == self.url){
                    self.imageBtn.imageView?.contentMode = .scaleAspectFit
                    self.imageBtn.setImage(image, for: .normal)
                }
            }, completion: nil)
        }) { (error) in
            print(error)}
    }
    
    override func prepareForReuse() {
        imageBtn.setBackgroundImage(nil, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class ProfImageChat:UITableViewCell{
    @IBOutlet weak var imageBtn: UIButton!
    var vcView:UIViewController?
    var url:String!
    @IBOutlet weak var chatBubbles: UIButton!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    @IBAction func fullscreen(_ sender: Any) {
        let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
        vc.receivedImage = imageBtn.currentImage
        vc.view.frame = (vcView?.view.bounds)!
        vcView!.addChild(vc)
        UIView.transition(with: (vcView?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.vcView!.view.addSubview(vc.view)}, completion: nil)
    }
    
    func setChat(chat:Chat){
        horary.text = nsDateToHour(chat.createdAt!)
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        let application = UIApplication.shared.delegate as! AppDelegate
        let downloader = application.getImageDownloader()
        url = chat.message!
        downloader.downloadImage(url, { (image,url) in
            UIView.transition(with: self.imageBtn, duration: 0.3, options: .transitionCrossDissolve, animations: {
                if(url == self.url){
                    self.imageBtn.imageView?.contentMode = .scaleAspectFit
                    self.imageBtn.setImage(image, for: .normal)
                }
            }, completion: nil)
        }) { (error) in
            print(error)}
    }
    
    override func prepareForReuse() {
        imageBtn.setBackgroundImage(nil, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class UserVideoChat:UITableViewCell{
    
    @IBOutlet weak var imageBtn: UIButton!
    var vcView:UIViewController?
    var url:String!
    @IBOutlet weak var chatBubbles: UIButton!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    @IBAction func videoPlayer(_ sender: Any) {
        let vd = VideoDownloader()
        vd.presentVideo(url, vcView!)
    }
    
    func setChat(chat:Chat){
        horary.text = nsDateToHour(chat.createdAt!)
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        let application = UIApplication.shared.delegate as! AppDelegate
        let downloader = application.getImageDownloader()
        url = chat.message!
        downloader.downloadImage(url, { (image,url) in
            UIView.transition(with: self.imageBtn, duration: 0.3, options: .transitionCrossDissolve, animations: {
                if(url == self.url){
                    self.imageBtn.setBackgroundImage(image, for: .normal)
                }
            }, completion: nil)
        }) { (error) in
            print(error)}
    }
    
    override func prepareForReuse() {
        imageBtn.setBackgroundImage(nil, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class ProfVideoChat:UITableViewCell{
    
    @IBOutlet weak var imageBtn: UIButton!
    var vcView:UIViewController?
    var url:String!
    @IBOutlet weak var chatBubbles: UIButton!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    @IBAction func videoPlayer(_ sender: Any) {
        let vd = VideoDownloader()
        vd.presentVideo(url, vcView!)
    }
    
    func setChat(chat:Chat){
        horary.text = nsDateToHour(chat.createdAt!)
        chatBubbles.layer.cornerRadius = 15
        chatBubbles.layer.masksToBounds = true
        let application = UIApplication.shared.delegate as! AppDelegate
        let downloader = application.getImageDownloader()
        url = chat.message!
        downloader.downloadImage(url, { (image,url) in
            UIView.transition(with: self.imageBtn, duration: 0.3, options: .transitionCrossDissolve, animations: {
                if(url == self.url){
                    self.imageBtn.setBackgroundImage(image, for: .normal)
                }
            }, completion: nil)
        }) { (error) in
            print(error)}
    }
    
    override func prepareForReuse() {
        imageBtn.setBackgroundImage(nil, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class UserAudioChat:UITableViewCell{
    @IBOutlet weak var playPauserBtn: UIButton!
    @IBOutlet weak var slider: UISlider!
    var audioPlayer:AudioPlayerProtocol!
    var url:String!
    var chat:Chat!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    func setHorary(){
        horary.text = nsDateToHour(chat.createdAt!)
    }
    
    @IBAction func playPause(_ sender: Any) {
        audioPlayer.streamAudio(url)
        audioPlayer.playPause(self.playPauserBtn, self.slider)
    }
}

class ProfAudioChat:UITableViewCell{
    @IBOutlet weak var playPauserBtn: UIButton!
    @IBOutlet weak var slider: UISlider!
    var audioPlayer:AudioPlayerProtocol!
    var url:String!
    var chat:Chat!
    @IBOutlet weak var horary: UILabel!
        @IBOutlet weak var date: UILabel!
    
    func setHorary(){
        horary.text = nsDateToHour(chat.createdAt!)
    }
    
    @IBAction func playPause(_ sender: Any) {
        audioPlayer.streamAudio(url)
        audioPlayer.playPause(self.playPauserBtn, self.slider)
    }
}

class Chat{
    var displayName:String?
    var message:String?
    var createdAt:NSDate?
    var device:String?
    var read:Bool?
    var type:String?
    var uid:String?
    
    init(_ displayName:String,_ message:String,_ createdAt:NSDate,_ device:String,_ read:Bool,_ type:String,_ uid:String){
        self.displayName = displayName
        self.createdAt = createdAt
        self.message = message
        self.device = device
        self.read = read
        self.type = type
        self.uid = uid
    }
}

fileprivate func nsDateToHour(_ date:NSDate) -> String{
    let calendar = Calendar.current
    let comp = calendar.dateComponents([.hour, .minute], from: date as Date)
    let hour = comp.hour
    let minute = comp.minute
    return String(format:"%02d", hour!)+":"+String(format: "%02d",minute!)
}
