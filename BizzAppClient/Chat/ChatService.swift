import Foundation
import UIKit
import Firebase
import FirebaseAuth
import Messages
import MessageUI

protocol ChatServiceProtocol {
    typealias getMessagesSuccess = (([Chat]) -> Void)
    typealias getMessagesError = ((String) -> Void)
    
    typealias sendMessageSuccess = ((String) -> Void)
    typealias sendMessageError = ((String) -> Void)
    
    typealias didSaveAudioSuccess = ((String) -> Void)
    typealias didSaveAudioError = ((String) -> Void)
    
    func getMessagesService(_ userUid:String,
                            _ seqSol:String,
                            _ profUid:String,
                            _ success:@escaping getMessagesSuccess,
                            _ error:@escaping getMessagesError)
    
    func sendTextMessageService(_ userUid:String,
                                _ seqSol:String,
                                _ profUid:String,
                                _ messageID:String,
                                _ dic:[String:Any],
                                _ messageType:Int,
                                _ success:@escaping sendMessageSuccess,
                                _ error:@escaping sendMessageError)
}

class ChatService:ChatServiceProtocol{
   
    var listOfuploadingFeatures:[String] = []
    var ref = DatabaseReference.init()
    
    init(_ ref:DatabaseReference){
        self.ref = ref
    }
    
    func getMessagesService(_ userUid:String,
                            _ seqSol:String,
                            _ profUid:String,
                            _ success:@escaping getMessagesSuccess,
                            _ error:@escaping getMessagesError) {
        
        self.ref.child("messages_bizzapp").child(userUid+"_"+seqSol+"_"+profUid).queryOrdered(byChild: "createdAt").observe( .value, with:{ (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                var listOfChatInfo:[Chat] = []
                for snap in snapshot{
                    if let postData = snap.value as? [String:Any]{
                      
                        let clongDate = postData["createdAt"] as? CLong
                        let createdAt = NSDate(timeIntervalSince1970:TimeInterval((clongDate!)/1000))
                        let device = postData["device"] as? String
                        let displayName = postData["displayName"] as? String
                        let message = postData["message"] as? String
                        let read = postData["read"] as? Bool
                        let type = postData["type"] as? String
                        let uid = postData["uid"] as? String
                        let thisMessage = Chat(displayName!, message!, createdAt, device!, read!, type!, uid!)
                        listOfChatInfo.append(thisMessage)
                    }
                    
                }
                success(listOfChatInfo)
            }else{
                error("erro ao recuperar as mensagens")
            }
        })
    }
    
    func sendTextMessageService(_ userUid:String,
                                _ seqSol:String,
                                _ profUid:String,
                                _ messageID:String,
                                _ dic:[String:Any],
                                _ messageType:Int,
                                _ success:@escaping sendMessageSuccess,
                                _ error:@escaping sendMessageError){
        
        let uid = UserDefaults.standard.object(forKey:"userUid") as? String
        if (messageType == 0){
            if(uid != nil){
                self.ref.child("messages_bizzapp").child(userUid+"_"+seqSol+"_"+profUid).child(messageID).setValue(dic)
                self.ref.child("messages_bizzapp_pro").child(profUid+"_"+seqSol+"_"+userUid).child(messageID).setValue(dic)
                success(messageID)
            }else{
                error("erro ao enviar mensagem.")
            }
        }else{
            if(uid != nil){
                self.ref.child("messages_bizzapp").child(userUid+"_"+seqSol+"_"+profUid).child(messageID).updateChildValues(dic)
                self.ref.child("messages_bizzapp_pro").child(profUid+"_"+seqSol+"_"+userUid).child(messageID).updateChildValues(dic)
                success(messageID)
            }else{
                error("erro ao atualizar mensagem.")
            }
        }
    }
}
