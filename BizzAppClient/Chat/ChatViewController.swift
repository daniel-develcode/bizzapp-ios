import UIKit
import Firebase
import FirebaseAuth
import Messages
import MessageUI
import IQKeyboardManagerSwift
import AVKit

class ChatViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ChatViewModelDelegate,ImageReceiverDelegate,VideoReceiverDelegate,MediaPickerDelegate,AVAudioRecorderDelegate,UploadAudioDelegate{
    
    var importedImage:UIImage?
    var videoLocalUrl:URL?
    var seqSol:String?
    var profUid:String?
    var userUid:String?
    var userName:String?
    var recURL:URL?
    var viewModel:ChatViewModelProtocol?
    var listOfChatInfo = [Chat]()
    var uploadList:[String:String] = [:]
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AudioPlayerProtocol!
    var audioUploader:UploadAudioProtocol!
    var labelName:String!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var sendText: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    var ref = DatabaseReference.init()
    @IBOutlet weak var profImage: UIImageView!
    @IBOutlet weak var selectImageBtn: UIButton!
    var profImageUrl:String!
    
    @IBAction func sendButton(_ sender: Any) {
        if (sendText.text != ""){
            let dic = [
                "createdAt":ServerValue.timestamp(),
                "device":"bizzapp",
                "displayName":userName ?? "",
                "message":sendText.text!,
                "read":false,
                "type":"text",
                "uid":userUid!
                ] as [String:Any]
            let id = ref.childByAutoId().key
            if(id != nil){
                viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 0, dic)
            }else{
                print("erro ao enviar mensagem de texto")
            }
            sendText.text = ""
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    @IBAction func mediaPicker(_ sender: Any) {
        let vc = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "MediaPicker") as! MediaPicker
        vc.seqSol = Int(seqSol!)
        vc.videoReceiverDelegate = self
        vc.imageReceiverDelegate = self
        vc.mediaPickerDelegate = self
        vc.view.frame = self.view.bounds
        self.addChild(vc)
        UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.view.addSubview(vc.view)}, completion: nil)
    }
    
    func didSendImage(_ imageID: String) {
        let dic = [
            "createdAt":ServerValue.timestamp(),
            "device":"bizzapp",
            "displayName":userName ?? "",
            "message":"",
            "read":false,
            "type":"enviando",
            "uid":userUid!
            ] as [String:Any]
        let id = ref.childByAutoId().key
        if(id != nil){
            uploadList[imageID] = id!
            viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 0, dic)
        }else{
            print("erro ao recuperar id")
        }
    }
    
    func didSendVideo(_ videoID: String) {
        let dic = [
            "createdAt":ServerValue.timestamp(),
            "device":"bizzapp",
            "displayName":userName ?? "",
            "message":"",
            "read":false,
            "type":"enviando",
            "uid":userUid!
            ] as [String:Any]
        let id = ref.childByAutoId().key
        if(id != nil){
            uploadList[videoID] = id!
            viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 0, dic)
        }else{
            print("erro ao recuperar id")
        }
    }
    
    func imageUploadProgress(_ progress: Double, _ imageReference: String) {
        print("Progresso imagem: \(progress)")
    }
    
    func imageUploadSucess(_ downloadURL: String, _ imageReference: String) {
        print(imageReference)
        let dic = [
            "device":"bizzapp",
            "displayName":userName ?? "",
            "message":downloadURL,
            "read":false,
            "type":"image",
            "uid":userUid!
            ] as [String:Any]
        let id = uploadList[imageReference]
        uploadList.removeValue(forKey: imageReference)
        if(id != nil){
            viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 1, dic)
        }else{
            print("erro ao enviar imagem")
        }
        
        print("imagem sucesso: \(downloadURL)")
    }
    
    func imageUploadError(_ errorMessage: String) {
        print("imagem falhou: \(errorMessage)")
    }
    
    func videoUploadProgress(_ progress: Double, _ videoReference: String) {
        print("Progresso video: \(progress)")
    }
    
    func videoUploadSucess(_ downloadURL: String, _ videoReference: String) {
        print("video sucesso: \(downloadURL)")
        let dic = [
            "device":"bizzapp",
            "displayName":userName ?? "",
            "message":downloadURL,
            "read":false,
            "type":"video",
            "uid":userUid!
            ] as [String:Any]
        let id = uploadList[videoReference]
        if(id != nil){
            viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 1, dic)
        }else{
            print("erro ao enviar imagem")
        }
    }
    
    func videoUploadError(_ errorMessage: String) {
        print("imagem falhou: \(errorMessage)")
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfChatInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(listOfChatInfo[indexPath.row].device != "bizzapp-pro"){
            if(listOfChatInfo[indexPath.row].type == "text"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserChatCell")! as? UserChatCell else { return UITableViewCell()}
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else if(listOfChatInfo[indexPath.row].type == "image"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserImageChat")! as? UserImageChat else { return UITableViewCell()}
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                cell.vcView = self
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else if (listOfChatInfo[indexPath.row].type == "video"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserVideoChat")! as? UserVideoChat else { return UITableViewCell()}
                cell.vcView = self
                let videoURL = listOfChatInfo[indexPath.row].message
                cell.url = videoURL
                var imagePlay = #imageLiteral(resourceName: "play")
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                let cgsize:CGSize = CGSize(width: 50, height: 50)
                imagePlay = imagePlay.resize(size: cgsize)
                cell.imageBtn.setImage(imagePlay, for: .normal)
                let application = UIApplication.shared.delegate as! AppDelegate
                let downloader = application.getImageDownloader()
                let thumb = downloader.checkCache(videoURL!)
                if(thumb == nil){
                    imagePlay.videoSnapshot(videoURL!, { (image) in
                        downloader.addImageToCache(videoURL!, image)
                        DispatchQueue.main.sync {
                            cell.imageBtn.setBackgroundImage(image, for: .normal)
                        }
                    }) { (error) in
                        print(error)
                    }
                }else{
                    cell.imageBtn.setBackgroundImage(thumb, for: .normal)
                }
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
               
            }else if (listOfChatInfo[indexPath.row].type == "audio"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserAudioChat")! as? UserAudioChat else { return UITableViewCell()}
                cell.audioPlayer = self.audioPlayer
                cell.url = listOfChatInfo[indexPath.row].message
                cell.chat = listOfChatInfo[indexPath.row]
                cell.setHorary()
                if(listOfChatInfo.count>=indexPath.row+2) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else if (listOfChatInfo[indexPath.row].type == "enviando"){
               guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserLoadingDataCell")! as? UserLoadingDataCell  else { return UITableViewCell()}
                cell.startAnimateSpin()
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            if(listOfChatInfo[indexPath.row].type == "text"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfChatCell")! as? ProfChatCell else { return UITableViewCell()}
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                return cell
                
            }else if(listOfChatInfo[indexPath.row].type == "image") {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfImageChat")! as? ProfImageChat else { return UITableViewCell()}
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                cell.vcView = self
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else if (listOfChatInfo[indexPath.row].type == "video"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfVideoChat")! as? ProfVideoChat else { return UITableViewCell()}
                cell.vcView = self
                let videoURL = listOfChatInfo[indexPath.row].message
                cell.url = videoURL
                var imagePlay = #imageLiteral(resourceName: "play")
                cell.setChat(chat: listOfChatInfo[indexPath.row])
                let cgsize:CGSize = CGSize(width: 50, height: 50)
                imagePlay = imagePlay.resize(size: cgsize)
                cell.imageBtn.setImage(imagePlay, for: .normal)
                let application = UIApplication.shared.delegate as! AppDelegate
                let downloader = application.getImageDownloader()
                let thumb = downloader.checkCache(videoURL!)
                if(thumb == nil){
                    imagePlay.videoSnapshot(videoURL!, { (image) in
                        downloader.addImageToCache(videoURL!, image)
                        DispatchQueue.main.sync {
                            cell.imageBtn.setBackgroundImage(image, for: .normal)
                        }
                    }) { (error) in
                        print(error)
                    }
                }else{
                    cell.imageBtn.setBackgroundImage(thumb, for: .normal)
                }
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else if (listOfChatInfo[indexPath.row].type == "audio"){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfAudioChat")! as? ProfAudioChat else { return UITableViewCell()}
                cell.audioPlayer = self.audioPlayer
                cell.url = listOfChatInfo[indexPath.row].message
                cell.chat = listOfChatInfo[indexPath.row]
                cell.setHorary()
                if(listOfChatInfo.count>indexPath.row+1) {
                    if(compareDate(listOfChatInfo[indexPath.row].createdAt!, listOfChatInfo[indexPath.row+1].createdAt!)){
                        cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row+1].createdAt!)
                        cell.date.isHidden = false
                    }else{
                        cell.date.isHidden = true
                    }
                }
                if(indexPath.row == listOfChatInfo.count - 1){
                    cell.date.text = nsDateToDay(listOfChatInfo[indexPath.row].createdAt!)
                    cell.date.isHidden = false
                }
                cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                return cell
                
            }else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        navigationController?.isNavigationBarHidden = false;
        return false
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    func getMessageSuccess(_ listOfChatInfo: [Chat]) {
        if (listOfChatInfo.count > 0){
            self.listOfChatInfo = listOfChatInfo.reversed()
            self.table.reloadData()
            let indexpath = IndexPath(row:0, section: 0)
            self.table.scrollToRow(at: indexpath, at: .top, animated: false)
        }
    }
    
    func getMessageError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    func sendTextMessageSuccess(_ successMessage: String) {
        print(successMessage)
    }
    
    func sendTextMessageError(_ errorMessage: String) {
        print(errorMessage)
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    
    @IBAction func recBtn(_ sender: Any) {
        recordTapped()
    }
    
    @IBAction func releaseRecBtn(_ sender: Any) {
        recordTapped()
    }
    
    
    func askForRecPermission(){
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.recordButton.isEnabled = true
                    } else {
                        self.recordButton.isEnabled = false
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        self.recURL = audioFilename
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.low.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            selectImageBtn.setImage(#imageLiteral(resourceName: "baseline-mic-24px-green"), for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        if success {
            let uploadTime = String(CACurrentMediaTime())
            let reference = "scheduling_gallery/"+String(seqSol!)+"/audio_"+uploadTime
            let dic = [
                "createdAt":ServerValue.timestamp(),
                "device":"bizzapp",
                "displayName":userName ?? "",
                "message":"enviando audio",
                "read":false,
                "type":"enviando",
                "uid":userUid!
                ] as [String:Any]
            let id = ref.childByAutoId().key
            if(id != nil){
                uploadList[reference] = id!
                viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 0, dic)
            }else{
                print("erro ao recuperar id")
            }
            audioUploader.saveRecordedMessage(recURL!, reference)
            
              selectImageBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        } else {
              selectImageBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        }
    }
    
    @objc func recordTapped() {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func didUploadAudioSuccess(_ stringURL: String, _ reference: String) {
        print("video sucesso: \(stringURL)")
        let dic = [
            "device":"bizzapp",
            "displayName":userName ?? "",
            "message":stringURL,
            "read":false,
            "type":"audio",
            "uid":userUid!
            ] as [String:Any]
        let id = uploadList[reference]
        if(id != nil){
            viewModel?.sendTextMessage(userUid!, seqSol!, profUid!, id!, 1, dic)
        }else{
            print("erro ao enviar imagem")
        }
    }
    
    func didUploadAudioError(_ Error: String, _ reference: String) {
        print(Error,reference)
    }
    
    func didUploadAudioProgress(_ progress: Double, _ audioReference: String) {
        print(progress)
    }
    
    private func loadImage(){
        let application = UIApplication.shared.delegate as! AppDelegate
        let downloader = application.getImageDownloader()
        downloader.downloadImage(profImageUrl ?? "", { (image,url) in
            self.profImage.image = image
        }) { (error) in
            print(error)
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
        table.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        askForRecPermission()
        let uid = UserDefaults.standard.object(forKey:"userUid") as? String
        let name = UserDefaults.standard.object(forKey:"userName") as? String
        userName = name
        userUid = uid
        nameLbl.text = labelName
        audioUploader = UploadAudioService(self)
        self.audioPlayer = AudioPlayer()
        self.ref = Database.database().reference()
        table.isEmptyRowsHidden = true
        let service = ChatService(ref)
        viewModel = ChatViewModel(service)
        viewModel?.delegate = self
        viewModel?.getMessages(userUid!, seqSol!, profUid!,listOfChatInfo)
        sendText.delegate = self
    }
}

class UserLoadingDataCell:UITableViewCell{
    @IBOutlet weak var spin: UIActivityIndicatorView!
    
    func startAnimateSpin(){
        spin.startAnimating()
    }
}

fileprivate func nsDateToDay(_ date:NSDate) -> String{    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    return dateFormatter.string(from: date as Date)
}

fileprivate func compareDate(_ date1:NSDate,_ date2:NSDate) -> Bool{
    let calendar1 = Calendar.current
    let comp1 = calendar1.dateComponents([.day, .month, .year], from: date1 as Date)
    let day1 = comp1.day
    let month1 = comp1.month
    let year1 = comp1.year
    
    let calendar2 = Calendar.current
    let comp2 = calendar2.dateComponents([.day, .month, .year], from: date2 as Date)
    let day2 = comp2.day
    let month2 = comp2.month
    let year2 = comp2.year
    
    if(year2! > year1!){
        return true
    }else if(month2! > month1!){
        return true
    }else if ((year2! == year1!)&&(month2! == month1!)&&(day2! > day1!)){
        return true
    }else{
       return false
    }
}
