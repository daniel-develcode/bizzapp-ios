    import UIKit
    import AVFoundation
    import Foundation
    import Firebase
    import AVKit
    import MobileCoreServices
    
    protocol MediaPickerDelegate {
        func didSendImage(_ imageID:String)
        func didSendVideo(_ videoID:String)
    }
    
    class MediaPicker:UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UploadVideoDelegate,UploadImageDelegate{
        var mediaPickerDelegate:MediaPickerDelegate?
        var videoReceiverDelegate:VideoReceiverDelegate?
        var imageReceiverDelegate:ImageReceiverDelegate?
        var seqSol:Int?
        var saveFileName:String!
        let imagePicker:UIImagePickerController! = UIImagePickerController()
        
        @IBAction func pickFromCameraBtn(_ sender: Any) {
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
                    imagePicker.allowsEditing = false
                    imagePicker.delegate = self
                    
                    present(imagePicker, animated: true, completion: {})
                } else {
                    print("Rear camera doesn't exist,Application cannot access the camera.")
                }
            } else {
                print("Camera inaccessable,Application cannot access the camera.")
            }
        }
        
        @IBAction func pickFromGaleryBtn(_ sender: Any) {
            let video = UIImagePickerController()
            video.delegate = self
            video.sourceType = .photoLibrary
            video.allowsEditing = false
            video.mediaTypes = ["public.movie","public.image"]
            self.present(video, animated: true)
        }
        
        @IBAction func cancelBtn(_ sender: Any) {
            self.view.removeFromSuperview()
        }
////////////////////////////////////////////////////////////////////////////////////
        func uploadVideoSuccess(_ videoUrl: String, _ videoReference: String) {
            self.videoReceiverDelegate?.videoUploadSucess(videoUrl, videoReference)
        }
        
        func uploadVideoError(_ errorMessage: String) {
            self.videoReceiverDelegate?.videoUploadError(errorMessage)
        }
        
       func uploadVideoProgress(_ progress: Double, _ reference: String) {
        self.videoReceiverDelegate?.videoUploadProgress(progress, reference)
        }
        
        func uploadImageSuccess(_ successMessage: String,_ imageReference:String) {
            self.imageReceiverDelegate!.imageUploadSucess(successMessage, imageReference)
        }
        
        func uploadImageError(_ errorMessage: String) {
            self.imageReceiverDelegate!.imageUploadError(errorMessage)
        }
        
        func uploadImageProgress(_ progress: Double,_ reference:String) {
            self.imageReceiverDelegate!.imageUploadProgress(progress,reference)
        }
////////////////////////////////////////////////////////////////////////////////////
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let data = convertFromUIImagePickerControllerInfoKeyDictionary(info)
            let callBackType = data["UIImagePickerControllerMediaType"] as! String
            if(callBackType == "public.movie"){
                if let pickedVideo:URL = (data[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL) {
                    let selectorToCall = #selector(self.videoWasSavedSuccessfully(_:didFinishSavingWithError:context:))
                    UISaveVideoAtPathToSavedPhotosAlbum(pickedVideo.relativePath, self, selectorToCall, nil)
                    encodeVideo(videoUrl: pickedVideo)
                }
            }else if(callBackType == "public.image"){
                if let photo = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                {
                    imageReceiverDelegate?.importedImage = photo
                    let uploadImage = uploadImageService(self)
                    let uploadTime = String(CACurrentMediaTime())
                    let reference = "scheduling_gallery/"+String(seqSol!)+"/chat/image_"+uploadTime
                    self.mediaPickerDelegate?.didSendImage(reference)
                    uploadImage.uploadImage(photo,reference)
                }
                else
                {
                    self.imageReceiverDelegate?.imageUploadError("Ocorreu um erro inesperado")
                }
            }
            self.dismiss(animated: true, completion: nil)
            self.view.removeFromSuperview()
        }
////////////////////////////////////////////////////////////////////////////////////
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            print("User canceled image")
            dismiss(animated: true, completion: {
            })
        }
        
        @objc func videoWasSavedSuccessfully(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
            if let theError = error {
                print("An error happened while saving the video = \(theError)")
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        }
        
        private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
            return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
        }
        
        private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
            return input.rawValue
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            let uploadTime = String(CACurrentMediaTime())
            saveFileName = "/video_"+uploadTime+"_"+String(seqSol!)+".mp4"
        }
        
        //////////////=================//////////////////====================
        
        // converte o video para mp4
        func encodeVideo(videoUrl:URL) {
            
            var finalOutputUrl:URL
            
            var url = videoUrl
            url.deletePathExtension()
            url.appendPathExtension("mp4")
            finalOutputUrl = url
            
            if FileManager.default.fileExists(atPath: finalOutputUrl.path) {
                print("Converted file already exists \(finalOutputUrl.path)")
                print(finalOutputUrl)
            }
            
            let asset = AVURLAsset(url: videoUrl)
            if let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) {
                exportSession.outputURL = finalOutputUrl
                exportSession.outputFileType = AVFileType.mp4
                let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
                let range = CMTimeRangeMake(start: start, duration: asset.duration)
                exportSession.timeRange = range
                exportSession.shouldOptimizeForNetworkUse = true
                exportSession.exportAsynchronously() {
                    
                    switch exportSession.status {
                    case .failed:
                        print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
                    case .cancelled:
                        print("Export canceled")
                    case .completed:
                        print("SUCESS ---------- "+finalOutputUrl.absoluteString)
                        
                        
                        let videoData = try? Data(contentsOf: finalOutputUrl)
                        let uploadVidServ = UploadVideoService(self)
                        var uploadTime = String(CACurrentMediaTime())
                        let index = uploadTime.index(of: ".")
                        if index != nil {
                            uploadTime.remove(at: index!)
                        }
                        let reference = "scheduling_gallery/"+String(self.seqSol!)+"/chat/video_"+uploadTime
                        self.mediaPickerDelegate?.didSendVideo(reference)
                        uploadVidServ.uploadVideo(videoData!,reference)
                        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                        let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
                        self.videoReceiverDelegate?.videoLocalUrl = videoUrl
                        let dataPath = documentsDirectory.appendingPathComponent(self.saveFileName)
                        try! videoData?.write(to: dataPath, options: [])
                        print("Saved to " + dataPath.absoluteString)
                        
                    default:
                        break
                    }
                }
            } else {
                print("Export failed")
            }
        }
    }
