import Foundation
import Firebase
import Alamofire
import FirebaseAuth
import FirebaseStorage

struct LoginUserData{
    let email: String?
    let password: String?
}

protocol LoginServiceProtocol {
    
    typealias AuthorizationSuccess = ((String) -> Void)
    typealias AuthorizationError = ((String) -> Void)
    
    typealias OnLoginCallbackSuccess = ((UserData) -> Void)
    typealias OnLoginCallbackError = ((String) -> Void)
    
    func loginService (
        _ user:LoginUserData,
        _ success: @escaping OnLoginCallbackSuccess,
        _ error: @escaping OnLoginCallbackError)
}

class LoginUserService:LoginServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ requestService: RequestServiceProtocol) {
        self.service = requestService
    }
    
    func loginService (_ user:LoginUserData,
                       _ success: @escaping OnLoginCallbackSuccess,
                       _ error: @escaping OnLoginCallbackError){
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"auth/public/auth-user"
        let application = UIApplication.shared.delegate as! AppDelegate
        let token = application.getNotificationToken()!
        LoginAuthorization(user,{ (userUid) in
            let parameters:Parameters = [
                "chvUsuario":userUid,
                "desEmail":user.email!,
                "device":[
                    "numImei":"123456789",
                    "numId":token,
                    "tipApp":1,
                    "tipSystem":2
                    ]
                ] as [String : Any]
            
            let header:HTTPHeaders = [
                "Content-Type":"application/json",
                "Accept-Encoding":"gzip",
                "User-Agent":"gzip"
                ] as [String : String]
            
            self.service.request(url,.post,parameters,header,
                                 { (dataR) in
                                    let decoded = Decoders.decodeData(dataR!) as UserData
                                    print(decoded.token!)
                                    UserDefaults.standard.set(decoded.email, forKey: "lastLogged")
                                    UserDefaults.standard.set(decoded.latitude, forKey: "userLatitude")
                                    UserDefaults.standard.set(decoded.longitude, forKey: "userLongitude")
                                    UserDefaults.standard.set(decoded.token, forKey: "userToken")
                                    UserDefaults.standard.set(decoded.userCode, forKey: "userCode")
                                    UserDefaults.standard.set(decoded.uid, forKey: "userUid")
                                    UserDefaults.standard.set(decoded.name, forKey: "userName")
                                    let adress = SolicAdress(
                                    latitude:decoded.latitude,
                                    longitude:decoded.longitude,
                                    adress:decoded.adress,
                                    reference:decoded.reference)
                                    self.validateSolAdress(adress)
                                    success(decoded)},
                                 { (requestError) in
                                    error(requestError)})
        })
        { (authError) in
            error(authError)
        }
    }
    
    private func validateSolAdress(_ adress:SolicAdress){
     guard let _ = UserDefaults.standard.object(forKey: "SolLatitude") as? Double else {saveDefaultSolAdress(adress);return}
     guard let _ = UserDefaults.standard.object(forKey: "SolLongitude") as? Double else {saveDefaultSolAdress(adress);return}
     guard let _ = UserDefaults.standard.object(forKey: "SolAdress") as? String else {saveDefaultSolAdress(adress);return}
     guard let _ = UserDefaults.standard.object(forKey: "SolReference") as? String else {saveDefaultSolAdress(adress);return}
    }
    
    private func saveDefaultSolAdress(_ adress:SolicAdress){
        UserDefaults.standard.set(adress.adress, forKey: "SolAdress")
        UserDefaults.standard.set(adress.latitude, forKey: "SolLatitude")
        UserDefaults.standard.set(adress.longitude, forKey: "SolLongitude")
        UserDefaults.standard.set(adress.reference, forKey: "SolReference")
    }
    
    private func LoginAuthorization(
        _ user:LoginUserData,
        _ success: @escaping AuthorizationSuccess,
        _ error: @escaping AuthorizationError){
        let auth = Auth.auth()
        auth.signIn(withEmail:user.email!, password:user.password!){ (userL,errorL) in
            if(errorL == nil){
                success(((auth.currentUser?.uid)!))
            }else{
                let recError = errorL! as NSError
                let errorCode = recError.userInfo["error_name"] as! String
                error(errorCode)
            }
        }
    }
}
