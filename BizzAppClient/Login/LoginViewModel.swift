import Foundation
import UIKit

protocol LoginViewModelDelegate {
    
    func didLoginWithSuccess(_ token:String)
    
    func didLoginWithError(_ error:[String])
    
    func didPressRegisterBtn(_ sender:Any)
}

protocol LoginViewModelProtocol{
    
    var delegate:LoginViewModelDelegate?{get set}
    
    func login(_ user:LoginUserData)
}

class LoginViewModel:LoginViewModelProtocol{
    var delegate:LoginViewModelDelegate?
    var service:LoginServiceProtocol
    
    init(_ loginService: LoginServiceProtocol) {
        service = loginService
    }
    
    func login(_ user:LoginUserData){
        
        var errorBundle:[String] = []
        
        guard let validUser = validateInput(user) else { errorBundle.append("erro ao validar os dados");self.delegate?.didLoginWithError(errorBundle); return }
        
        if (user.password == ""){
            errorBundle.append("É necessário preencher a senha.")
        }
        if (user.email == ""){
            errorBundle.append("É necessário preencher o email.")
        }
        if (errorBundle.count < 1){
            service.loginService(validUser, { userData in
                self.delegate?.didLoginWithSuccess(userData.token!)
            }) { error in
                errorBundle.append(error)
                self.delegate?.didLoginWithError(errorBundle)
            }
        }else{
            self.delegate?.didLoginWithError(errorBundle)
        }
    }
    fileprivate func validateInput(_ user:LoginUserData) -> LoginUserData? {
        guard let email = user.email else { return nil }
        guard let password = user.password else { return nil  }
        
        return LoginUserData(
            email: email,
            password: password
        )
    }
}
