import UIKit
import Swinject
import BEMCheckBox
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase

class LoginViewController:UIViewController,LoginViewModelDelegate,UITextFieldDelegate,FacebookLoginViewModelDelegate,LoginButtonDelegate {
    var viewModel:LoginViewModelProtocol!
    var tokenExpired:Bool = false
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var keepLogged: BEMCheckBox!
    @IBOutlet weak var facebookOutlet: UIButton!
    var spinner:UIView!
    @IBOutlet weak var version: UILabel!
    var securyEntryBtn = UIButton(type: .custom)
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        UserDefaults.standard.set(keepLogged.on, forKey: "keepLogged")
        spinner = UIViewController.displaySpinner(onView:self.view)
            let userData = LoginUserData(
            email:emailTxt.text,
            password:passwordTxt.text
        )
        viewModel.login(userData)
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if error != nil {
            print(error!)
            return
        }
        print("Logged in")
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Logged out!")
    }
    
    
    private func validateSearch(){
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as? Int
        let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as? Int
        let catName = UserDefaults.standard.object(forKey:"categoryName") as? String
        let subcatName = UserDefaults.standard.object(forKey:"subCategoryName") as? String
        let catImageStr = UserDefaults.standard.object(forKey:"categoryImage") as? String
        
        if  ((catCode == nil)||(subcatCode == nil)||(catName == nil)||(subcatName == nil)||(catImageStr == nil)){
            let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
            let vc: SelectCategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
            self.present(vc, animated: true, completion: nil)
        }else{
            let storyboard: UIStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
            let tabBarVC: TabBarViewController = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            self.present(tabBarVC, animated: true, completion: nil)
        }
    }
    
    private func validateKeepLogged(){
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as? Int
        let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as? Int
        let catName = UserDefaults.standard.object(forKey:"categoryName") as? String
        let subcatName = UserDefaults.standard.object(forKey:"subCategoryName") as? String
        let catImageStr = UserDefaults.standard.object(forKey:"categoryImage") as? String
        let token = UserDefaults.standard.object(forKey:"userToken") as? String
        let isLogged = UserDefaults.standard.object(forKey:"keepLogged") as? Bool
        
        if  ((catCode == nil)||(subcatCode == nil)||(catName == nil)||(subcatName == nil)||(catImageStr == nil)||(token == nil)){
            keepLogged.on = false
        }else{
            if (isLogged != nil){
                let validateIsLogged = isLogged!
                if(validateIsLogged == false){
                    keepLogged.on = false
                }else{
                    keepLogged.on = true
                }
            }else{
                keepLogged.on = false
            }
        }
    }
    
    func didLoginWithSuccess(_ token: String) {
        print("usuario logado com sucesso \(token)")
       UIViewController.removeSpinner(spinner)
        validateSearch()
    }
    
    func didLoginWithError(_ error: [String]) {
        print("erro ao logar \(error)")
         UIViewController.removeSpinner(spinner)
        emailError.isHidden = true
        passwordError.isHidden = true
        emailTxt.layer.borderColor = UIColor.lightGray.cgColor
        passwordTxt.layer.borderColor = UIColor.lightGray.cgColor
        
        for err in error{
            
            if (err == "É necessário preencher o email."){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "email requerido."
                emailError.isHidden = false
            }else if (err == "ERROR_INVALID_EMAIL"){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "email inválido."
                emailError.isHidden = false
            }else if (err == "ERROR_USER_NOT_FOUND"){
                self.alertMessage(titleAlert: "Erro", messageAlert: "Conta não cadastrada", buttonAlert: "OK")
            }
            
            if (err == "ERROR_TOO_MANY_REQUESTS"){
                self.alertMessage(titleAlert: "Erro", messageAlert: "Você tentou fazer muitas requisições, tente novamente em alguns segundos.", buttonAlert: "OK")
            }
            
            if (err == "É necessário preencher a senha."){
                passwordTxt.layer.borderColor = UIColor.red.cgColor
                passwordError.text = "Senha requerida."
                passwordError.isHidden = false
            }else if (err == "ERROR_WRONG_PASSWORD"){
                passwordTxt.layer.borderColor = UIColor.red.cgColor
                passwordError.text = "Senha Inválida"
                passwordError.isHidden = false
            }
        }
    }
    
    @IBAction func facebookLoginBtn(_ sender: Any) {
        spinner = UIViewController.displaySpinner(onView:self.view)
        let faceBookViewModel = FacebookLoginViewModel(AppDelegate.container.resolve(FacebookLoginServiceProtocol.self)!)
        faceBookViewModel.delegate = self
        faceBookViewModel.loginWithFacebook(self)
    }
    
    func loginWithFacebookSuccess(_ successMessage: String) {
        print(successMessage)
        UIViewController.removeSpinner(spinner)
    }
    
    func loginWithFacebookError(_ errorMessage: String) {
        UIViewController.removeSpinner(spinner)
        self.alertMessage(titleAlert: "Erro", messageAlert: errorMessage, buttonAlert: "OK")
    }
    
    @IBAction func didPressRegisterBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "RegUserForm", bundle: nil)
        let registerVC: UIViewController = storyboard.instantiateViewController(withIdentifier: "RegUserFormViewController") as UIViewController
        self.present(registerVC, animated: true, completion: nil)
    }
    
    private func alertTokenExpired(){
        if (tokenExpired == true){
            self.alertMessage(titleAlert: "Erro", messageAlert: "Sessão expirada.", buttonAlert: "OK")
        }
    }
    
    @IBAction func forgotPasswordBtn(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ForgotPasswordViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        vc.currentEmail = emailTxt.text
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        alertTokenExpired()
        if(keepLogged.on == true){
            validateSearch()
        }
    }
    
    @IBAction func refresh(_ sender: Any) {
        if (passwordTxt.isSecureTextEntry ==  true){
            passwordTxt.isSecureTextEntry =  false
             securyEntryBtn.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            passwordTxt.isSecureTextEntry =  true
             securyEntryBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    private func configSecureEntry(_ txt:UITextField){
    securyEntryBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
    securyEntryBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
    securyEntryBtn.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
    securyEntryBtn.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
    txt.rightView = securyEntryBtn
    txt.rightViewMode = .always
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        facebookOutlet.contentMode = .scaleAspectFit
        validateKeepLogged()
        let lastLogged = UserDefaults.standard.object(forKey:"lastLogged") as? String
        if (lastLogged != nil){
            emailTxt.text = lastLogged
        }
        let viewModel = LoginViewModel(AppDelegate.container.resolve(LoginServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        self.emailTxt.delegate = self
        self.passwordTxt.delegate = self
        self.configSecureEntry(self.passwordTxt)
        version.text = self.getVersion()
    }
}
