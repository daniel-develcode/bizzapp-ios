import UIKit
import Foundation
import Firebase

class ForgotPasswordViewController: UIViewController {
    
    var currentEmail:String?
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var emailError: UILabel!
    
    @IBAction func sendEmail(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail:emailTxt.text ?? "") { error in
            self.emailError.isHidden = true
            self.emailTxt.layer.borderColor = UIColor.lightGray.cgColor
            if(error == nil){
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordTransitionVC")
                self.present(vc, animated: true, completion: nil)
                
            }else{
                let errorCode = error?.localizedDescription
                
                switch errorCode {
                case "An email address must be provided.":
                    self.emailError.text = "Email requerido."
                    self.emailError.isHidden = false
                    self.emailTxt.layer.borderColor = UIColor.red.cgColor
                    break
                    
                case "The email address is badly formatted.":
                    self.emailError.text = "Email inválido."
                    self.emailError.isHidden = false
                    self.emailTxt.layer.borderColor = UIColor.red.cgColor
                    break
                    
                case "There is no user record corresponding to this identifier. The user may have been deleted.":
                    self.alertMessage(titleAlert: "Erro", messageAlert: "Conta não cadastrada.", buttonAlert: "OK")
                    break
                    
                default:
                    self.alertMessage(titleAlert: "Erro", messageAlert: "Ouve um erro inesperado", buttonAlert: "OK")
                }
            }
        }
    }
    
    @IBAction func returnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTxt.text = currentEmail ?? ""
    }
}
