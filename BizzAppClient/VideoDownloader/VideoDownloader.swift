import Foundation
import UIKit
import AVKit

class VideoDownloader{
    func presentVideo(_ url:String,_ viewControl:UIViewController){
        let player = AVPlayer(url: URL(string: url)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        viewControl.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}
