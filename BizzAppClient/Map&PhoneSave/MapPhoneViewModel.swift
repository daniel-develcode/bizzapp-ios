import Foundation

protocol PhoneViewModelDelegate {
    
    var user:GeneralRegData?{get set}
    
    func didGetPhoneWithSuccess(_ success:GeneralRegData)
    
    func didGetPhoneWithError (_ errorMessage:String)
    
    func didPressContinueBtn(_ sender:Any)
}

protocol MapViewModelDelegate {
    var user:GeneralRegData?{get set}
    
    func didSaveMapPhoneWithSuccess(_ successMessage:String)
    
    func didSaveMapPhoneWithError (_ errorMessage:String)
    
    func didPressContinueBtn(_ sender:Any)
}

protocol MapPhoneViewModelProtocol{
    
    var phoneDelegate:PhoneViewModelDelegate?{get set}
    var mapDelegate:MapViewModelDelegate?{get set}
    
    func getPhone(_ userData:GeneralRegData)
    
    func saveMapPhone(_ userData: GeneralRegData,_ service: RequestServiceProtocol)
}

class MapPhoneViewModel:MapPhoneViewModelProtocol{
    
    var mapDelegate: MapViewModelDelegate?
    var phoneDelegate: PhoneViewModelDelegate?
    
    func getPhone(_ userData: GeneralRegData) {
        
        guard let validUserData = validateInput(InvalidData: userData) else {phoneDelegate?.didGetPhoneWithError("erro ao validar os dados");return}
        if (validUserData.phone!.count >= 12){
            phoneDelegate?.didGetPhoneWithSuccess(validUserData)
        }else{
            phoneDelegate?.didGetPhoneWithError("Número Inválido")
        }
    }
    
    func saveMapPhone(_ userData: GeneralRegData,_ service: RequestServiceProtocol) {
        
        guard let validUserData = validateInput(InvalidData: userData) else {phoneDelegate?.didGetPhoneWithError("erro ao validar os dados");return}
        if (validUserData.adress != ""){
            let saveService = MapPhoneService(service)
            saveService.save(validUserData, { (successMessage) in
                self.mapDelegate?.didSaveMapPhoneWithSuccess(successMessage)
            }) { (errorMessage) in
                self.mapDelegate?.didSaveMapPhoneWithError(errorMessage)
            }
        }else{
            mapDelegate?.didSaveMapPhoneWithError("Você deve por um endereço")
        }
    }
}

fileprivate func validateInput(InvalidData: GeneralRegData)-> GeneralRegData?{
    
    guard let validName = InvalidData.name else { return nil }
    guard let validBI = InvalidData.biNumber else { return nil }
    guard let validEmail = InvalidData.email else {return nil}
    guard let validAdress = InvalidData.adress else {return nil}
    guard let validReference = InvalidData.reference else {return nil}
    guard let validPhone = InvalidData.phone else {return nil}
    guard let validToken = InvalidData.token else {return nil}
    guard let validLat = InvalidData.numLatitude else {return nil}
    guard let validLong =  InvalidData.numLongitude else {return nil}
    guard let validUid = InvalidData.uid else {return nil}
    
    return GeneralRegData(
        name:validName,
        biNumber:validBI,
        email:validEmail,
        uid:validUid,
        adress:validAdress,
        reference:validReference,
        phone:validPhone,
        token:validToken,
        numLatitude:validLat,
        numLongitude:validLong
    )
}
