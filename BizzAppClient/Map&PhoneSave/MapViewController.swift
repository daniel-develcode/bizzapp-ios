import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift

class MapViewController: UIViewController,MapViewModelDelegate,UISearchBarDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var mapContainer: GMSMapView!
    @IBOutlet weak var referenceTxt: UITextField!
    var user:GeneralRegData?
    var viewModel:MapPhoneViewModelProtocol!
    var locationManager = CLLocationManager()
    var adress:String = ""
    var lati:Float = 0.0
    var long:Float = 0.0
    var location: CLLocation?
    @IBOutlet weak var adressLbl: UINavigationItem!
    
    @IBAction func searchBtn(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.locationManager.startUpdatingLocation()
        self.present(autocompleteController, animated:true, completion:nil)
    }
    
    func didSaveMapPhoneWithSuccess(_ successMessage: String) {
        print(successMessage)
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
        let vc: SelectCategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func didSaveMapPhoneWithError(_ errorMessage: String) {
        print(errorMessage)
        self.alertMessage(titleAlert: "Error", messageAlert: errorMessage, buttonAlert: "OK")
    }
    
    @IBAction func didPressContinueBtn(_ sender: Any) {
        user?.adress = adress
        user?.numLatitude = lati
        user?.numLongitude = long
        user?.reference = referenceTxt.text
        viewModel.saveMapPhone(user!, AppDelegate.container.resolve(RequestServiceProtocol.self)!)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16.0)
        mapContainer.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        marker.title = place.name
        marker.map = mapContainer
        marker.icon = self.getMarkerImage()
        adress = place.formattedAddress!
        let invalidLati = place.coordinate.latitude
        let invalidLong = place.coordinate.longitude
        lati = Float(invalidLati)
        long = Float(invalidLong)
        adressLbl.title = place.formattedAddress!
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("falhou ao completar\(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func iniGoogleMaps(){
        self.mapContainer.delegate = self
        self.mapContainer.isMyLocationEnabled = true
        self.mapContainer.settings.myLocationButton = true
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        let camera = GMSCameraPosition.camera(withLatitude: location?.coordinate.latitude ?? 0, longitude: location?.coordinate.longitude ?? 0 , zoom: 16.0)
        mapContainer.camera = camera
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("erro ao conseguir a localização\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 16.0)
        self.mapContainer.animate(to: camera)
        locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapContainer.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        navigationController?.isNavigationBarHidden = false;
        return false
    }
    
    @IBAction func returnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
        let viewModel = MapPhoneViewModel()
        self.viewModel = viewModel
        viewModel.mapDelegate = self
        iniGoogleMaps()
        self.referenceTxt.delegate = self
    }
}
