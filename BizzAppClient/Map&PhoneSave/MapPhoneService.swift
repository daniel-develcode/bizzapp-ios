import Foundation
import Alamofire

protocol MapPhoneServiceProtocol {
    
    typealias OnSaveSuccess = (String) -> Void
    typealias OnSaveError = (String) -> Void
    
    func save(_ userData:GeneralRegData,
              _ SaveSuccess:@escaping OnSaveSuccess,
              _ SaveError:@escaping OnSaveError)
}

class MapPhoneService:MapPhoneServiceProtocol {
    
    var service:RequestServiceProtocol
    init(_ requestService: RequestServiceProtocol) {
        service = requestService
    }
    
    func save(_ userData: GeneralRegData,
              _ SaveSuccess: @escaping OnSaveSuccess,
              _ SaveError: @escaping OnSaveError){
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"auth/update-user"
        let header = getHeader(userData)
        let param = getParam(userData)
        
        service.request(url, .post, param, header, { (Data) in
            UserDefaults.standard.set(userData.numLatitude, forKey: "userLatitude")
            UserDefaults.standard.set(userData.numLongitude, forKey: "userLongitude")
            SaveSuccess("Salvo com sucesso")
        }) { (error) in
            SaveError("Erro ao salvar")
        }
    }
    
    private func getHeader(_ user:GeneralRegData) -> HTTPHeaders{
        let header:HTTPHeaders = [
            "Authorization":user.token!,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        return header
    }
    
    private func getParam(_ user:GeneralRegData) -> Parameters{
        let adress = SolicAdress(
            latitude:Double(user.numLatitude ?? 0),
            longitude:Double(user.numLongitude ?? 0),
            adress:user.adress,
            reference:user.reference)
        validateSolAdress(adress)
        let application = UIApplication.shared.delegate as! AppDelegate
        let token = application.getNotificationToken()!
        let parameters:Parameters = [
            "chvUsuario":user.uid!,
            "desEmail":user.email!,
            "desNome":user.name!,
            "desNif":"tstdev",
            "desBi":user.biNumber!,
            "desTelefonePrin":user.phone!,
            "numLatitude": user.numLatitude!,
            "numLongitude": user.numLongitude!,
            "desStringEndereco":user.adress!,
            "desReferencia":user.reference!,
            "indAceite":1,
            "device":[
                "numImei":"123456789",
                "numId":token,
                "tipApp":1,
                "tipSystem":2]
            ] as [String : Any]
        return parameters
    }
    
    private func validateSolAdress(_ adress:SolicAdress){
        guard let _ = UserDefaults.standard.object(forKey: "SolLatitude") as? Double else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolLongitude") as? Double else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolAdress") as? String else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolReference") as? String else {saveDefaultSolAdress(adress);return}
    }
    
    private func saveDefaultSolAdress(_ adress:SolicAdress){
        UserDefaults.standard.set(adress.adress, forKey: "SolAdress")
        UserDefaults.standard.set(adress.latitude, forKey: "SolLatitude")
        UserDefaults.standard.set(adress.longitude, forKey: "SolLongitude")
        UserDefaults.standard.set(adress.reference, forKey: "SolReference")
    }
}
