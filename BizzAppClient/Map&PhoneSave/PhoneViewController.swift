import UIKit

class PhoneViewController: UIViewController,PhoneViewModelDelegate,UITextFieldDelegate {
    
    var user:GeneralRegData?
    var viewModel:MapPhoneViewModelProtocol!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var phoneError: UILabel!
    
    func didGetPhoneWithSuccess(_ success:GeneralRegData) {
    
        print("Salvou telefone com sucesso\(user!.phone!)")
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MapPhone", bundle: nil)
        let mapVC: MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapVC.user = user
        let navigationController = UINavigationController(rootViewController: mapVC)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func didGetPhoneWithError(_ errorMessage: String) {
        print(errorMessage)
        phoneTxt.layer.borderColor = UIColor.lightGray.cgColor
        phoneError.isHidden = true
        
        if (errorMessage == "Número Inválido"){
            phoneTxt.layer.borderColor = UIColor.red.cgColor
            phoneError.text = errorMessage
            phoneError.isHidden = false
        }
    }
    
    @IBAction func didPressContinueBtn(_ sender: Any) {
        
        user?.phone = phoneTxt.text
        viewModel.getPhone(user!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        navigationController?.isNavigationBarHidden = false;
        return false
    }
    
    @IBAction func returnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let viewModel = MapPhoneViewModel()
        self.viewModel = viewModel
        viewModel.phoneDelegate = self
        self.phoneTxt.delegate = self
    }
}
