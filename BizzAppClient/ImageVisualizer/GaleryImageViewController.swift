import UIKit
import Foundation

class GaleryImageViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    var receivedImage:UIImage!
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.view.removeFromSuperview()
  
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = receivedImage
    }
}
