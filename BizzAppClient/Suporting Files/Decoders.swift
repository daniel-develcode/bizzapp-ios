import Foundation
import Alamofire

class Decoders{
    
    static func decodeData<T:Codable>(_ decodableData:DataResponse<Any>) ->  T{
        do{
            _ = try JSONDecoder().decode(Payload<T>.self, from: decodableData.data!)
        }catch{
            print(error)
        }
        
        let payload = try? JSONDecoder().decode(Payload<T>.self, from: decodableData.data!)
        let data = payload?.data
        return data!
    }
    
    static func decodeDataArray<T:Codable>(_ decodableData:DataResponse<Any>) -> [T]{
        
        do{
            _ = try JSONDecoder().decode(PayloadArray<T>.self, from: decodableData.data!)
        }catch{
            print(error)
        }
        
        let payload = try? JSONDecoder().decode(PayloadArray<T>.self, from: decodableData.data!)
        let data = payload?.data
        return data!
    }
}
