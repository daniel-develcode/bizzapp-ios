import UIKit
import CoreData
import Firebase
import Swinject
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import FBSDKCoreKit
import UserNotifications
import AlamofireImage
import AVFoundation

@UIApplicationMain
class AppDelegate:UIResponder,UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate{
    var window: UIWindow?
    private var token:String = ""
    
    var imageDownloader:ImageDownloader!
    
    func setImageDownloader(_ imageDownloader:ImageDownloader){
        self.imageDownloader = imageDownloader
    }
    
    func getImageDownloader() -> ImageDownloader{
        return self.imageDownloader
    }
    
    func getNotificationToken() -> String?{
        print("FCM TOKEN: ",token)
        return self.token
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        let db = Firestore.firestore()
        let settings = db.settings
        db.settings = settings
        configureAudioSession()
        
        GMSServices.provideAPIKey(getGoogleApiKey())
        GMSPlacesClient.provideAPIKey(getGoogleApiKey())
        IQKeyboardManager.shared.enable = true
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        let application = UIApplication.shared.delegate as! AppDelegate
        let imageDownloader = ImageDownloader()
        application.setImageDownloader(imageDownloader)
        registerForPushNotifications()
        return true
    }
    
    private func getGoogleApiKey() -> String{
        let bundle = Bundle.main.infoDictionary
        let key = bundle!["GOOGLE_API_KEY"] as! String? ?? ""
        return key
    }
    
    func configureAudioSession(){
        do {
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord , mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
    }
    
    func registerForPushNotifications(){
        if #available(iOS 10.0, *) {
            //	if (UIApplication.shared.isRegisteredForRemoteNotifications) { return }
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            
            //UIApplication.shared.enabledRemoteNotificationTypes() != UIRemoteNotificationType
            
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("FCM Token: \(token)")
        self.token = fcmToken
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        
        Messaging.messaging().apnsToken = deviceToken
        
        print("Device Token: \(token)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication , annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BizzAppClient")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    static let container = { () -> Container in
        let container = Container()
        
        container.register(RequestServiceProtocol.self) { _ in
            RequestService()
        }
        
        container.register(ImageDownloaderProtocol.self) { _ in
            ImageDownloader()
        }
        
        container.register(LoginServiceProtocol.self) { r  in
            LoginUserService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(RegUserFormServiceProtocol.self){ r in
            RegUserFormService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(MapPhoneServiceProtocol.self){ r in
            MapPhoneService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(SearchProfServiceProtocol.self){ r in
            SearchProfService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(SolicitationsServiceProtocol.self) { r in
            SolicitationService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(EvaluationsServiceProtocol.self) { r in
            EvaluationsService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(UserProfileServiceProtocol.self) { r in
            UserProfileService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(ChangePasswordServiceProtocol.self) { r in
            ChangePasswordService()
        }
        
        container.register(UpdateProfileServiceProtocol.self) { r in
            UpdateProfileService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(ProfProfileServiceProtocol.self){ r in
            ProfProfileService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(ScheduleServiceProtocol.self){ r in
            ScheduleService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(SaveSolicitationServiceProtocol.self){ r in
            SaveSolicitationService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(ChangeSolicAdressServiceProtocol.self){ r in
            ChangeSolicAdress()
        }
        
        container.register(CancelServiceProtocol.self){r in
            CancelService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(SelectCategoryServiceProtocol.self){r in
            SelectCategoryService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(SelectedSolicitationServiceProtocol.self){r in
            SelectedSolicitationService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        container.register(FacebookLoginServiceProtocol.self){r in
            FacebookLoginService(r.resolve(RequestServiceProtocol.self)!)
        }
        
        return container
    }()
}
