import Foundation
import Alamofire
import UIKit
import Gzip

protocol RequestServiceProtocol {
    typealias RequestCallbackSuccess = ((DataResponse<Any>?) -> Void)
    typealias RequestCallbackError = ((String) -> Void)
    
    func request (
        _ url:String,
        _ httpMethod:HTTPMethod,
        _ parameters:Parameters?,
        _ header:HTTPHeaders?,
        _ success:@escaping RequestCallbackSuccess,
        _ error:@escaping RequestCallbackError
    )
}

class RequestService:RequestServiceProtocol {
    
    func  request(_ url:String,
                  _ httpMethod:HTTPMethod,
                  _ parameters:Parameters?,
                  _ header:HTTPHeaders?,
                  _ success:@escaping RequestCallbackSuccess,
                  _ error:@escaping RequestCallbackError) {
      /*
         print("")
         print(parameters ?? "parameters nulo")
         print("")
         print(header ?? "header nulo")
         print("")
       */
        Alamofire.request(url,
                          method:httpMethod,
                          parameters:parameters,
                          encoding: JSONEncoding.default,
                          headers:header)
            .responseJSON{ response in
             //  print(response)
                switch response.result {
                case .success:
                    if (((response.response?.statusCode)! >= 200) && ((response.response?.statusCode)!<=300)){
                        success(response)
                    }else{
                        error("Erro ao fazer requisição")
                    }
                case .failure:
                    self.sessionExpired()
                    error("Erro ao fazer requisição")
                }
        }
    }
    
    private func sessionExpired(){
        
        let topController = UIApplication.getPresentedViewController()
        UserDefaults.standard.removeObject(forKey:"userLatitude")
        UserDefaults.standard.removeObject(forKey:"userLongitude")
        UserDefaults.standard.removeObject(forKey:"userToken")
        UserDefaults.standard.removeObject(forKey:"userCode")
        UserDefaults.standard.removeObject(forKey:"userUid")
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.tokenExpired = true
        topController!.present(vc, animated: true, completion: nil)
    }
}
