import Foundation
import Alamofire

protocol LoadImageProtocol {
    typealias OnLoadImageSuccess = ((UIImage) -> Void)
    typealias OnLoadImageError = ((String) -> Void)
    
    func load(_ url:String,
              _ success:@escaping OnLoadImageSuccess,
              _ error:@escaping OnLoadImageError)
}

class LoadImage:LoadImageProtocol{
    
    func load(_ url:String,
              _ success:@escaping OnLoadImageSuccess,
              _ error:@escaping OnLoadImageError){
        
        Alamofire.request(url).responseData { (response) in
            if (response.error == nil && response.data != nil){
                let LoadedImage = UIImage(data: response.data!)
                success(LoadedImage ?? #imageLiteral(resourceName: "DefaultProfile"))
            }
            else{
                error("erro ao baixar a imagem")
            }
        }
    }
}
