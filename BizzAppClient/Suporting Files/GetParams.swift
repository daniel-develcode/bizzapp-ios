import Foundation
import UIKit

class SessionHolder{
    var userCode:String
    var token: String
    
    init(_ userCode:String,_ token:String){
        self.userCode = userCode
        self.token = token
    }
}

struct getWithIdParams{
    var token:String
    var userCode:String
}
struct getWithUidParams{
    var token:String
    var uid:String
}

class GetParams{
    
    static func WithIdParams() -> getWithIdParams? {
        
        guard let validUserCode = UserDefaults.standard.object(forKey:"userCode") else {return nil}
        let userCodeInt = validUserCode as! Int
        let userCodeString = String(userCodeInt)
        guard let validToken = UserDefaults.standard.object(forKey:"userToken") else {return nil}
        
        let param = getWithIdParams(
            token:validToken as! String,
            userCode:userCodeString
        )
        return param
    }
    
    static func WithUidParams() -> getWithUidParams? {
        
        guard let validUserUid = UserDefaults.standard.object(forKey:"userUid") else {return nil}
        guard let validToken = UserDefaults.standard.object(forKey:"userToken") else {return nil}
        
        let param = getWithUidParams(
            token:validToken as! String,
            uid:validUserUid as! String
        )
        return param
    }
}
