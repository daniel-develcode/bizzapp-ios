import Foundation

struct UserData:Codable{
    var userCode:Int?
    var uid:String?
    var email:String?
    var name:String?
    var reference:String?
    var bi:String?
    var image:String?
    var adress:String?
    var phone:String?
    var termsAccept:Int?
    var inactive:Int?
    var token:String?
    var grade:Float?
    var latitude:Double?
    var longitude:Double?
    
    enum CodingKeys: String, CodingKey {
        
        case userCode = "codUsuario"
        case uid = "chvUsuario"
        case email = "desEmail"
        case name = "desNome"
        case reference = "desReferencia"
        case bi = "desBi"
        case image = "desFoto"
        case adress = "desStringEndereco"
        case phone = "desTelefonePrin"
        case termsAccept = "indAceite"
        case inactive =  "indInativo"
        case token = "dsToken"
        case grade = "numNota"
        case latitude = "numLatitude"
        case longitude = "numLongitude"
    }
}

struct GeneralRegData{
    var name:String?
    var biNumber:String?
    var email:String?
    var uid:String?
    var adress:String?
    var reference:String?
    var phone:String?
    var token:String?
    var numLatitude:Float?
    var numLongitude:Float?
}
