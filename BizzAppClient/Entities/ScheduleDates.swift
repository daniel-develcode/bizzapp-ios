import Foundation

struct ScheduleDates:Codable{
    var dates:[String]
    
    enum CodingKeys: String, CodingKey {
        case dates = "scheduleDates"
    }
}
