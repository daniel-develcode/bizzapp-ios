import Foundation

struct Payload<T>: Codable where T: Codable {
    var status: Int
    var timestamp: String
    var data: T
    
    enum CodingKeys: String, CodingKey {
        case status = "cdStatus"
        case timestamp = "timestamp"
        case data = "data"
    }
}

struct PayloadArray<T>: Codable where T: Codable {
    var status: Int
    var timestamp: String
    var data: [T]
    
    enum CodingKeys: String, CodingKey {
        case status = "cdStatus"
        case timestamp = "timestamp"
        case data = "data"
    }
}
