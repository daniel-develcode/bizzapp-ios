import Foundation

struct GetSolicitations:Codable{
    var page:Int
    var limit:Int
    var offset:Int
    var pageCount:Int
    var totalCount:Int
    var lsSolicitation:[Solicitations]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case limit = "limit"
        case offset = "offset"
        case pageCount = "pageCount"
        case totalCount = "totalCount"
        case lsSolicitation = "lsSolicitation"
    }
}

struct Solicitations:Codable{
    var solicitationsSeq:Int
    var profUid:String
    var profName:String
    var profImage:String?
    var descServ:String
    var profGrade:Float?
    var schedDate:String
    var solicDate:String
    var status:Int
    var messageList:[MessageList]
    
    enum CodingKeys: String, CodingKey {
        case solicitationsSeq = "seqSolicitacao"
        case profUid = "chvUsuario"
        case profName = "nomeProfissional"
        case profImage = "imagemProfissional"
        case descServ = "descServicoSolicitado"
        case profGrade = "notaProfissional"
        case schedDate = "dtaSolicitacao"
        case solicDate = "dtaAgendamento"
        case status = "indStatus"
        case messageList = "mensagensLista"
    }
}

struct MessageList:Codable{
    var messageSeq:Int
    var messageDate:String
    var messageTxt:String
    var readInd:Int
    
    enum CodingKeys: String, CodingKey {
        case messageSeq = "seqMensagem"
        case messageDate = "dtaMensagem"
        case messageTxt = "txtMensagem"
        case readInd = "indLeitura"
    }
}

struct SolicitationParams{
    var userCode:Int?
    var profCode:Int?
    var seqServ:Int?
    var seqSol:Int?
    var solicDate:String?
    var schedDate:String?
    var txtObs:String?
    var indStatus:Int?
    var price:Float?
    var adress:String?
    var reference:String?
    var lat:Double?
    var lon:Double?
    var token:String?
    var imageUrl:[String]?
    var videoUrl:String?
}

struct SelectedSolicitation:Codable{
    
    var solicitationSeq:Int
    var codProf:Int
    var codUser:Int
    var userUid:String
    var profName:String
    var profImage:String?
    var clientName:String
    var clientImage:String?
    var descSubcategory:String
    var descService:String
    var profGrade:Float?
    var clientGrade:Float?
    var solicDate:String
    var schedDate:String
    var indStatus:Int
    var obsTxt:String
    var adress:String?
    var reference:String?
    var indAttresidence:Int
    var indAttOwnPlace:Int
    var numLat:Float?
    var numLong:Float?
    var messages:[messageList]
    var imgVideoList:[ImgVideoList]
    
    enum CodingKeys: String, CodingKey {
        
        case solicitationSeq = "seqSolicitacao"
        case codProf = "codProfissional"
        case codUser = "codUsuario"
        case userUid = "chvUsuario"
        case profName = "nomeProfissional"
        case profImage = "imagemProfissional"
        case clientName = "nomeCliente"
        case clientImage = "imagemCliente"
        case descSubcategory = "descSubcategoria"
        case descService = "descServicoSolicitado"
        case profGrade = "notaProfissional"
        case clientGrade = "notaCliente"
        case solicDate = "dtaSolicitacao"
        case schedDate = "dtaAgendamento"
        case indStatus = "indStatus"
        case obsTxt = "txtObs"
        case adress = "desStringEndereco"
        case reference = "desStringReferencia"
        case indAttresidence = "indAtendeDom"
        case indAttOwnPlace = "indAtendeEstab"
        case numLat = "numLatitude"
        case numLong = "numLongitude"
        case messages = "mensagensLista"
        case imgVideoList = "imgsvideoSolicitationPresenterList"
        
    }
}

struct messageList:Codable{
    var seqMessage:Int
    var dateMessage:String
    var txtMessage:String
    var indRead:Int
    
    enum CodingKeys: String, CodingKey {
        
        case seqMessage = "seqMensagem"
        case dateMessage = "dtaMensagem"
        case txtMessage = "txtMensagem"
        case indRead = "indLeitura"
    }
}

struct ImgVideoList:Codable{
    var seqImage:Int
    var desImage:String
    var indInactive:Int
    var seqSolic:Int
    var type:Int
    
    enum CodingKeys: String, CodingKey {
        
        case seqImage = "seqImagem"
        case desImage = "desImagem"
        case indInactive = "indInativo"
        case seqSolic = "seqSolicitacao"
        case type = "tipo"
    }
}

struct GetSeqParams:Codable{
    var id:String
    var seqSol:Int
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case seqSol = "value"
    }
}
