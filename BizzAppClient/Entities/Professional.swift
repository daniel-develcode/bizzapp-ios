import Foundation

struct GetProfessional:Codable{
    var page:Int
    var limit:Int
    var offset:Int
    var pageCount:Int
    var totalCount:Int
    var lsProfessional:[Professional]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case limit = "limit"
        case offset = "offset"
        case pageCount = "pageCount"
        case totalCount = "totalCount"
        case lsProfessional = "lsProfessional"
    }
}

struct Professional: Codable {
    var id: Int
    var userId: Int
    var name: String
    var image: String?
    var availableNow: Int
    var latitude: Float?
    var longitude: Float?
    var nota: Float?
    var subCategories: [SubCategory]
    
    enum CodingKeys: String, CodingKey {
        case id = "codProfissional"
        case userId = "codUsuario"
        case name = "desNome"
        case image = "fotoUsuario"
        case availableNow = "indDisponibilityNow"
        case latitude = "numLatitude"
        case longitude = "numLongitude"
        case nota = "numNota"
        case subCategories = "lsSubcategories"
    }
}

struct SubCategory: Codable {
    var id: Int
    var subcategoryId: Int
    var description: String
    var available: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "codCategoria"
        case subcategoryId = "codSubcategoria"
        case description = "desSubcategoria"
        case available = "indInativo"
    }
}

struct GetProfProfile:Codable{
    var profId:Int?
    var userId:Int?
    var phoneAlt:String?
    var descTxt:String?
    
    var profLat:Double?
    var profLon:Double?
    var profAdress:String?
    var profReference:String?
    
    var twitUrl:String?
    var instUrl:String?
    var faceUrl:String?
    var googUrl:String?
    var portUrl:String?
    
    var indAttResidence:Int?
    var indAttEstab:Int?
    var indInact:Int?
    var profName:String?
    var profGrade:Float?
    var profImage:String?
    var servicesDone:Int?
    
    var dateBegMorn:String?
    var dateEndMorn:String?
    var dateBegEven:String?
    var dateEndEven:String?
    
    var amountMinAtt:Int?
    
    var indSundayMorn:Int?
    var indSundayEven:Int?
    var indSundayNight:Int?
    
    var indMondayMorn:Int?
    var indMondayEven:Int?
    var indMondayNight:Int?
    
    var indTuesdayMorn:Int?
    var indTuesdayEven:Int?
    var indTuesdayNight:Int?
    
    var indWednesdayMorn:Int?
    var indWednesdayEven:Int?
    var indWednesdayNight:Int?
    
    var indThursdayMorn:Int?
    var indThursdayEven:Int?
    var indThursdayNight:Int?
    
    var indFridayMorn:Int?
    var indFridayEven:Int?
    var indFridayNight:Int?
    
    var indSaturdayMorn:Int?
    var indSaturdayEven:Int?
    var indSaturdayNight:Int?
    
    var profImages:[String]?
    var subCatList:[ProfSubCategories]?
    var profServices:[ProfServ]?
    var profServParams:[ProfServParam]?
    var profCatParams:[ProfCatParam]?
    var profEval:[Evaluations]?
    var profImag:[Images]?

    enum CodingKeys: String, CodingKey {
        case profId = "codProfissional"
        case userId = "codUsuario"
        case phoneAlt = "desTelefoneAlt"
        case descTxt = "txtDescricao"
        
        case profLat = "numLatitude"
        case profLon = "numLongitude"
        case profAdress = "desEndereco"
        case profReference = "desReferencia"
        
        case twitUrl = "desLinkTwit"
        case instUrl = "desLinkInst"
        case faceUrl = "desLinkFace"
        case googUrl = "desLinkGoog"
        case portUrl = "desLinkPortifolio"
        
        case indAttResidence = "indAtendeDom"
        case indAttEstab = "indAtendeEstab"
        case indInact = "indInativo"
        case profName = "desNome"
        case profGrade = "numNota"
        case profImage = "fotoUsuario"
        case servicesDone = "servicesDone"
        
        case dateBegMorn = "dthIniManha"
        case dateEndMorn = "dthFimManha"
        case dateBegEven = "dthIniTarde"
        case dateEndEven = "dthFimTarde"
        
        case amountMinAtt = "qtdMinAtend"
        
        case indSundayMorn = "indDomManha"
        case indSundayEven = "indDomTarde"
        case indSundayNight = "indDomNoite"
        
        case indMondayMorn = "indSegManha"
        case indMondayEven = "indSegTarde"
        case indMondayNight = "indSegNoite"
        
        case indTuesdayMorn = "indTerManha"
        case indTuesdayEven = "indTerTarde"
        case indTuesdayNight = "indTerNoite"
        
        case indWednesdayMorn = "indQuaManha"
        case indWednesdayEven = "indQuaTarde"
        case indWednesdayNight = "indQuaNoite"
        
        case indThursdayMorn = "indQuiManha"
        case indThursdayEven = "indQuiTarde"
        case indThursdayNight = "indQuiNoite"
        
        case indFridayMorn = "indSexManha"
        case indFridayEven = "indSexTarde"
        case indFridayNight = "indSexNoite"
        
        case indSaturdayMorn = "indSabManha"
        case indSaturdayEven = "indSabTarde"
        case indSaturdayNight = "indSabNoite"
        
        case profImages = "lsFotosProfissional"
        case subCatList = "lsSubcategories"
        case profServices = "lsServices"
        case profServParams = "serviceProfessionalParamList"
        case profCatParams = "professionalCategoryParamList"
        case profEval = "lsEvaluations"
        case profImag = "lsImages"
        }
    }

    struct ProfSubCategories:Codable{
        var catCode:Int?
        var subCatCode:Int?
        var descSubCat:String?
        var indInact:Int?
        
        enum CodingKeys: String, CodingKey {
            case catCode = "codCategoria"
            case subCatCode = "codSubcategoria"
            case descSubCat = "desSubcategoria"
            case indInact = "indInativo"
        }
    }
    
    struct ProfServ:Codable{
        var numServ:Int?
        var desServ:String?
        var price:Float?
        var codeUm:String?
        var codeCat:Int?
        var codeSubCat:Int?
        
        enum CodingKeys: String, CodingKey {
            case numServ = "seqServico"
            case desServ = "desServico"
            case price = "vlrPreco"
            case codeUm = "codUm"
            case codeCat = "codCategoria"
            case codeSubCat = "codSubCategoria"
        }
    }
    
    struct ProfServParam:Codable{
        var numServ:Int?
        var profCode:Int?
        var desServ:String?
        var price:Float?
        var codeUm:String?
        var indInac:Int?
        var codeCat:Int?
        var codeSubCat:Int?
        
        enum CodingKeys: String, CodingKey {
            case numServ = "seqServico"
            case profCode = "codProfissional"
            case desServ = "desServico"
            case price = "vlrPreco"
            case codeUm = "codUm"
            case indInac = "indInativo"
            case codeCat = "codCategoria"
            case codeSubCat = "codSubcategoria"
        }
    }
    
    struct ProfCatParam:Codable{
        var profCode:Int?
        var subCatCode:Int?
        var catCode:Int?
        var desCat:String?
        var inInact:Int?
        
        enum CodingKeys: String, CodingKey {
            case profCode = "codProfissional"
            case subCatCode = "codSubcategoria"
            case catCode = "codCategoria"
            case desCat = "desCategoria"
            case inInact = "inInactive"
        }
    }
    
    struct Evaluations:Codable{
        var userImage:String?
        var userName:String?
        var grade:Float?
        var txtObs:String?
        
        enum CodingKeys: String, CodingKey {
            case userImage = "fotoUsuario"
            case userName = "desNome"
            case grade = "numNota"
            case txtObs = "txtObs"
        }
    }
    
    struct Images:Codable{
        var numImage:Int?
        var desImage:String?
        var indInact:Int?
        var profCode:Int?
        
        enum CodingKeys: String, CodingKey {
            case numImage = "seqImagem"
            case desImage = "desImagem"
            case indInact = "indInativo"
            case profCode = "codProfissional"
        }
    }
