import Foundation

struct GetCategory:Codable{
    var catCode:Int
    var desCat:String
    var imageCat:String
    var indInact:Int
    
    enum CodingKeys: String, CodingKey {
        case catCode = "codCategoria"
        case desCat = "desCategoria"
        case imageCat = "desImagem"
        case indInact = "indInativo"
    }
}

struct GetSubCategory:Codable{
    var page:Int
    var cats:[SubCategories]
    
    enum CodingKeys: String, CodingKey {
        
        case page = "page"
        case cats = "lsSubcategoria"
    }
}

struct SubCategories:Codable{
    var catCode:Int
    var codSubCat:Int
    var desCat:String
    var indInact:Int
    
    enum CodingKeys: String, CodingKey {
        case catCode = "codCategoria"
        case codSubCat = "codSubcategoria"
        case desCat = "desSubcategoria"
        case indInact = "indInativo"
    }
}

