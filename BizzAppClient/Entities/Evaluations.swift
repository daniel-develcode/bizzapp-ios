import Foundation

struct GetEvaluationsToDo:Codable{
    var page:Int
    var limit:Int
    var offset:Int
    var pageCount:Int
    var totalCount:Int
    var lsEvaluation:[EvaluationsToDo]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case limit = "limit"
        case offset = "offset"
        case pageCount = "pageCount"
        case totalCount = "totalCount"
        case lsEvaluation = "lsEvaluation"
    }
}

struct GetEvaluationsDone:Codable{
    var page:Int
    var limit:Int
    var offset:Int
    var pageCount:Int
    var totalCount:Int
    var lsEvaluation:[EvaluationsDone]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case limit = "limit"
        case offset = "offset"
        case pageCount = "pageCount"
        case totalCount = "totalCount"
        case lsEvaluation = "lsEvaluation"
    }
}

struct EvaluationsToDo:Codable{
    var seqSolic:Int
    var profCode:Int
    var image:String
    var name:String
    var subCategorie:String
    var date:String
    
    enum CodingKeys: String, CodingKey {
        case seqSolic = "seqSolicitacao"
        case profCode = "codUserProfissional"
        case image = "desFoto"
        case name = "desNome"
        case subCategorie = "desSubcategoria"
        case date = "dtaAgendamento"
    }
}

struct EvaluationsDone:Codable{
    var image:String
    var name:String
    var subCategorie:String
    var grade:Float
    var observation:String
    var date:String
    
    enum CodingKeys: String, CodingKey {
        case image = "desFoto"
        case name = "desNome"
        case subCategorie = "desSubcategoria"
        case grade = "numNota"
        case observation = "txtObs"
        case date = "dtaAgendamento"
    }
}
