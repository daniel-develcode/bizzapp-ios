import Foundation
import UIKit

protocol RegisterViewModelDelegate {
    
    func didRegisterWithSuccess(_ message:String,_ user:GeneralRegData)
    
    func didRegisterWithError(_ error:[String])
    
    func didPressRegisterFormBtn(_ sender:Any)
}

protocol RegisterViewModelProtocol{
    
    var delegate:RegisterViewModelDelegate?{get set}
    
    func register(_ user:RegUserDataTxt)
}

class RegUserFormViewModel:RegisterViewModelProtocol{
    
    var delegate:RegisterViewModelDelegate?
    var service:RegUserFormServiceProtocol
    
    init(_ registerService: RegUserFormServiceProtocol) {
        service = registerService
    }
    
    func register(_ user: RegUserDataTxt) {
        
        var errorBundle:[String] = []
        
        guard let validUser = validateInput(user) else {errorBundle.append("erro ao validar os dados");delegate?.didRegisterWithError(errorBundle);return}
        
        if (validUser.password == ""){
            errorBundle.append("É necessário preencher a senha.")
        }else if (validUser.password != validUser.conPassword){
            errorBundle.append("As senhas não são iguais.")
        }
        
        if (validUser.email == ""){
            errorBundle.append("É necessário preencher o email.")
        }else if (validUser.email != validUser.conEmail){
            errorBundle.append("Os emails não são iguais.")
        }
        
        if (validUser.name == ""){
            errorBundle.append("É necessário preencher seu nome.")
        }
        
        if (validUser.biNumber == ""){
            errorBundle.append("É necessário preencher o número bi.")
        }
        
        if (errorBundle.count < 1){
            service.register(validUser, { (user) in
                self.delegate?.didRegisterWithSuccess("registrado com sucesso.",user)
            }) { (error) in
                errorBundle.append(error)
                self.delegate?.didRegisterWithError(errorBundle)
            }
        }else{
            self.delegate?.didRegisterWithError(errorBundle)
        }
    }
    
    fileprivate func validateInput(_ user: RegUserDataTxt)-> RegUserDataTxt?{
        
        guard let validName = user.name else { return nil }
        guard let validBI = user.biNumber else { return nil }
        guard let validEmail = user.email else {return nil}
        guard let validConEmail = user.conEmail else {return nil}
        guard let validPassword = user.password else {return nil}
        guard let validConPassword = user.conPassword else {return nil}
        
        return RegUserDataTxt(
            name:validName,
            biNumber:validBI,
            email:validEmail,
            conEmail:validConEmail,
            password:validPassword,
            conPassword:validConPassword
        )
    }
}
