import UIKit
import Foundation

class TermsConditionsViewController: UIViewController, UITextViewDelegate {
    var previousView:RegUserFormViewController!
    var user:GeneralRegData!
    
    @IBOutlet weak var termsAcceptApperance: UIButton!{
        didSet {
            termsAcceptApperance.isEnabled = false
        }
    }
    
    @IBOutlet weak var termsConditionsText: UITextView!{
        didSet {
            termsConditionsText.delegate = self
        }
    }
    
    @IBAction func termsAcceptButton(_ sender: Any){
        if(previousView != nil){
            previousView.termsBtnIsEnabled = true
            self.dismiss(animated: true, completion: nil)
        }else if (user != nil){
            let storyboard: UIStoryboard = UIStoryboard(name: "MapPhone", bundle: nil)
            let phoneVC: PhoneViewController = storyboard.instantiateViewController(withIdentifier: "PhoneViewController") as! PhoneViewController
            phoneVC.user = user
            self.present(phoneVC, animated: true, completion: nil)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if((scrollView.contentOffset.y + scrollView.bounds.height > scrollView.contentSize.height)||(termsAcceptApperance.isEnabled == true)){
            termsAcceptApperance.isEnabled = true
            termsAcceptApperance.backgroundColor = UIColor.black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

