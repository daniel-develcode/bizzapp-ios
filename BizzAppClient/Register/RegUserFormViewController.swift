import UIKit
import BEMCheckBox
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase

class RegUserFormViewController: UIViewController,RegisterViewModelDelegate,UITextFieldDelegate,FacebookLoginViewModelDelegate{
    
    var viewModel:RegisterViewModelProtocol!
    var termsBtnIsEnabled:Bool = false
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var biNumberTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var conEmailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var conPasswordTxt: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var checkBoxBtn: BEMCheckBox!
    @IBOutlet weak var termsConditionsLbl: UILabel!
    @IBOutlet weak var nameError: UILabel!
    @IBOutlet weak var biError: UILabel!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var conEmailError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var conPasswordError: UILabel!
    var securePass = UIButton(type: .custom)
    var secureConPass = UIButton(type: .custom)
    var spinner:UIView!
    
    func didRegisterWithSuccess(_ message: String,_ user:GeneralRegData ) {
        
        print("Registrado com sucesso\(user.uid!)")
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MapPhone", bundle: nil)
        let phoneVC: PhoneViewController = storyboard.instantiateViewController(withIdentifier: "PhoneViewController") as! PhoneViewController
        phoneVC.user = user
        self.present(phoneVC, animated: true, completion: nil)
    }
    func didRegisterWithError(_ errorBundle: [String]) {
        print(errorBundle)
        nameError.isHidden = true
        biError.isHidden = true
        emailError.isHidden = true
        conEmailError.isHidden = true
        passwordError.isHidden = true
        conPasswordError.isHidden = true
        
        nameTxt.layer.borderColor = UIColor.lightGray.cgColor
        biNumberTxt.layer.borderColor = UIColor.lightGray.cgColor
        emailTxt.layer.borderColor = UIColor.lightGray.cgColor
        conEmailTxt.layer.borderColor = UIColor.lightGray.cgColor
        passwordTxt.layer.borderColor = UIColor.lightGray.cgColor
        conPasswordTxt.layer.borderColor = UIColor.lightGray.cgColor
        
        for err in errorBundle{
            if(err == "É necessário preencher seu nome."){
                nameTxt.layer.borderColor = UIColor.red.cgColor
                nameError.text = "Nome requerido."
                nameError.isHidden = false
            }
            
            if(err == "É necessário preencher o número bi."){
                biNumberTxt.layer.borderColor = UIColor.red.cgColor
                biError.text = "Número Bi requerido."
                biError.isHidden = false
            }
            
            if(err == "É necessário preencher o email."){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "Email requerido."
                emailError.isHidden = false
            }else if(err == "Os emails não são iguais."){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                conEmailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "Os emails não são iguais."
                conEmailError.text = "Os emails não são iguais."
                emailError.isHidden = false
                conEmailError.isHidden = false
            }else if(err == "O email já está em uso."){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                conEmailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "O email já está em uso."
                conEmailError.text = "O email já está em uso."
                emailError.isHidden = false
                conEmailError.isHidden = false
            }else if(err == "Email inválido."){
                emailTxt.layer.borderColor = UIColor.red.cgColor
                conEmailTxt.layer.borderColor = UIColor.red.cgColor
                emailError.text = "Email inválido."
                conEmailError.text = "Email inválido."
                emailError.isHidden = false
                conEmailError.isHidden = false
            }
            
            if(err == "É necessário preencher a senha."){
                passwordTxt.layer.borderColor = UIColor.red.cgColor
                passwordError.text = "Senha requerida."
                passwordError.isHidden = false
            }else if(err == "As senhas não são iguais."){
                passwordTxt.layer.borderColor = UIColor.red.cgColor
                conPasswordTxt.layer.borderColor = UIColor.red.cgColor
                passwordError.text = "As senhas não são iguais."
                conPasswordError.text = "As senhas não são iguais."
                passwordError.isHidden = false
                conPasswordError.isHidden = false
            }else if (err == "A senha deve possuir no mínimo 6 caracteres."){
                passwordTxt.layer.borderColor = UIColor.red.cgColor
                conPasswordTxt.layer.borderColor = UIColor.red.cgColor
                passwordError.text = "A senha deve possuir no mínimo 6 caracteres."
                conPasswordError.text = "A senha deve possuir no mínimo 6 caracteres."
                passwordError.isHidden = false
                conPasswordError.isHidden = false
            }
            
            if (err == "Dados inválidos."){
                self.alertMessage(titleAlert: "Erro", messageAlert: "Dados inválidos.", buttonAlert: "OK")
            }
        }
    }
    
    @IBAction func didPressRegisterFormBtn(_ sender: Any) {
        let user = RegUserDataTxt(
            name:nameTxt.text,
            biNumber:biNumberTxt.text,
            email:emailTxt.text,
            conEmail:conEmailTxt.text,
            password:passwordTxt.text,
            conPassword:conPasswordTxt.text
        )
        if (checkBoxBtn.on == true){
            viewModel.register(user)
        }else{
            self.alertMessage(titleAlert: "Erro", messageAlert: "você deve aceitar os termos e codições", buttonAlert: "OK")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        navigationController?.isNavigationBarHidden = false;
        return false
    }
    
    @IBAction func termsBtn(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "RegUserForm", bundle: nil)
        let vc: TermsConditionsViewController = storyboard.instantiateViewController(withIdentifier: "TermsConditionsViewController") as! TermsConditionsViewController
        vc.previousView = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func returnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(termsBtnIsEnabled == false){
            checkBoxBtn.isEnabled = false
        }else{
            checkBoxBtn.isEnabled = true
            nextBtn.isEnabled = true
            checkBoxBtn.on = true
            termsConditionsLbl.textColor = UIColor.black
        }
    }
    
    @IBAction func facebookLoginBtn(_ sender: Any) {
        spinner = UIViewController.displaySpinner(onView:self.view)
        let faceBookViewModel = FacebookLoginViewModel(AppDelegate.container.resolve(FacebookLoginServiceProtocol.self)!)
        faceBookViewModel.delegate = self
        faceBookViewModel.loginWithFacebook(self)
    }
    
    func loginWithFacebookSuccess(_ successMessage: String) {
        print(successMessage)
        UIViewController.removeSpinner(spinner)
    }
    
    func loginWithFacebookError(_ errorMessage: String) {
        UIViewController.removeSpinner(spinner)
        self.alertMessage(titleAlert: "Erro", messageAlert: errorMessage, buttonAlert: "OK")
    }
    
    @IBAction func passwordTxtRefresh(_ sender: Any) {
        if (passwordTxt.isSecureTextEntry ==  true){
            passwordTxt.isSecureTextEntry =  false
            securePass.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            passwordTxt.isSecureTextEntry =  true
            securePass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    @IBAction func conPasswordTxtRefresh(_ sender: Any) {
        if (conPasswordTxt.isSecureTextEntry ==  true){
            conPasswordTxt.isSecureTextEntry =  false
             secureConPass.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            conPasswordTxt.isSecureTextEntry =  true
            secureConPass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    private func configSecureEntryPassword(_ txt:UITextField){
        securePass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        securePass.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        securePass.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        securePass.addTarget(self, action: #selector(self.passwordTxtRefresh), for: .touchUpInside)
        txt.rightView = securePass
        txt.rightViewMode = .always
    }
    
    private func configSecureEntryConPassword(_ txt:UITextField){
        secureConPass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        secureConPass.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        secureConPass.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        secureConPass.addTarget(self, action: #selector(self.conPasswordTxtRefresh), for: .touchUpInside)
        txt.rightView = secureConPass
        txt.rightViewMode = .always
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(termsBtnIsEnabled == false){
            checkBoxBtn.isEnabled = false
            
        }else{
            checkBoxBtn.isEnabled = true
            nextBtn.isEnabled = true
            checkBoxBtn.on = true
            termsConditionsLbl.textColor = UIColor.black
        }
        let viewModel = RegUserFormViewModel(AppDelegate.container.resolve(RegUserFormServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        nameTxt.delegate = self
        biNumberTxt.delegate = self
        emailTxt.delegate = self
        conEmailTxt.delegate = self
        passwordTxt.delegate = self
        conPasswordTxt.delegate = self
        configSecureEntryConPassword(conPasswordTxt)
        configSecureEntryPassword(passwordTxt)
    }
}
