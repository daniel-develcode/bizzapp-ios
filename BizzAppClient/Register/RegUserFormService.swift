import Foundation
import Alamofire
import Firebase

struct RegUserDataTxt {
    var name:String?
    var biNumber:String?
    var email:String?
    var conEmail:String?
    var password:String?
    var conPassword:String?
}

protocol RegUserFormServiceProtocol{
    
    typealias OnCreateSuccess = ((String) -> Void)
    typealias OnCreateError = ((String) -> Void)
    
    typealias OnRegCallbackSuccess = ((GeneralRegData) -> Void)
    typealias OnRegCallbackError = ((String) -> Void)
    
    typealias OnSaveSuccess = ((UserData) -> Void)
    typealias OnSaveError = ((String) -> Void)
    
    func register(
        _ user:RegUserDataTxt,
        _ success:@escaping OnRegCallbackSuccess,
        _ error:@escaping OnRegCallbackError
    )
}

class RegUserFormService:RegUserFormServiceProtocol{
    
    var service:RequestServiceProtocol
    init(_ requestService: RequestServiceProtocol) {
        service = requestService
    }
    
    func register(_ user: RegUserDataTxt,
                  _ success: @escaping OnRegCallbackSuccess,
                  _ error: @escaping OnRegCallbackError) {
        
        saveUser(user, { (userData) in
            UserDefaults.standard.set(userData.token, forKey: "userToken")
            UserDefaults.standard.set(userData.uid, forKey: "userUid")
            UserDefaults.standard.set(userData.userCode, forKey: "userCode")
            UserDefaults.standard.set(user.name, forKey: "userName")
            let userForm = GeneralRegData(
                name: userData.name,
                biNumber:user.biNumber!,
                email: userData.email,
                uid: userData.uid,
                adress: "",
                reference:"",
                phone: "",
                token:userData.token,
                numLatitude:0,
                numLongitude:0)
            
            success(userForm)
        }) { (errorR) in
            error(errorR)
        }
    }
    
    private func saveUser(_ user:RegUserDataTxt,
                          _ success: @escaping OnSaveSuccess,
                          _ error: @escaping OnSaveError){
        
        createFirebaseUser(user, { (uid) in
           
            let bundle = Bundle.main.infoDictionary
            let Api_Url = bundle!["API_URL"] as! String? ?? ""
            let url = Api_Url+"auth/public/insert-user"
            
            let header:HTTPHeaders = [
                "Accept-Encoding":"gzip",
                "User-Agent":"gzip"
            ]
            
            let param = self.getParam(user, uid)
            self.service.request(url,.post,param,header, { (regUserData) in
                success(Decoders.decodeData(regUserData!))
            }, { (regUserError) in
                self.DeleteUser()
                error(regUserError)
            })
        }) { (AuthError) in
            error(AuthError)
        }
    }
    
    private func getParam(_ user:RegUserDataTxt, _ uid:String) -> Parameters{
        let application = UIApplication.shared.delegate as! AppDelegate
        let token = application.getNotificationToken()!
        let parameters:Parameters = [
            "chvUsuario":uid,
            "desEmail":user.email!,
            "desNome":user.name!,
            "desNif":"tstdev",
            "desBi":user.biNumber!,
            "desTelefonePrin":"",
            "desStringEndereco":"",
            "indAceite":1,
            "device":[
                "numImei":"123456789",
                "numId":token,
                "tipApp":1,
                "tipSystem":2
            ]
            ] as [String : Any]
        return parameters
    }
    
    private func DeleteUser(){
        let auth = Auth.auth()
        auth.currentUser?.delete(completion: { (Error) in
            if(Error != nil){
                print(Error!)
            }
        })
    }
    
    private func createFirebaseUser(_ user:RegUserDataTxt,
                                    _ success: @escaping OnCreateSuccess,
                                    _ error: @escaping OnCreateError){
        let auth = Auth.auth()
        auth.createUser(withEmail: user.email!,password:user.password!) { (userC, errorC) in
            
            if (errorC == nil){
                success((auth.currentUser?.uid)!)
                return
            }else{
                let recError = errorC! as NSError
                let errorCode = recError.userInfo["error_name"] as! String
                switch errorCode {
                case "ERROR_WEAK_PASSWORD":
                    error("A senha deve possuir no mínimo 6 caracteres.")
                    break
                    
                case "ERROR_EMAIL_ALREADY_IN_USE":
                    error("O email já está em uso.")
                    break
                    
                case "ERROR_INVALID_EMAIL":
                    error("Email inválido.")
                    break
                default:
                    error( "Dados inválidos.")
                }
            }
        }
    }
}
