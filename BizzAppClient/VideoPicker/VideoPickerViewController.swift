    import UIKit
    import AVFoundation
    import Foundation
    import Firebase
    import AVKit
    import MobileCoreServices
    
    protocol VideoReceiverDelegate {
        var videoLocalUrl:URL?{get set}
        func videoUploadProgress(_ progress:Double,_ videoReference:String)
        func videoUploadSucess(_ downloadURL:String,_ videoReference:String)
        func videoUploadError(_ errorMessage:String)
    }
    
    class VideoPickerViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UploadVideoDelegate {
        
        var videoReceiverDelegate:VideoReceiverDelegate?
        var seqSol:Int?
        var saveFileName:String!
        let imagePicker:UIImagePickerController! = UIImagePickerController()
        
        @IBAction func pickFromCameraBtn(_ sender: Any) {
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = [kUTTypeMovie as String]
                    imagePicker.allowsEditing = false
                    imagePicker.delegate = self
                    
                    present(imagePicker, animated: true, completion: {})
                } else {
                    print("Rear camera doesn't exist,Application cannot access the camera.")
                }
            } else {
                print("Camera inaccessable,Application cannot access the camera.")
            }
        }
        
        @IBAction func pickFromGaleryBtn(_ sender: Any) {
            let video = UIImagePickerController()
            video.delegate = self
            video.sourceType = .photoLibrary
            video.allowsEditing = false
            video.mediaTypes = ["public.movie"]
            self.present(video, animated: true)
        }
        
        @IBAction func cancelBtn(_ sender: Any) {
            self.view.removeFromSuperview()
        }
        
        func uploadVideoSuccess(_ videoUrl: String, _ videoReference: String) {
            self.videoReceiverDelegate?.videoUploadSucess(videoUrl, videoReference)
        }
        
        func uploadVideoError(_ errorMessage: String) {
            self.videoReceiverDelegate?.videoUploadError(errorMessage)
        }
        
        func uploadVideoProgress(_ progress: Double,_ reference:String) {
            self.videoReceiverDelegate?.videoUploadProgress(progress, reference)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
            
            print("Got a video")
           
            if let pickedVideo:URL = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL) {
                let selectorToCall = #selector(self.videoWasSavedSuccessfully(_:didFinishSavingWithError:context:))
                UISaveVideoAtPathToSavedPhotosAlbum(pickedVideo.relativePath, self, selectorToCall, nil)
                let videoData = try? Data(contentsOf: pickedVideo)
                let uploadVidServ = UploadVideoService(self)
                let uploadTime = String(CACurrentMediaTime())
                let reference = "scheduling_gallery/"+String(seqSol!)+"/video_"+uploadTime
                uploadVidServ.uploadVideo(videoData!,reference)
                let paths = NSSearchPathForDirectoriesInDomains(
                    FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
                videoReceiverDelegate?.videoLocalUrl = pickedVideo 
                let dataPath = documentsDirectory.appendingPathComponent(saveFileName)
                try! videoData?.write(to: dataPath, options: [])
                print("Saved to " + dataPath.absoluteString)
            }
            self.dismiss(animated: true, completion: nil)
            self.view.removeFromSuperview()
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            print("User canceled image")
            dismiss(animated: true, completion: {
            })
        }
        
        @objc func videoWasSavedSuccessfully(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer){
            if let theError = error {
                print("An error happened while saving the video = \(theError)")
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        }
        
        private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
            return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
        }
        
        private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
            return input.rawValue
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            saveFileName = "/video_"+String(seqSol!)+".mp4"
        }
    }
