import Foundation
import UIKit
import Firebase

protocol UploadVideoDelegate {
    func uploadVideoSuccess(_ videoUrl:String,_ videoReference:String)
    func uploadVideoError(_ errorMessage:String)
    func uploadVideoProgress(_ progress:Double,_ videoReference:String)
}

protocol UploadVideoProtocol{
    var delegate:UploadVideoDelegate{get set}
    
    func uploadVideo(_ video:Data,_ reference:String)
}

class UploadVideoService:UploadVideoProtocol{
    var delegate: UploadVideoDelegate
    
    private var reference:String!
    
    init(_ delegate:UploadVideoDelegate) {
        self.delegate = delegate
    }
    
    func uploadVideo(_ video:Data,_ reference:String) {
        self.reference = reference
        var data = Data()
        data = video
        var riversRef:StorageReference?
        riversRef = Storage.storage().reference().child(reference)
        let metaData = StorageMetadata()
        metaData.contentType = "video/mp4"
        
        let uploadTask = riversRef!.putData(data, metadata: metaData) { (metadata, error) in
            guard metadata != nil else {
                self.delegate.uploadVideoError("Ocorreu um erro ao fazer upload")
                return
            }
            riversRef!.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    self.delegate.uploadVideoError("Ocorreu um erro ao fazer upload")
                    return
                }
                let stringURL = downloadURL.absoluteString
                self.delegate.uploadVideoSuccess(stringURL, self.reference)
            }
        }
        uploadTask.observe(.progress) { snapshot in
            self.delegate.uploadVideoProgress((snapshot.progress?.fractionCompleted)!,self.reference)
        }
    }
}
