import Foundation
import UIKit
import FBSDKLoginKit
import Firebase
import Alamofire

protocol FacebookLoginServiceProtocol {
    typealias IsNewUser = Bool
    typealias OnFacebookLoginCallbackSuccess = ((UserData,IsNewUser) -> Void)
    typealias OnFacebookLoginCallbackError = ((String) -> Void)
    
    func facebookLoginService (
        _ viewController:UIViewController,
        _ success: @escaping OnFacebookLoginCallbackSuccess,
        _ error: @escaping OnFacebookLoginCallbackError)
}

class FacebookLoginService:FacebookLoginServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ requestService: RequestServiceProtocol) {
        self.service = requestService
    }
    
    func facebookLoginService(_ viewController: UIViewController,
                              _ success: @escaping OnFacebookLoginCallbackSuccess,
                              _ error: @escaping OnFacebookLoginCallbackError) {
        
        let manager = LoginManager()
        manager.logOut()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            manager.logIn(permissions: ["public_profile","email"], from: viewController) { (result, err) in
                if(err == nil){
                    self.signIntoFB({ (user,isNewUser)  in
                        success(user,isNewUser)
                    }, { (err) in
                        error(err)
                    })
                }else{
                    error("Erro ao continuar com o facebook.")
                }
            }
        }
    }
    
    private func signIntoFB(_ success: @escaping OnFacebookLoginCallbackSuccess,
                            _ error: @escaping OnFacebookLoginCallbackError){
        
        guard let authToken = AccessToken.current?.tokenString else {error("Erro ao logar com o Facebook");return}
        let credential = FacebookAuthProvider.credential(withAccessToken: authToken)
        let auth = Auth.auth()
        auth.signInAndRetrieveData(with: credential) { (data, err) in
            
            if(err == nil){
                let email = auth.currentUser?.email
                let name = auth.currentUser?.displayName
                let uid = auth.currentUser?.uid
                
                if (data?.additionalUserInfo?.isNewUser == true){
                    let bundle = Bundle.main.infoDictionary
                    let Api_Url = bundle!["API_URL"] as! String? ?? ""
                    let url = Api_Url+"auth/public/insert-user"
                    let param = self.getParam(name!, email!, uid!)
                    
                    let header:HTTPHeaders = [
                        "Accept-Encoding":"gzip",
                        "User-Agent":"gzip"
                    ]
                    
                    self.service.request(url,.post,param,header, { (regUserData) in
                        success(Decoders.decodeData(regUserData!),true)
                    }, { (regUserError) in
                        self.DeleteUser()
                        error(regUserError)
                    })
                }else{
                    let bundle = Bundle.main.infoDictionary
                    let Api_Url = bundle!["API_URL"] as! String? ?? ""
                    let url = Api_Url+"auth/public/auth-user"
                    let application = UIApplication.shared.delegate as! AppDelegate
                    let token = application.getNotificationToken()!
                    let parameters:Parameters = [
                        "chvUsuario":uid!,
                        "desEmail":email!,
                        "device":[
                            "numImei":"123456789",
                            "numId":token,
                            "tipApp":1,
                            "tipSystem":2]
                        ] as [String : Any]
                    
                    let header:HTTPHeaders = [
                        "Accept-Encoding":"gzip",
                        "User-Agent":"gzip"
                    ]
                    
                    self.service.request(url,.post,parameters,header,
                                         { (dataR) in
                                            let decoded = Decoders.decodeData(dataR!) as UserData
                                            print(decoded.token!)
                                            UserDefaults.standard.set(decoded.email, forKey: "lastLogged")
                                            UserDefaults.standard.set(decoded.latitude, forKey: "userLatitude")
                                            UserDefaults.standard.set(decoded.longitude, forKey: "userLongitude")
                                            UserDefaults.standard.set(decoded.token, forKey: "userToken")
                                            UserDefaults.standard.set(decoded.userCode, forKey: "userCode")
                                            UserDefaults.standard.set(decoded.uid, forKey: "userUid")
                                            UserDefaults.standard.set(decoded.name, forKey: "userName")
                                            
                                            let adress = SolicAdress(
                                                latitude:decoded.latitude,
                                                longitude:decoded.longitude,
                                                adress:decoded.adress,
                                                reference:decoded.reference)
                                            self.validateSolAdress(adress)
                                            success(decoded,false)},
                                         { (requestError) in
                                            error(requestError)})
                }
            }else{
                let recError = err! as NSError
                let errorCode = recError.userInfo["error_name"] as! String
                switch errorCode {
                case "ERROR_WEAK_PASSWORD":
                    error("A senha deve possuir no mínimo 6 caracteres.")
                    break
                    
                case "ERROR_EMAIL_ALREADY_IN_USE":
                    error("O email já está em uso.")
                    break
                    
                case "ERROR_INVALID_EMAIL":
                    error("Email inválido.")
                    break
                    
                case "ERROR_INVALID_CREDENTIAL":
                    error("O email já está em uso.")
                    break
                case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                    error("Email já está em uso por outra conta com credenciais diferentes.")
                    break
                default:
                    error( "Dados inválidos.")
                }
                error("erro ao logar com o facebook")
            }
        }
    }
    
    private func getParam(_ userName:String,
                          _ userEmail:String,
                          _ uid:String) -> Parameters{
        let application = UIApplication.shared.delegate as! AppDelegate
        let token = application.getNotificationToken()!
        let parameters:Parameters = [
            "chvUsuario":uid,
            "desEmail":userEmail,
            "desNome":userName,
            "desNif":"tstdev",
            "desBi":"0",
            "desTelefonePrin":"",
            "desStringEndereco":"",
            "indAceite":1,
            "device":[
             "numImei":"123456789",
             "numId":token,
             "tipApp":1,
             "tipSystem":2]
            ] as [String : Any]
        return parameters
    }
    
    private func validateSolAdress(_ adress:SolicAdress){
        guard let _ = UserDefaults.standard.object(forKey: "SolLatitude") as? Double else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolLongitude") as? Double else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolAdress") as? String else {saveDefaultSolAdress(adress);return}
        guard let _ = UserDefaults.standard.object(forKey: "SolReference") as? String else {saveDefaultSolAdress(adress);return}
    }
    
    private func saveDefaultSolAdress(_ adress:SolicAdress){
        UserDefaults.standard.set(adress.adress, forKey: "SolAdress")
        UserDefaults.standard.set(adress.latitude, forKey: "SolLatitude")
        UserDefaults.standard.set(adress.longitude, forKey: "SolLongitude")
        UserDefaults.standard.set(adress.reference, forKey: "SolReference")
    }

    
    private func DeleteUser(){
        let auth = Auth.auth()
        auth.currentUser?.delete(completion: { (Error) in
            if(Error != nil){
                print(Error!)
            }
        })
    }
}
