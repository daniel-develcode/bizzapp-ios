import Foundation
import FBSDKLoginKit
import UIKit

protocol FacebookLoginViewModelDelegate{
    func loginWithFacebookSuccess(_ successMessage:String)
    func loginWithFacebookError(_ errorMessage:String)
}

protocol FacebookLoginViewModelProtocol {
    func loginWithFacebook(_ viewController:UIViewController)
    
    var delegate:FacebookLoginViewModelDelegate?{get set}
}

class FacebookLoginViewModel:FacebookLoginViewModelProtocol {
    
    var delegate: FacebookLoginViewModelDelegate?
    var service:FacebookLoginServiceProtocol
    
    
    init(_ service: FacebookLoginServiceProtocol) {
        self.service = service
    }
    
    func loginWithFacebook(_ viewController: UIViewController) {
        service.facebookLoginService(viewController, { (user, isNewUser) in
            UserDefaults.standard.set(user.token, forKey: "userToken")
            UserDefaults.standard.set(user.uid, forKey: "userUid")
            UserDefaults.standard.set(user.userCode, forKey: "userCode")
            if(isNewUser == true){
                self.delegate?.loginWithFacebookSuccess("logado com sucesso")
                let storyboard: UIStoryboard = UIStoryboard(name: "RegUserForm", bundle: nil)
                let vc: TermsConditionsViewController = storyboard.instantiateViewController(withIdentifier: "TermsConditionsViewController") as! TermsConditionsViewController
                let validUser = GeneralRegData(
                    name:user.name,
                    biNumber:"",
                    email:user.email,
                    uid:user.uid,
                    adress:"",
                    reference:"",
                    phone:"",
                    token:user.token,
                    numLatitude:0,
                    numLongitude:0)
                vc.user = validUser
                viewController.present(vc, animated: true, completion: nil)
            }else{
                let manager = LoginManager()
                manager.logOut()
                self.delegate?.loginWithFacebookSuccess("logado com sucesso")
                self.validateSearch(viewController)
            }
        }) { (err) in
            self.delegate?.loginWithFacebookError(err)
        }
    }
    
    private func validateSearch(_ viewController: UIViewController){
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as? Int
        let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as? Int
        let catName = UserDefaults.standard.object(forKey:"categoryName") as? String
        let subcatName = UserDefaults.standard.object(forKey:"subCategoryName") as? String
        let catImageStr = UserDefaults.standard.object(forKey:"categoryImage") as? String
        
        if  ((catCode == nil)||(subcatCode == nil)||(catName == nil)||(subcatName == nil)||(catImageStr == nil)){
            let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
            let vc: SelectCategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
            viewController.present(vc, animated: true, completion: nil)
        }else{
            let storyboard: UIStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
            let tabBarVC: TabBarViewController = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            viewController.present(tabBarVC, animated: true, completion: nil)
        }
    }
}
