import Foundation
import UIKit
import Firebase

protocol UploadImageDelegate {
    func uploadImageSuccess(_ successMessage:String,_ imageReference:String)
    func uploadImageError(_ errorMessage:String)
    func uploadImageProgress(_ progress:Double,_ reference:String)
}

protocol UploadImageProtocol{
    var delegate:UploadImageDelegate{get set}
    func uploadImage(_ image:UIImage,_ reference:String)
}

class uploadImageService:UploadImageProtocol{
    var delegate: UploadImageDelegate
    var reference:String!
    
    init(_ delegate:UploadImageDelegate) {
        self.delegate = delegate
    }
    
    func uploadImage(_ image:UIImage,_ reference:String) {
        self.reference = reference
        let imageData = image
        var data = Data()
        data = imageData.jpegData(compressionQuality: 0)! as Data
        var riversRef:StorageReference?
        riversRef = Storage.storage().reference().child(reference)
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        let uploadTask = riversRef!.putData(data, metadata: metaData) { (metadata, error) in
            guard metadata != nil else {
                self.delegate.uploadImageError("Ocorreu um erro ao fazer upload")
                return
            }
            riversRef!.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    self.delegate.uploadImageError("Ocorreu um erro ao fazer upload")
                    return
                }
                let stringURL = downloadURL.absoluteString
                self.delegate.uploadImageSuccess(stringURL,reference)
            }
        }
        uploadTask.observe(.progress) { snapshot in
            self.delegate.uploadImageProgress((snapshot.progress?.fractionCompleted)!, reference)
        }
    }
}
