import UIKit
import Foundation
import Firebase


protocol ImageReceiverDelegate {
    var importedImage:UIImage?{get set}
    func imageUploadProgress(_ progress:Double,_ imageReference:String)
    func imageUploadSucess(_ downloadURL:String,_ imageReference:String)
    func imageUploadError(_ errorMessage:String)
}

class ImagePickerViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UploadImageDelegate {
    
    
    var imageReceiverDelegate:ImageReceiverDelegate?
    var seqSol:Int?
    var userCode:Int?
    
    @IBAction func pickFromCameraBtn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.camera
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    @IBAction func pickFromGaleryBtn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    func uploadImageSuccess(_ successMessage: String,_ imageReference:String) {
        self.imageReceiverDelegate!.imageUploadSucess(successMessage,imageReference)
    }
    
    func uploadImageError(_ errorMessage: String) {
        self.imageReceiverDelegate!.imageUploadError(errorMessage)
    }
    
    func uploadImageProgress(_ progress: Double, _ reference:String) {
        self.imageReceiverDelegate!.imageUploadProgress(progress,reference)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            if (userCode != nil){
                imageReceiverDelegate?.importedImage = image
                let uploadImage = uploadImageService(self)
                let uploadTime = String(CACurrentMediaTime())
                let reference = "cli_profile_photo/"+String(userCode!)+"/image_"+uploadTime
                uploadImage.uploadImage(image,reference)
            }else if(seqSol != nil){
                imageReceiverDelegate?.importedImage = image
                let uploadImage = uploadImageService(self)
                let uploadTime = String(CACurrentMediaTime())
                let reference = "scheduling_gallery/"+String(seqSol!)+"/image_"+uploadTime
                uploadImage.uploadImage(image,reference)
        }
    }
    else
    {
    self.imageReceiverDelegate?.imageUploadError("Ocorreu um erro inesperado")
    }
    self.dismiss(animated: true, completion: nil)
    self.view.removeFromSuperview()
}

override func viewDidLoad() {
    super.viewDidLoad()
}
}
