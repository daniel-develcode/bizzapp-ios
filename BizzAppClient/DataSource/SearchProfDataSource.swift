import Foundation
import UIKit
import Alamofire
import AlamofireImage

class SearchProfDataSource:NSObject, UITableViewDataSource{
    
    private var profs:[ProfCellData]
    private var vc:SearchProfViewController
    var textToFind:String = ""
    
    init(_ profs:[ProfCellData],_ vc:SearchProfViewController){
        self.profs = profs
        self.vc = vc
    }
    
    func reload(_ profs: [ProfCellData]) {
        self.profs = profs
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (vc.pageCount >= vc.nextPage){
            return profs.count+1
        }else{
            return profs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < profs.count){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "profsCell")! as? ProfCell else { return UITableViewCell()}
            let str = NSString(string: profs[indexPath.row].name.lowercased())
            let myRange = str.range(of: textToFind.lowercased())
            let myString = NSMutableAttributedString(string: profs[indexPath.row].name)
            myString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: myRange)
            cell.profName.attributedText = myString
            if(profs[indexPath.row].indDisp == 1){
                cell.profDisp.isHidden = false
            }else{
                cell.profDisp.isHidden = true
            }
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(profs[indexPath.row].profImageURL, { (image,url) in
                UIView.transition(with: cell, duration: 0.3, options: .transitionCrossDissolve, animations: {cell.profImage.image = image}, completion: nil)
            }) { (error) in
                print(error)
            }
            cell.profGrade.text = profs[indexPath.row].grade+"/5"
            let formattedDistance = profs[indexPath.row].distance.components(separatedBy: ".")[0]+","+profs[indexPath.row].distance.components(separatedBy: ".")[1]
            cell.profDistance.text = formattedDistance
            return cell
        }else if (indexPath.row == profs.count){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchMoreProf")! as? SearchMoreProf else { return UITableViewCell()}
            cell.vc = vc
            return cell
        }
        return UITableViewCell()
    }
}

class ProfCell:UITableViewCell{
    
    @IBOutlet weak var profName: UILabel!
    @IBOutlet weak var profDisp: UILabel!
    @IBOutlet weak var profGrade: UILabel!
    @IBOutlet weak var profDistance: UILabel!
    @IBOutlet weak var profImage: UIImageView!
    
    override func prepareForReuse() {
        profImage.image = #imageLiteral(resourceName: "DefaultProfile")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class SearchMoreProf:UITableViewCell{
    var vc:SearchProfViewController!
    
    @IBAction func moreBtn(_ sender: Any) {
        vc.viewModel.getProf(vc.nextPage)
    }
}
