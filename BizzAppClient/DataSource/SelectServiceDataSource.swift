import Foundation
import UIKit

class SelectServiceDataSource:NSObject,UITableViewDataSource{
    var outputSubCats:[ProfServParam]
    var selectedRow:Int
    var selected:Bool
    
    init(_ outputSubCats:[ProfServParam],_ selectedRow:Int,_ selected:Bool) {
        self.outputSubCats = outputSubCats
        self.selectedRow = selectedRow
        self.selected = selected
    }
    
    func reloadData(_ outputSubCats:[ProfServParam],_ selectedRow:Int,_ selected:Bool){
        self.outputSubCats = outputSubCats
        self.selectedRow = selectedRow
        self.selected = selected
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return outputSubCats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelectServiceCell")! as? SelectServiceCell else { return UITableViewCell()}
        cell.serviceName.text = ((outputSubCats[indexPath.row].desServ))
        let priceNbr = outputSubCats[indexPath.row].price!
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        let formattedPriceStr = formatter.string(from: priceNbr as NSNumber) ?? "0"
        cell.servicePrice.text = (formattedPriceStr + " / " + (outputSubCats[indexPath.row].codeUm!))
        cell.hexagonoImage.image = UIImage.init(named:"radio-off")
        if ((selectedRow == indexPath.row) && (selected == true)){
            cell.hexagonoImage.image = UIImage.init(named: "radio-on")
        }
        return cell
    }
}
