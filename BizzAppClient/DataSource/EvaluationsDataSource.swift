import Foundation
import UIKit

class EvaluationsDataSource:NSObject,UITableViewDataSource{
    
    var thisView:UIView
    var viewModel:EvaluationsViewModelProtocol
    var evalDone:[EvaluationsDone]
    var evalToDo:[EvaluationsToDo]
    var evalVC:EvaluationsViewController
    var evalSectLbl:[String] = ["       AVALIAR","       PROFISSIONAIS AVALIADOS POR VOCÊ"]
    
    init(_ evalDone:[EvaluationsDone],_ evalToDo:[EvaluationsToDo],_ viewModel:EvaluationsViewModelProtocol,_ evalVC:EvaluationsViewController){
        self.thisView = evalVC.view
        self.evalVC = evalVC
        self.evalDone = evalDone
        self.evalToDo = evalToDo
        self.viewModel = viewModel
    }
    
    func reload(_ evalDone:[EvaluationsDone],_ evalToDo:[EvaluationsToDo]) {
        self.evalDone = evalDone
        self.evalToDo = evalToDo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            if (indexPath.row < evalToDo.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvalToDoCell")! as? EvalToDoCell else { return UITableViewCell()}
                cell.nameLbl.text = evalToDo[indexPath.row].name
                cell.desServ.text = evalToDo[indexPath.row].subCategorie
                if let url = URL(string: evalToDo[indexPath.row].image) {
                    
                    cell.profImage?.af_setImage(withURL:url, placeholderImage: nil, filter: nil,  imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: {response in
                        cell.profImage.image = cell.profImage.image?.resized(toWidth: 45)
                    })
                }
                let formattedday = (evalToDo[indexPath.row].date.components(separatedBy: "-")[2]).components(separatedBy:"T").first
                let formattedMonth = evalToDo[indexPath.row].date.components(separatedBy: "-")[1]
                let formattedYear = evalToDo[indexPath.row].date.components(separatedBy: "-")[0]
                let formattedDate = " "+formattedday!+"/"+formattedMonth+"/"+formattedYear
                cell.dateDone.text = formattedDate
                cell.evalViewModel = viewModel
                cell.thisView = evalVC
                cell.params = SaveEvalParams(
                    seqSolic:evalToDo[indexPath.row].seqSolic,
                    profCode:evalToDo[indexPath.row].profCode,
                    grade:0,
                    txtObs:"",
                    index:indexPath.row)
                return cell
            }else if (indexPath.row == evalToDo.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoreEvalCell")! as? MoreEvalCell else { return UITableViewCell()}
                cell.vc = evalVC
                cell.evalType = 0
                return cell
            }
        }else{
            if(indexPath.row < evalDone.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EvalDoneCell")! as? EvalDoneCell else { return UITableViewCell()}
                cell.nameLbl.text = evalDone[indexPath.row].name
                cell.obsLbl.text = evalDone[indexPath.row].observation
                cell.desServ.text = evalDone[indexPath.row].subCategorie
                let formattedday = (evalDone[indexPath.row].date.components(separatedBy: "-")[2]).components(separatedBy:"T").first
                let formattedMonth = evalDone[indexPath.row].date.components(separatedBy: "-")[1]
                let formattedYear = evalDone[indexPath.row].date.components(separatedBy: "-")[0]
                let formattedDate = " "+formattedday!+"/"+formattedMonth+"/"+formattedYear
                cell.dateLbl.text = formattedDate
                if (evalDone[indexPath.row].grade < 5) {
                    cell.star5.image = #imageLiteral(resourceName: "avaliacao-off@512h")
                }
                if (evalDone[indexPath.row].grade < 4) {
                    cell.star4.image = #imageLiteral(resourceName: "avaliacao-off@512h")
                }
                if (evalDone[indexPath.row].grade < 3) {
                    cell.star3.image = #imageLiteral(resourceName: "avaliacao-off@512h")
                }
                if (evalDone[indexPath.row].grade < 2) {
                    cell.star2.image = #imageLiteral(resourceName: "avaliacao-off@512h")
                }
                if (evalDone[indexPath.row].grade < 1) {
                    cell.star1.image = #imageLiteral(resourceName: "avaliacao-off@512h")
                }
                
                let application = UIApplication.shared.delegate as! AppDelegate
                let downloader = application.getImageDownloader()
                downloader.downloadImage(evalDone[indexPath.row].image , { (image,url) in
                    UIView.transition(with: cell, duration: 0.3, options: .transitionCrossDissolve, animations: {cell.profImage.image = image}, completion: nil)
                }) { (error) in
                    print(error)
                }
                return cell
            }else if (indexPath.row == evalDone.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoreEvalCell")! as? MoreEvalCell else { return UITableViewCell()}
                cell.vc = evalVC
                cell.evalType = 1
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return evalSectLbl[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            if (evalVC.evalToDoPageCount >= evalVC.evalToDoNextPage){
                return (evalToDo.count+1)
            }else{
                return (evalToDo.count)
            }
        }else{
            if (evalVC.evalDonePageCount >= evalVC.evalDoneNextPage){
                return (evalDone.count+1)
            }else{
                return (evalDone.count)
            }
        }
    }
}
