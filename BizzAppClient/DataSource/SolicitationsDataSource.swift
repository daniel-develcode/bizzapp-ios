import Foundation
import UIKit

class SolicitationDataSource:NSObject,UITableViewDataSource{
    
    var solicSectLbl:[String] = ["       SERVIÇOS AGENDADOS","       SERVIÇOS FINALIZADOS"]
    var solicAct:[Solicitations]
    var solicInact:[Solicitations]
    var vc:SolicitationsViewController
    
    init(_ solicAct:[Solicitations],_ solicInact:[Solicitations],_ vc:SolicitationsViewController) {
        self.solicAct = solicAct
        self.solicInact = solicInact
        self.vc = vc
    }
    func reload(_ solicAct:[Solicitations],_ solicInact:[Solicitations]){
        self.solicAct = solicAct
        self.solicInact = solicInact
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            if (indexPath.row < solicAct.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SolicActCell") as? SolicActiveClass else { return UITableViewCell()}
                if let url = URL(string: solicAct[indexPath.row].profImage ?? "") {
                    cell.profImage?.af_setImage(withURL:url, placeholderImage: nil, filter: nil,  imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: true, completion: {response in
                        cell.profImage.image = cell.profImage.image?.resized(toWidth: 45)
                    })}
                cell.name.text = solicAct[indexPath.row].profName
                cell.grade.text = String(solicAct[indexPath.row].profGrade ?? 0)
                cell.descService.text = solicAct[indexPath.row].descServ
                if (solicAct[indexPath.row].status == 1){
                    cell.indAcep.text = "Aguardando Confirmação"
                    cell.indAcep.textColor = UIColor(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
                }else if (solicAct[indexPath.row].status == 2) {
                    cell.indAcep.text = "Solicitação Confirmada"
                    cell.indAcep.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                } else {
                    cell.indAcep.text = "Em Andamento"
                    cell.indAcep.textColor = UIColor(red: 10/255, green: 195/255, blue: 10/255, alpha: 1)
                }
                cell.schedDate.text = getFormattedSchedDate(solicAct[indexPath.row].solicDate)
                cell.date.text = getFormattedSolicDate(solicAct[indexPath.row].schedDate)
                cell.controller = vc
                cell.profUid = solicAct[indexPath.row].profUid
                cell.seqSol = String(solicAct[indexPath.row].solicitationsSeq)
                cell.profImageUrl = solicAct[indexPath.row].profImage
                if (solicAct[indexPath.row].status == 1){
                    cell.chatBtn.setImage(#imageLiteral(resourceName: "chat-off@512w"), for: .normal)
                    cell.chatBtn.isEnabled = false
                }else{
                    cell.chatBtn.setImage(#imageLiteral(resourceName: "chat-on@512w"), for: .normal)
                    cell.chatBtn.isEnabled = true
                }
                return cell
            }else if (indexPath.row == solicAct.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoreSolicCell") as? MoreSolicCell else { return UITableViewCell()}
                cell.vc = vc
                cell.type = 0
                return cell
            }
        }else{
            if (indexPath.row < solicInact.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SolicInactCell") as? SolicInactiveClass else { return UITableViewCell()}
                let application = UIApplication.shared.delegate as! AppDelegate
                let downloader = application.getImageDownloader()
                downloader.downloadImage(solicInact[indexPath.row].profImage ?? "", { (image,url) in
                    UIView.transition(with: cell, duration: 0.3, options: .transitionCrossDissolve, animations: {cell.profImage.image = image}, completion: nil)
                }) { (error) in
                    print(error)}
                if (solicInact[indexPath.row].status == 3){
                    cell.indAcep.text = "Não Aceito"
                }else if(solicInact[indexPath.row].status == 6){
                    cell.indAcep.text = "Profissional Não Compareceu"
                }
                else if(solicInact[indexPath.row].status == 7){
                    cell.indAcep.text = "Agendamento Cancelado"
                }else{
                    cell.indAcep.text = "Encerrado"
                }
                cell.name.text = solicInact[indexPath.row].profName
                cell.grade.text = String(solicInact[indexPath.row].profGrade ?? 0)
                cell.schedDate.text = getFormattedSchedDate(solicInact[indexPath.row].solicDate)
                cell.descServ.text = solicInact[indexPath.row].descServ
                cell.date.text = getFormattedSolicDate(solicInact[indexPath.row].schedDate)
                return cell
            }else if (indexPath.row == solicInact.count){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoreSolicCell") as? MoreSolicCell else { return UITableViewCell()}
                cell.vc = vc
                cell.type = 1
                return cell
            }
        }
        return UITableViewCell()
    }
    
    private func getFormattedSchedDate(_ date:String) -> String{
        let schedDayHour = date.components(separatedBy:"-")[2]
        let schedFormDay = schedDayHour.components(separatedBy: " ")[0]
        let schedFormYear = date.components(separatedBy:"-")[0]
        let schedFormMonth = getMonth(date.components(separatedBy:"-")[1])
        let schedFormHour = schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[0] + ":" + schedDayHour.components(separatedBy: " ")[1].components(separatedBy: ":")[1]
        return schedFormDay + " de " + schedFormMonth + " de " + schedFormYear + " às " + schedFormHour
    }
    
    private func getFormattedSolicDate(_ date:String) -> String{
        let schedDayHour = date.components(separatedBy:"-")[2]
        let schedFormDay = schedDayHour.components(separatedBy: " ")[0]
        let schedFormYear = date.components(separatedBy:"-")[0]
        let schedFormMonth = date.components(separatedBy:"-")[1]
        return schedFormDay + "/" + schedFormMonth + "/" + schedFormYear
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return solicSectLbl[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            if (vc.solicActPageCount >= vc.solicActNextPage){
                return (solicAct.count+1)
            }else{
                return (solicAct.count)
            }
        }else{
            if (vc.solicInactPageCount >= vc.solicInactNextPage){
                return (solicInact.count+1)
            }else{
                return (solicInact.count)
            }
        }
    }
    
    private func getMonth(_ month:String) -> String{
        
        switch month {
        case "01":
            return "janeiro"
        case "02":
            return "fevereiro"
        case "03":
            return "março"
        case "04":
            return "abril"
        case "05":
            return "maio"
        case "06":
            return "junho"
        case "07":
            return "julho"
        case "08":
            return "agosto"
        case "09":
            return "setembro"
        case "10":
            return "outubro"
        case "11":
            return "novembro"
        case "12":
            return "dezembro"
        default:
            return month
        }
    }
}
