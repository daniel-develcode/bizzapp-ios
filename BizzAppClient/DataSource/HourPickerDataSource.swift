import Foundation
import UIKit

class HourPickerDataSource:NSObject,UIPickerViewDataSource{
    var hours:[String]
    
    init(_ hours:[String]) {
        self.hours = hours
    }
    
    func reloadData(_ hours:[String]){
        self.hours = hours
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hours.count
    }
}
