import Foundation
import UIKit

class DatePickerDataSource:NSObject,UIPickerViewDataSource{
    
    var uIDates:[String]
    
    init(_ uIDates:[String]) {
        self.uIDates = uIDates
    }
    
    func reloadData(_ uIDates:[String]){
        self.uIDates = uIDates
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return uIDates.count
    }
}
