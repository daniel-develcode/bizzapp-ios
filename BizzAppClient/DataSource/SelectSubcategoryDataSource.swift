import Foundation
import UIKit

class SelectSubcategoryDataSource:NSObject,UITableViewDataSource{
    
    var subCategories:[SubCategories] = []
    var selectedRow:Int
    var selected:Bool
    
    init(_ subCategories:[SubCategories],_ selectedRow:Int,_ selected:Bool) {
        self.subCategories = subCategories
        self.selectedRow = selectedRow
        self.selected = selected
    }
    
    func reloadData(_ subCategories:[SubCategories],_ selectedRow:Int,_ selected:Bool){
        self.subCategories = subCategories
        self.selectedRow = selectedRow
        self.selected = selected
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubcategoryCell")! as? SubcategoryCell else { return UITableViewCell()}
        cell.subCategory.text = subCategories[indexPath.row].desCat
        cell.hexagonoImage.image = UIImage.init(named:"radio-off")
        if ((selectedRow == indexPath.row) && (selected == true)){
          cell.hexagonoImage.image = UIImage.init(named: "radio-on")
        }
        return cell
    }
}
