import Foundation
import UIKit

class SelectCategoryDataSource:NSObject,UITableViewDataSource{
    
    var categories:[GetCategory] = []
    
    init(_ categories:[GetCategory]) {
        self.categories = categories
    }
    
    func reloadData(_ categories:[GetCategory]){
        self.categories = categories
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell")! as? CategoryCell else { return UITableViewCell()}
        cell.categoryLbl.text = categories[indexPath.row].desCat.uppercased()
        cell.categoryImage.image = UIImage.init(named: categories[indexPath.row].imageCat)
        cell.tintColor = UIColor.black
        return cell
    }
}
