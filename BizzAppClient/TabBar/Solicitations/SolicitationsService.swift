import Foundation
import Alamofire

protocol SolicitationsServiceProtocol{
    
    typealias  OnGetSolicitationsActSuccess = ((GetSolicitations) -> Void)
    typealias  OnGetSolicitationsActError = ((String) -> Void)
    
    typealias  OnGetSolicitationsInactSuccess = ((GetSolicitations) -> Void)
    typealias  OnGetSolicitationsInactError = ((String) -> Void)
    
    func getSolicitationsActService(_ params:getWithIdParams,
                                    _ solicActNextPage:Int,
                                    _ successG:@escaping OnGetSolicitationsActSuccess,
                                    _ errorG:@escaping OnGetSolicitationsActError)
    
    func getSolicitationsInactService(_ params:getWithIdParams,
                                      _ solicInactNextPage:Int,
                                      _ successG:@escaping OnGetSolicitationsInactSuccess,
                                      _ errorG:@escaping OnGetSolicitationsInactError)
}

class SolicitationService:SolicitationsServiceProtocol{
    
    var service:RequestServiceProtocol
    init(_ service:RequestServiceProtocol) {
        self.service = service
    }
    
    func getSolicitationsActService(_ params:getWithIdParams,
                                    _ solicActNextPage:Int,
                                    _ successG:@escaping OnGetSolicitationsActSuccess,
                                    _ errorG:@escaping OnGetSolicitationsActError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let urlActive = Api_Url+"solicitation/v2/find-solicitations-active?codUser="+params.userCode+"&page="+String(solicActNextPage)
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(urlActive, .get, nil, header, { solicitationsActive in
                successG(Decoders.decodeData(solicitationsActive!))
        }) { (error) in
            errorG(error)
        }
    }
    
    func getSolicitationsInactService(_ params:getWithIdParams,
                                      _ solicInactNextPage:Int,
                                      _ successG:@escaping OnGetSolicitationsInactSuccess,
                                      _ errorG:@escaping OnGetSolicitationsInactError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let urlInactive = Api_Url+"solicitation/v2/find-solicitations-inactive?codUser="+params.userCode+"&page="+String(solicInactNextPage)
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(urlInactive, .get, nil, header, { solicitationInactive in
                successG(Decoders.decodeData(solicitationInactive!))
        }) { (error) in
            errorG(error)
        }
    }
}
