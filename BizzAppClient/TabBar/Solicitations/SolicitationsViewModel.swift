import Foundation

protocol SolicitationViewModelDelegate{
    
    func didGetSolicitationActSuccess(_ solicitationActive:GetSolicitations)
    func didGetSolicitationActError(_ error:String)
    
    func didGetSolicitationInactSuccess(_ solicitationInactive:GetSolicitations)
    func didGetSolicitationInactError(_ error:String)
}

protocol CancelViewModelDelegate {
    func cancelService()
    func didCancelSuccess(_ success:String)
    func didCancelError(_ error:String)
}

protocol SolicitationsViewModelProtocol{
    
    var solDelegate:SolicitationViewModelDelegate?{get set}
    var CancDelegate:CancelViewModelDelegate?{get set}
    
    func getSolicitationsAct(_ solicActNextPage: Int)
    func getSolicitationsInact(_ solicInactNextPage: Int)
    func cancel(_ params:CancelParams)
}

class SolicitationsViewModel:SolicitationsViewModelProtocol{
    
    var CancDelegate: CancelViewModelDelegate?
    var solDelegate: SolicitationViewModelDelegate?
    var serviceSol:SolicitationsServiceProtocol
    var serviceCancel:CancelServiceProtocol
    
    init(_ solicitationService: SolicitationsServiceProtocol,_ cancelService:CancelServiceProtocol) {
        serviceSol = solicitationService
        serviceCancel = cancelService
    }
    
    func cancel(_ params: CancelParams) {
        serviceCancel.cancel({ (success) in
            self.CancDelegate?.didCancelSuccess(success)
        }, { (error) in
            self.CancDelegate?.didCancelError(error)
        }, params)
    }
    
    func getSolicitationsAct(_ solicActNextPage: Int) {
        guard let param = GetParams.WithIdParams() else {solDelegate?.didGetSolicitationActError("error ao recuperar os dados");return}
        serviceSol.getSolicitationsActService(param,solicActNextPage, {(solicitationActive) in
            self.solDelegate?.didGetSolicitationActSuccess(solicitationActive)
        }) {(error) in
            self.solDelegate?.didGetSolicitationActError(error)
        }
    }
    
    func getSolicitationsInact(_ solicInactNextPage: Int) {
        guard let param = GetParams.WithIdParams() else {solDelegate?.didGetSolicitationInactError("error ao recuperar os dados");return}
        serviceSol.getSolicitationsInactService(param,solicInactNextPage,{(solicitationInactive) in
            self.solDelegate?.didGetSolicitationInactSuccess(solicitationInactive)
        }) {(error) in
            self.solDelegate?.didGetSolicitationInactError(error)
        }
    }
}
