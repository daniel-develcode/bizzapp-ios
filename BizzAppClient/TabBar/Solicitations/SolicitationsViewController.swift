import UIKit
import Swinject

class SolicitationsViewController: UIViewController,SolicitationViewModelDelegate,UITableViewDelegate {
    
    @IBOutlet weak var notFoundView: UIView!
    @IBOutlet weak var solicTableView: UITableView!
    
    var solicAct:[Solicitations] = []
    var solicInact:[Solicitations] = []
    var viewModel:SolicitationsViewModelProtocol!
    var evalDataSource:SolicitationDataSource!
    var solicActNextPage:Int = 1
    var solicInactNextPage:Int = 1
    var solicActPageCount:Int = 1
    var solicInactPageCount:Int = 1
    var spinner:UIView!
    
    func didGetSolicitationActSuccess(_ solicitationActive: GetSolicitations) {
        solicAct.append(contentsOf: solicitationActive.lsSolicitation)
        if((solicAct.count > 0) || (solicInact.count > 0)){
            notFoundView.isHidden = true
        }else{
            notFoundView.isHidden = false
        }
        solicActPageCount = solicitationActive.pageCount
        solicActNextPage += 1
        evalDataSource.reload(solicAct,solicInact)
        solicTableView.reloadData()
        UIViewController.removeSpinner(spinner)
    }
    
    func didGetSolicitationInactSuccess(_ solicitationInactive: GetSolicitations) {
        solicInact.append(contentsOf: solicitationInactive.lsSolicitation)
        if((solicAct.count > 0) || (solicInact.count > 0)){
            notFoundView.isHidden = true
        }else{
            notFoundView.isHidden = false
        }
        solicInactPageCount = solicitationInactive.pageCount
        solicInactNextPage += 1
        evalDataSource.reload(solicAct,solicInact)
        solicTableView.reloadData()
        UIViewController.removeSpinner(spinner)
    }
    
    func didGetSolicitationInactError(_ error: String) {
        print(error)
    }
    
    func didGetSolicitationActError(_ error: String) {
        print(error)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectedSolicitation", bundle: nil)
        let vc: SelectedSolicitationViewController = storyboard.instantiateViewController(withIdentifier: "SelectedSolicitationViewController") as! SelectedSolicitationViewController
        vc.seqSol = solicAct[indexPath.row].solicitationsSeq
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner = UIViewController.displaySpinner(onView:self.view)
        viewModel.getSolicitationsAct(solicActNextPage)
        viewModel.getSolicitationsInact(solicInactNextPage)
        evalDataSource.reload(solicAct, solicInact)
        solicTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = SolicitationsViewModel(AppDelegate.container.resolve(SolicitationsServiceProtocol.self)!, AppDelegate.container.resolve(CancelServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.solDelegate = self
        evalDataSource = SolicitationDataSource(solicAct, solicInact,self)
        solicTableView.dataSource = evalDataSource
        solicTableView.isEmptyRowsHidden = true
        solicTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        solicActNextPage = 1
        solicInactNextPage = 1
        solicAct = []
        solicInact = []
    }
}

class SolicActiveClass:UITableViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var grade: UILabel!
    @IBOutlet weak var schedDate: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var indAcep: UILabel!
    @IBOutlet weak var descService: UILabel!
    @IBOutlet weak var profImage: UIImageView!
    @IBOutlet weak var chatBtn: UIButton!
    var controller:UIViewController!
    var profUid:String!
    var seqSol:String!
    var profImageUrl:String!
    
    @IBAction func chat(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Chat", bundle: nil)
        let vc: ChatViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.labelName = name.text
        vc.profUid = profUid
        vc.seqSol = seqSol
        vc.profImageUrl = profImageUrl
        controller.present(vc, animated: true, completion: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func prepareForReuse() {
        profImage.image = #imageLiteral(resourceName: "DefaultProfile")
    }
}

class SolicInactiveClass:UITableViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var grade: UILabel!
    @IBOutlet weak var schedDate: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var descServ: UILabel!
    @IBOutlet weak var profImage: UIImageView!
    @IBOutlet weak var indAcep: UILabel!
    
    override func prepareForReuse() {
        self.isUserInteractionEnabled = false
        profImage.image = #imageLiteral(resourceName: "DefaultProfile")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class MoreSolicCell:UITableViewCell{
    var type:Int!
    var vc:SolicitationsViewController!
    @IBAction func more(_ sender: Any) {
        if (type == 0){
          vc.viewModel.getSolicitationsAct(vc.solicActNextPage)
        }else{
          vc.viewModel.getSolicitationsInact(vc.solicInactNextPage)
        }
    }
}
