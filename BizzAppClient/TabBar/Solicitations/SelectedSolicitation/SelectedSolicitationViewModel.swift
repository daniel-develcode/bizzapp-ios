import Foundation

protocol SelectedSolicitationViewModelDelegate{

    func onGetSolicitationSuccess(_ solicitation:SelectedSolicitation)
    func onGetSolicitationError(_ error:String)
}

protocol SelectedSolicitationViewModelProtocol {
    
    var delegate:SelectedSolicitationViewModelDelegate?{get set}
    
    func getSolicitation(_ seqSol:Int)
}

class SelectedSolicitationViewModel:SelectedSolicitationViewModelProtocol{
    var delegate: SelectedSolicitationViewModelDelegate?
    
    var service:SelectedSolicitationServiceProtocol
    
    init (_ service:SelectedSolicitationServiceProtocol){
        self.service = service
    }
    
    func getSolicitation(_ seqSol: Int) {
        service.getSelectedSol(seqSol, { (solicitation) in
            self.delegate?.onGetSolicitationSuccess(solicitation)
        }) { (err) in
            self.delegate?.onGetSolicitationError(err)
        }
    }
}
