import Foundation
import Alamofire

protocol SelectedSolicitationServiceProtocol {
    
    typealias OnGetSelecSolSuccess = ((SelectedSolicitation) -> Void)
    typealias OnGetSelecSolError = ((String) -> Void)
    
    func getSelectedSol(_ seqSol:Int,
                        _ success:@escaping OnGetSelecSolSuccess,
                        _ error:@escaping OnGetSelecSolError)
}

class SelectedSolicitationService:SelectedSolicitationServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol) {
        self.service = service
    }
   
    func getSelectedSol(_ seqSol: Int,
                        _ success: @escaping OnGetSelecSolSuccess,
                        _ error: @escaping OnGetSelecSolError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"solicitation/find-by-id?seqSolicitacao=" + String(seqSol)
        let token = UserDefaults.standard.object(forKey:"userToken") as? String
        let validToken  = token ?? ""
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":validToken,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(url, .get, nil, header, { (data) in
            success(Decoders.decodeData(data!))
        }) { (err) in
            error(err)
        }
    }
}
