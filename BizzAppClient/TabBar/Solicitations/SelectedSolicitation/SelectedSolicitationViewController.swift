import UIKit
import GoogleMaps
import GooglePlaces
import AVKit

class SelectedSolicitationViewController: UIViewController,SelectedSolicitationViewModelDelegate,CancelViewModelDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var seqSol:Int!
    var cancelViewModel:SolicitationsViewModelProtocol!
    var viewModel:SelectedSolicitationViewModel!
    var solicitation: SelectedSolicitation?
    var spinner:UIView!
    var floatingBtnIsHidden = true
    var floatingBtn = UIButton(type: .custom)
    @IBOutlet weak var table: UITableView!
    @IBAction func returnBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (solicitation == nil){
            return 0
        }else if ((section == 3) && (solicitation?.imgVideoList.count == 0)){
            return 0
        }else if((section == 2) && (solicitation?.obsTxt == "")){
            return 0
        }else if(section == 6){
            return (solicitation?.messages.count) ?? 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0){
            guard let profileSolCell = tableView.dequeueReusableCell(withIdentifier: "ProfProfileSolCell")! as? ProfProfileSolCell else { return UITableViewCell()}
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(solicitation?.profImage ?? "", { (image,url) in
                UIView.transition(with: profileSolCell, duration: 0.3, options: .transitionCrossDissolve, animations: {profileSolCell.profImage?.image = image}, completion: nil)
            }) { (error) in
                print(error)
            }
            profileSolCell.grade.text = String(solicitation?.profGrade ?? 0)
            profileSolCell.profName.text = solicitation?.profName
            profileSolCell.schedDate.text = table.getFormattedSchedDate((solicitation?.schedDate)!)
            profileSolCell.date.text = table.getFormattedSolicDate((solicitation?.solicDate)!)
            profileSolCell.service.text = (solicitation?.descSubcategory)! + "/" + (solicitation?.descService)!
            profileSolCell.service.sizeToFit()
            if (solicitation?.indStatus == 1){
                profileSolCell.indStatus.text = "Aguardando confirmação"
                profileSolCell.indStatus.textColor = UIColor(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
            }else if (solicitation?.indStatus == 2) {
                profileSolCell.indStatus.text = "Solicitação confirmada"
                profileSolCell.indStatus.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
            } else {
                profileSolCell.indStatus.text = "Em andamento"
                profileSolCell.indStatus.textColor = UIColor(red: 10/255, green: 195/255, blue: 10/255, alpha: 1)
            }
            return profileSolCell
            
        }else if (indexPath.section == 1){
            guard let solServCell = tableView.dequeueReusableCell(withIdentifier: "SolServCell")! as? SolServCell else { return UITableViewCell()}
            solServCell.service.text = (solicitation?.descSubcategory)! + "/" + (solicitation?.descService)!
            return solServCell
            
        }else if (indexPath.section == 2){
            guard let solObsCell = tableView.dequeueReusableCell(withIdentifier: "SolObsCell")! as? SolObsCell else { return UITableViewCell()}
            solObsCell.obs.text = solicitation?.obsTxt
            return solObsCell
            
        }else if (indexPath.section == 3){
            guard let galerySolCell = tableView.dequeueReusableCell(withIdentifier: "GalerySolCell")! as? GalerySolCell else {return UITableViewCell()}
            return galerySolCell
            
        }else if (indexPath.section == 4){
            guard let solMapCell = tableView.dequeueReusableCell(withIdentifier: "SolMapCell")! as? SolMapCell else {return UITableViewCell()}
            solMapCell.lati = Double(solicitation?.numLat ?? 0)
            solMapCell.long = Double(solicitation?.numLong ?? 0)
            solMapCell.adressLbl.text = solicitation?.adress
            solMapCell.viewCotrol = self
            let userLat:Double? = UserDefaults.standard.double(forKey: "userLatitude")
            let userLon:Double? = UserDefaults.standard.double(forKey: "userLongitude")
            let SolCord = CLLocation(latitude: CLLocationDegrees(solicitation?.numLat ?? 0), longitude: CLLocationDegrees(solicitation?.numLong ?? 0))
            let userLoc = CLLocation(latitude: CLLocationDegrees( userLat ?? 0), longitude: CLLocationDegrees(userLon ?? 0))
            let solDistance = userLoc.distance(from: SolCord)
            if(solDistance < 150){
                solMapCell.wazeBtn.isHidden = true
                solMapCell.gooogleMapsBtn.isHidden = true
            }
            if ((solicitation?.reference != nil) && (solicitation?.reference != "")){
                solMapCell.reference.text = "Referência: "+(solicitation?.reference ?? "")
            }else{
                solMapCell.reference.isHidden = true
            }
            solMapCell.iniGoogleMaps()
            return solMapCell
            
        }else if (indexPath.section == 5){
            guard let notificationLblCell = tableView.dequeueReusableCell(withIdentifier: "NotificationLblCell")! as? NotificationLblCell else {return UITableViewCell()}
            return notificationLblCell
            
        }else if (indexPath.section == 6){
            guard let notificationMessagesCell = tableView.dequeueReusableCell(withIdentifier: "NotificationMessagesCell")! as? NotificationMessagesCell else {return UITableViewCell()}
            notificationMessagesCell.notificationMessage.text = solicitation?.messages[indexPath.row].txtMessage
            notificationMessagesCell.notificationDate.text = tableView.getFormattedDate((solicitation?.messages[indexPath.row].dateMessage)!)
            return notificationMessagesCell
            
        }else if (indexPath.section == 7){
            guard let cancelServCell = tableView.dequeueReusableCell(withIdentifier: "CancelServCell")! as? CancelServCell else {return UITableViewCell()}
            cancelServCell.cancelDelegate = self
            return cancelServCell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 3){
            return 110
        }else {
            return UITableView.automaticDimension
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    func cancelService(){
        let params = CancelParams(
            indStatus:7,
            seqSol:(seqSol)
        )
        cancelViewModel.cancel(params)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didCancelSuccess(_ success: String) {
        print(success)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didCancelError(_ error: String) {
        print(error)
    }
    
    func onGetSolicitationSuccess(_ solicitation: SelectedSolicitation) {
        self.solicitation = solicitation
        table.reloadData()
        UIViewController.removeSpinner(spinner)
        if(solicitation.indStatus != 1){
        floatingButton()
        floatingBtnIsHidden = false
        }
    }
    
    func onGetSolicitationError(_ error: String) {
        print(error)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (solicitation == nil){
            return 0
        }else {
            let images =  solicitation!.imgVideoList
            return images.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoSolCell", for: indexPath) as? PhotoSolCell else { return UICollectionViewCell()}
        
        if(solicitation!.imgVideoList[indexPath.row].type == 1){
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(solicitation!.imgVideoList[indexPath.row].desImage, { (image,url) in
                UIView.transition(with: cell.photoSol, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    cell.photoSol.setImage(image, for: .normal)
                    cell.type = 1
                }, completion: nil)
            }) { (error) in
                print(error)}
        }else{
            cell.type = 2
            let videoURL = self.solicitation!.imgVideoList[indexPath.row].desImage
            cell.url = self.solicitation!.imgVideoList[indexPath.row].desImage
            var imagePlay = #imageLiteral(resourceName: "play")
            let cgsize:CGSize = CGSize(width: 50, height: 50)
            imagePlay = imagePlay.resize(size: cgsize)
            cell.photoSol.setImage(imagePlay, for: .normal)
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            let thumb = downloader.checkCache(videoURL)
            if(thumb == nil){
                imagePlay.videoSnapshot(videoURL, { (image) in
                    let rotatedImage = UIImage(cgImage: image.cgImage!, scale: CGFloat(1.0), orientation: .right)
                    downloader.addImageToCache(videoURL, rotatedImage)
                    DispatchQueue.main.sync {
                        cell.photoSol.setBackgroundImage(rotatedImage, for: .normal)
                    }
                }) { (error) in
                    print(error)
                }
            }else{
                cell.photoSol.setBackgroundImage(thumb, for: .normal)
            }
        }
        cell.vcView = self
        return cell
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @objc func chatBtn(_ sender: UIButton!) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Chat", bundle: nil)
        let vc: ChatViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.profUid = solicitation?.userUid
        vc.seqSol = String(solicitation!.solicitationSeq)
        vc.profImageUrl = solicitation?.profImage
        vc.labelName = solicitation?.profName
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !floatingBtnIsHidden{
            floatingButton()
        }
    }
    
    func floatingButton(){
        floatingBtn.frame = CGRect(x: view.bounds.maxX - 75, y: view.bounds.maxY - 75, width: 65, height: 65)
        floatingBtn.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        floatingBtn.setTitle("", for: .normal)
        floatingBtn.setImage(#imageLiteral(resourceName: "chat-botao"), for: .normal)
        floatingBtn.backgroundColor = #colorLiteral(red: 0.9991701245, green: 0.7652246356, blue: 0.09975344688, alpha: 0)
        floatingBtn.clipsToBounds = true
        floatingBtn.layer.cornerRadius = 65/2
        floatingBtn.layer.borderColor = #colorLiteral(red: 0.9991701245, green: 0.7652246356, blue: 0.09975344688, alpha: 0)
        floatingBtn.layer.borderWidth = 3.0
        floatingBtn.addTarget(self,action: #selector(chatBtn), for: UIControl.Event.touchUpInside)
        if let window = UIApplication.shared.keyWindow {
        window.addSubview(floatingBtn)
        self.view.bringSubviewToFront(floatingBtn)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        floatingBtn.removeFromSuperview()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        table.isEmptyRowsHidden = true
        table.allowsSelection = false
        cancelViewModel = SolicitationsViewModel(AppDelegate.container.resolve(SolicitationsServiceProtocol.self)!, AppDelegate.container.resolve(CancelServiceProtocol.self)!)
        let viewModel = SelectedSolicitationViewModel(AppDelegate.container.resolve(SelectedSolicitationServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.getSolicitation(seqSol)
        viewModel.delegate = self
        spinner = UIViewController.displaySpinner(onView:self.view )
    }
}

class ProfProfileSolCell:UITableViewCell{
    
    @IBOutlet weak var profName: UILabel!
    @IBOutlet weak var profImage: UIImageView!
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var schedDate: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var indStatus: UILabel!
    @IBOutlet weak var grade: UILabel!
}

class SolServCell:UITableViewCell{
    @IBOutlet weak var service: UILabel!
}

class SolObsCell:UITableViewCell{
    @IBOutlet weak var obs: UILabel!
}

class SolMapCell:UITableViewCell,CLLocationManagerDelegate,GMSMapViewDelegate{
    @IBOutlet weak var adressLbl: UILabel!
    @IBOutlet weak var reference: UILabel!
    @IBOutlet weak var mapContainer: GMSMapView!
    var locationManager = CLLocationManager()
    var lati:Double!
    var long:Double!
    var viewCotrol:UIViewController!
    @IBOutlet weak var wazeBtn: UIButton!
    @IBOutlet weak var gooogleMapsBtn: UIButton!
    
    
    
    @IBAction func wazeBtn(_ sender: Any) {
        guard let url = URL(string: "https://waze.com/ul?ll="+String(lati)+","+String(long)+"&z=10") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func googleMapsBtn(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
           let url = URL(string:"http://maps.google.com/maps?q="+String(lati)+","+String(long))!
            UIApplication.shared.open(url)
        } else {
            print("Can't use comgooglemaps://");
        }
    }
    
    func iniGoogleMaps(){
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 16.0)
        mapContainer.camera = camera
        self.mapContainer.delegate = self
        self.mapContainer.isMyLocationEnabled = true
        self.mapContainer.settings.myLocationButton = true
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = mapContainer
        marker.icon = viewCotrol.getMarkerImage()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapContainer.isMyLocationEnabled = true
    }
}

class NotificationLblCell:UITableViewCell{}

class NotificationMessagesCell:UITableViewCell{
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var notificationDate: UILabel!
}

class GalerySolCell:UITableViewCell{
    @IBOutlet weak var galCollection: UICollectionView!
}

class PhotoSolCell:UICollectionViewCell{
    
    @IBOutlet weak var photoSol: UIButton!
    var type:Int!
    var url:String = ""
    var vcView:UIViewController?
    
    @IBAction func fullscreen(_ sender: Any) {
        if (type == 1){
            let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
            vc.receivedImage = photoSol.currentImage
            vc.view.frame = (vcView?.view.bounds)!
            vcView!.addChild(vc)
            UIView.transition(with: (vcView?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                              animations: {self.vcView!.view.addSubview(vc.view)}, completion: nil)
        }else{
            let vd = VideoDownloader()
            vd.presentVideo(url, vcView!)
        }
    }
}

class CancelServCell:UITableViewCell{
    var cancelDelegate:CancelViewModelDelegate!
    
    @IBAction func cancelServBtn(_ sender: Any) {
        cancelDelegate.cancelService()
    }
}

extension GalerySolCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDelegate & UICollectionViewDataSource>(_ dataSourceDelegate: D, forRow row:Int){
        galCollection.delegate = dataSourceDelegate
        galCollection.dataSource = dataSourceDelegate
        galCollection.reloadData()
    }
}
