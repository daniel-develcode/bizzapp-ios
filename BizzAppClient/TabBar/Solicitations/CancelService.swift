import Foundation
import Alamofire

struct CancelParams {
    var indStatus:Int
    var seqSol:Int
}

protocol CancelServiceProtocol{
    
    typealias OnCancelBtnSuccess = ((String) -> Void)
    typealias OnCancelBtnError = ((String) -> Void)
    
    func cancel(_ success:@escaping OnCancelBtnSuccess,
                _ error:@escaping OnCancelBtnError,
                _ params: CancelParams)
}

class CancelService:CancelServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol) {
        self.service = service
    }
    
    func cancel(_ success:@escaping OnCancelBtnSuccess,
                _ error:@escaping OnCancelBtnError,
                _ params:CancelParams) {
        
        let param = GetParams.WithIdParams()
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"solicitation/change-status?indStatus="+String(params.indStatus)+"&seqSolicitation="+String(params.seqSol)
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":param!.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(url, .delete, nil, header, { _ in
            success("cancelado com sucesso.")
        }) { (errorC) in
            error(errorC)
        }
    }
}
