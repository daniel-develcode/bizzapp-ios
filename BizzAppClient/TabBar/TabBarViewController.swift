import UIKit
import Firebase

class TabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let err = error {
                print("Error \(err)")
            } else if let result = result {
                print("Remote instance \(result.token)")
            }
        }
    }
}
