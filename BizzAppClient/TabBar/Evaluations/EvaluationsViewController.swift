import UIKit

class EvaluationsViewController: UIViewController,EvaluationsViewModelDelegate,UITableViewDelegate {
    
    @IBOutlet weak var evalTableView: UITableView!
    var viewModel:EvaluationsViewModelProtocol!
    var evalDS:EvaluationsDataSource!
    var evalDone:[EvaluationsDone] = []
    var evalToDo:[EvaluationsToDo] = []
    var evalDonePageCount:Int = 0
    var evalToDoPageCount:Int = 0
    var evalDoneNextPage:Int = 2
    var evalToDoNextPage:Int = 2
    var spinner:UIView!
    @IBOutlet weak var notFoundView: UIView!
    
    func didGetEvaluationsWithSuccess(_ successDone: GetEvaluationsDone, _ successToDo: GetEvaluationsToDo) {
        evalDone.append(contentsOf: successDone.lsEvaluation)
        evalToDo.append(contentsOf: successToDo.lsEvaluation)
        evalDonePageCount = successDone.pageCount
        evalToDoPageCount = successToDo.pageCount
        if((evalDone.count > 0) || (evalToDo.count > 0)){
            notFoundView.isHidden = true
        }
        evalDS.reload(evalDone, evalToDo)
        evalTableView.reloadData()
        UIViewController.removeSpinner(spinner)
    }
    
    func didGetEvaluationsWithError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    func didSaveEvaluationsWithSuccess(_ successMessage: String,_ saveCallback:SaveEvalCallback) {
        let image = evalToDo[saveCallback.index!].image
        let name = evalToDo[saveCallback.index!].name
        let subCatecorie = evalToDo[saveCallback.index!].subCategorie
        let obs = saveCallback.obs!
        let grade = saveCallback.grade!
        let currentDate = evalToDo[saveCallback.index!].date
        let eval = EvaluationsDone(
            image:image,
            name:name,
            subCategorie:subCatecorie,
            grade:grade,
            observation:obs,
            date:currentDate
        )
        evalDone.insert(eval, at:0)
        evalToDo.remove(at: saveCallback.index!)
        evalDS.reload(evalDone, evalToDo)
        UIViewController.removeSpinner(spinner)
        evalTableView.reloadData()
    }
    
    func didSaveEvaluationsWithError(_ errorMessage: String) {
        print(errorMessage)
        UIViewController.removeSpinner(spinner)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        
    }
    
    func didGetEvaluationsToDoWithSuccess(_ successToDo: GetEvaluationsToDo) {
        evalToDo.append(contentsOf: successToDo.lsEvaluation)
        evalToDoPageCount = successToDo.pageCount
        evalToDoNextPage += 1
        evalDS.reload(evalDone, evalToDo)
        evalTableView.reloadData()
    }
    
    func didGetEvaluationsToDoWithError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    func didGetEvaluationsDoneWithSuccess(_ successDone: GetEvaluationsDone) {
        evalDone.append(contentsOf: successDone.lsEvaluation)
        evalDonePageCount = successDone.pageCount
        evalDoneNextPage += 1
        evalDS.reload(evalDone, evalToDo)
        evalTableView.reloadData()
    }
    
    func didGetEvaluationsDoneWithError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = EvaluationsViewModel(AppDelegate.container.resolve(EvaluationsServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        evalDS = EvaluationsDataSource(evalDone,evalToDo,viewModel,self)
        evalTableView.dataSource = evalDS
        evalTableView.allowsSelection = false
        evalTableView.isEmptyRowsHidden = true
        evalTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner = UIViewController.displaySpinner(onView:self.view)
        viewModel.getEvaluations(page: 1)
        evalDS.reload(evalDone, evalToDo)
        evalTableView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        evalDone = []
        evalToDo = []
        evalDoneNextPage = 2
        evalToDoNextPage = 2
    }
}

class EvalDoneCell:UITableViewCell{
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var obsLbl: UILabel!
    @IBOutlet weak var desServ: UILabel!
    @IBOutlet weak var profImage: UIImageView!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    override func prepareForReuse() {
        profImage.image = #imageLiteral(resourceName: "DefaultProfile")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class EvalToDoCell:UITableViewCell,UITextFieldDelegate{
    
    var evalViewModel:EvaluationsViewModelProtocol?
    var params:SaveEvalParams?
    var gradeValue:Float = 0
    var thisView:EvaluationsViewController!
    @IBOutlet weak var desServ: UILabel!
    @IBOutlet weak var dateDone: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var eval1Lbl: UIButton!
    @IBOutlet weak var eval2Lbl: UIButton!
    @IBOutlet weak var eval3Lbl: UIButton!
    @IBOutlet weak var eval4Lbl: UIButton!
    @IBOutlet weak var eval5Lbl: UIButton!
    @IBOutlet weak var comentTxt: UITextField!
    @IBOutlet weak var profImage: UIImageView!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        thisView?.view!.endEditing(true)
        return false
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        resetEval()
    }
    
    @IBAction func evalBtn(_ sender: Any) {
        thisView.spinner = UIViewController.displaySpinner(onView:thisView!.view)
        if (gradeValue > 0){
        params?.grade = gradeValue
        params?.txtObs = comentTxt.text
        evalViewModel?.saveEvaluations(params!)
        resetEval()
        }else{
            UIViewController.removeSpinner(thisView.spinner)
            thisView?.alertMessage(titleAlert: "Erro", messageAlert: "Não é permitido avaliar com zero estrelas, a avaliação mínima é uma estrela.", buttonAlert: "OK")
        }
    }
    
    private func resetEval(){
        gradeValue = 0
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        comentTxt.text = ""
    }
    
    @IBAction func eval1Btn(_ sender: Any) {
        gradeValue = 1
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
    }
    
    @IBAction func eval2Btn(_ sender: Any) {
        gradeValue = 2
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
    }
    
    @IBAction func eval3Btn(_ sender: Any) {
        gradeValue = 3
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
    }
    
    @IBAction func eval4Btn(_ sender: Any) {
        gradeValue = 4
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-off@512h"), for: .normal)
    }
    
    @IBAction func eval5Btn(_ sender: Any) {
        gradeValue = 5
        eval1Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval2Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval3Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval4Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
        eval5Lbl.setImage(#imageLiteral(resourceName: "avaliacao-grande-on@512h"), for: .normal)
    }
    
    override func prepareForReuse() {
        profImage.image = #imageLiteral(resourceName: "DefaultProfile")
        comentTxt.delegate = self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class MoreEvalCell:UITableViewCell{
    var evalType:Int!
    var vc:EvaluationsViewController!
    
    @IBAction func moreEval(_ sender: Any) {
        if (evalType == 0){
            vc.viewModel.getEvaluationsTodo(page: vc.evalToDoNextPage)
        }else{
            vc.viewModel.getEvaluationsDone(page: vc.evalDoneNextPage)
        }
    }
}
