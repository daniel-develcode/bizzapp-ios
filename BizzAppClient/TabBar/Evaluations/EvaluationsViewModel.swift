import Foundation

protocol EvaluationsViewModelDelegate {
    
    func didGetEvaluationsWithSuccess(_ successDone:GetEvaluationsDone,_ successToDo:GetEvaluationsToDo)
    
    func didGetEvaluationsWithError(_ errorMessage:String)
    
    func didSaveEvaluationsWithSuccess(_ successMessage:String,_ saveCallback:SaveEvalCallback)
    
    func didSaveEvaluationsWithError(_ errorMessage:String)
   
    func didGetEvaluationsToDoWithSuccess(_ successToDo:GetEvaluationsToDo)
    
    func didGetEvaluationsToDoWithError(_ errorMessage:String)
    
    func didGetEvaluationsDoneWithSuccess(_ successDone:GetEvaluationsDone)
    
    func didGetEvaluationsDoneWithError(_ errorMessage:String)
    
}

protocol EvaluationsViewModelProtocol {
    var delegate:EvaluationsViewModelDelegate?{get set}
    
    func getEvaluations(page:Int)
    
    func saveEvaluations(_ params1:SaveEvalParams)
    
    func getEvaluationsDone(page:Int)
    
    func getEvaluationsTodo(page:Int)
}

class EvaluationsViewModel:EvaluationsViewModelProtocol{
    func getEvaluationsDone(page: Int) {
        guard let param = GetParams.WithIdParams() else {delegate?.didGetEvaluationsWithError("error ao recuperar os dados");return}
        service.getEvaluationsDoneService(param,page, { (EvalDone) in
            self.delegate?.didGetEvaluationsDoneWithSuccess(EvalDone)
        }) { (error) in
            self.delegate?.didGetEvaluationsDoneWithError(error)
        }
    }
    
    func getEvaluationsTodo(page: Int) {
        guard let param = GetParams.WithIdParams() else {delegate?.didGetEvaluationsWithError("error ao recuperar os dados");return}
        service.getEvaluationsToDoService(param,page, { (EvalToDo) in
            self.delegate?.didGetEvaluationsToDoWithSuccess(EvalToDo)
        }) { (error) in
            self.delegate?.didGetEvaluationsToDoWithError(error)
        }
    }
    
    
    var delegate: EvaluationsViewModelDelegate?
    var service:EvaluationsServiceProtocol
    
    init(_ SearchProfService: EvaluationsServiceProtocol) {
        service = SearchProfService
    }
    
    func getEvaluations(page:Int) {
        guard let param = GetParams.WithIdParams() else {delegate?.didGetEvaluationsWithError("error ao recuperar os dados");return}
        
        service.getEvaluationsService(param,page, { (EvalDone, EvalToDo) in
            self.delegate?.didGetEvaluationsWithSuccess(EvalDone,EvalToDo)
        }) { (error) in
            self.delegate?.didGetEvaluationsWithError(error)
        }
    }
    
    func saveEvaluations(_ params1:SaveEvalParams) {
        guard let params2 = GetParams.WithIdParams() else {delegate?.didGetEvaluationsWithError("error ao recuperar os dados");return}
        
        service.saveEvaluationService(params1,params2, { (successMessage,callback) in
            if (params1.grade == 0){
                self.delegate?.didSaveEvaluationsWithError("Você não pode avaliar com zero.")
            }else{
                self.delegate?.didSaveEvaluationsWithSuccess(successMessage,callback)
            }
        }) { (errorMessage) in
            self.delegate?.didSaveEvaluationsWithError(errorMessage)
        }
    }
}
