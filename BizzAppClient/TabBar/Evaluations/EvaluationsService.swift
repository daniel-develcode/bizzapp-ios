import Foundation
import Alamofire

struct SaveEvalParams{
    var seqSolic:Int?
    var profCode:Int?
    var grade:Float?
    var txtObs:String?
    var index:Int?
}

struct SaveEvalCallback{
    var index:Int?
    var obs:String?
    var grade:Float?
}

protocol EvaluationsServiceProtocol {
    typealias OnGetEvaluationsSuccess = ((GetEvaluationsDone,GetEvaluationsToDo) -> Void)
    typealias OnGetEvaluationsError = ((String) -> Void)
    
    typealias OnSaveEvaluationsSuccess = ((String,SaveEvalCallback) -> Void)
    typealias OnSaveEvaluationsError = ((String) -> Void)
    
    typealias OnGetEvalToDoSuccess = ((GetEvaluationsToDo) -> Void)
    typealias OnGetEvalDoneSuccess = ((GetEvaluationsDone) -> Void)
    
    typealias OnGetEvalToDoError = ((String) -> Void)
    typealias OnGetEvalDoneError = ((String) -> Void)
    
    func getEvaluationsService(_ params:getWithIdParams,
                               _ page:Int,
                               _ success:@escaping OnGetEvaluationsSuccess,
                               _ error:@escaping OnGetEvaluationsError)
    
    func saveEvaluationService(_ params1:SaveEvalParams,
                               _ params2: getWithIdParams,
                               _ success:@escaping OnSaveEvaluationsSuccess,
                               _ error:@escaping OnSaveEvaluationsError)
    
    func getEvaluationsToDoService(_ params:getWithIdParams,
                                   _ page:Int,
                                   _ success:@escaping OnGetEvalToDoSuccess,
                                   _ error:@escaping OnGetEvalToDoError)
    
    func getEvaluationsDoneService(_ params:getWithIdParams,
                                   _ page:Int,
                                   _ success:@escaping OnGetEvalDoneSuccess,
                                   _ error:@escaping OnGetEvalDoneError)
}

class EvaluationsService:EvaluationsServiceProtocol{
    
    var service:RequestServiceProtocol
    init(_ service:RequestServiceProtocol){
        self.service = service
    }
    
    func getEvaluationsToDoService(_ params:getWithIdParams,
                               _ page:Int,
                               _ success:@escaping OnGetEvalToDoSuccess,
                               _ error:@escaping OnGetEvalToDoError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let urlEvalToDo = Api_Url+"evaluation/v2/find-evaluations-to-do?codUser="+params.userCode+"&page="+String(page)
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(urlEvalToDo, .get, nil, header, { evalToDoData in
            success(Decoders.decodeData(evalToDoData!))
        }) { (requestError) in
            error(requestError)
        }
    }
    
    func getEvaluationsDoneService(_ params:getWithIdParams,
                               _ page:Int,
                               _ success:@escaping OnGetEvalDoneSuccess,
                               _ error:@escaping OnGetEvalDoneError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let urlEvalDone = Api_Url+"evaluation/v2/find-evaluations-done?codUser="+params.userCode+"&page="+String(page)
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(urlEvalDone, .get, nil, header, { evalDoneData in
            success(Decoders.decodeData(evalDoneData!))
        }) { (requestError) in
            error(requestError)
        }
    }
    
    
    func getEvaluationsService(_ params:getWithIdParams,
                               _ page:Int,
                               _ success:@escaping OnGetEvaluationsSuccess,
                               _ error:@escaping OnGetEvaluationsError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let urlEvalDone = Api_Url+"evaluation/v2/find-evaluations-done?codUser="+params.userCode+"&page="+String(page)
        let urlEvalToDo = Api_Url+"evaluation/v2/find-evaluations-to-do?codUser="+params.userCode+"&page="+String(page)
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(urlEvalDone, .get, nil, header, { evalDonedata in
            self.service.request(urlEvalToDo, .get, nil, header, { evalToDoData in
                success(Decoders.decodeData(evalDonedata!),Decoders.decodeData(evalToDoData!))
            }, { (requestError) in
                error(requestError)
            })
        }) { (requestError) in
            error(requestError)
        }
    }
    
    func saveEvaluationService(_ params1:SaveEvalParams,
                               _ params2: getWithIdParams,
                               _ success:@escaping OnSaveEvaluationsSuccess,
                               _ error:@escaping OnSaveEvaluationsError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"evaluation/save"
        
        let requestParams:Parameters = [
            "seqSolicitacao":params1.seqSolic!,
            "codUserProfissional":params1.profCode!,
            "codUsuario":params2.userCode,
            "numNota":params1.grade!,
            "txtObs":params1.txtObs!
        ]
        let header:HTTPHeaders = [
            "Authorization":params2.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        let saveCallback = SaveEvalCallback(
        index:params1.index,
        obs:params1.txtObs,
        grade:params1.grade
        )
        
        service.request(url, .post, requestParams, header, { data in
            success("Avaliação feita com sucesso",saveCallback)
        }) { (requestError) in
            error(requestError)
        }
    }
}
