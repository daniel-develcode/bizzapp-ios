import UIKit
import Swinject
import Alamofire
import AlamofireImage

class SearchProfViewController: UIViewController,SearchProfViewModelDelegate,UITableViewDelegate,UISearchBarDelegate{
    
    @IBOutlet weak var notFoundView: UIView!
    @IBOutlet weak var profTableView: UITableView!
    var viewModel:SearchProfViewModelProtocol!
    var profs:[ProfCellData] = []
    var currentProfList:[ProfCellData] = []
    var searchProfDS:SearchProfDataSource!
    var imageArray:[UIImage] = []
    var catName:String!
    var subcatName:String!
    var catCode:Int!
    var spinner:UIView!
    var catImageStr:String!
    var nextPage:Int = 1
    var pageCount:Int = 0
    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var adressBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchIcon: UITabBarItem!
    @IBOutlet weak var changeCatLbl: UIButton!
    @IBOutlet weak var changeSubCatBtn: UIButton!
    
    func didSearchWithSuccess(_ professionals: [ProfCellData],_ pageCount:Int) {
        self.pageCount = pageCount
        nextPage += 1
        profs.append(contentsOf: professionals)
        searchProfDS.reload(profs)
        profTableView.reloadData()
        if (profs.count == 0){
            notFoundView.isHidden = false
        }
        UIViewController.removeSpinner(spinner)
    }
    
    func didSearchWithError(_ error: String) {
        print(error)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "ProfProfile", bundle: nil)
        let profProfile:ProfProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfProfileViewController") as! ProfProfileViewController
        profProfile.profCode = profs[indexPath.row].profCode
        profProfile.indDisp = profs[indexPath.row].indDisp
        profProfile.distance = profs[indexPath.row].distance.components(separatedBy: ".")[0]+","+profs[indexPath.row].distance.components(separatedBy: ".")[1]
        profProfile.subCategoryID = profs[indexPath.row].subCatID
        self.present(profProfile, animated: true, completion: nil)
    }
    
    @IBAction func changeAdressBtn1(_ sender: Any) {
        goToChangeAdressView()
    }
    
    @IBAction func changeAdressBtn2(_ sender: Any) {
        goToChangeAdressView()
    }
    
    @IBAction func changeAdressBtn3(_ sender: Any) {
        goToChangeAdressView()
    }
    
    @IBAction func changeCatBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
        let vc: SelectCategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func changeSubCatBtn(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
        let vc: SelectSubcategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectSubcategoryViewController") as! SelectSubcategoryViewController
        vc.catCode = catCode
        vc.catName = catName
        vc.categoryImage = catImageStr
        self.present(vc, animated: true, completion: nil)
    }

    private func goToChangeAdressView(){
        let storyboard: UIStoryboard = UIStoryboard(name: "ChangeSolicAdress", bundle: nil)
        let mapVC: ChangeSolicAdressViewController = storyboard.instantiateViewController(withIdentifier: "ChangeSolicAdress") as! ChangeSolicAdressViewController
        let navigationController = UINavigationController(rootViewController: mapVC)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentProfList = profs
            searchProfDS.reload(currentProfList)
            searchProfDS.textToFind = searchText
            profTableView.reloadData()
            return
        }
        currentProfList = profs.filter({ prof -> Bool in
            searchProfDS.textToFind = searchText
            return prof.name.lowercased().contains(searchText.lowercased())
        })
        searchProfDS.reload(currentProfList)
        profTableView.reloadData()
    }
    
    private func getSearchParams(){
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as? Int
        let catName = UserDefaults.standard.object(forKey:"categoryName") as? String
        let subcatName = UserDefaults.standard.object(forKey:"subCategoryName") as? String
        catImageStr = UserDefaults.standard.object(forKey:"categoryImage") as? String
        catImage.image =  UIImage.init(named: catImageStr!)
        self.catName = catName
        self.subcatName = subcatName
        self.catCode = catCode
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notFoundView.isHidden = true
        getSearchParams()
        changeCatLbl.setTitle(catName, for: .normal)
        changeSubCatBtn.setTitle(subcatName, for: .normal)
        let viewModel = SearchProfViewModel(AppDelegate.container.resolve(SearchProfServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        searchProfDS = SearchProfDataSource(profs,self)
        profTableView.dataSource = searchProfDS
        let adress = UserDefaults.standard.object(forKey: "SolAdress") as? String
        if(adress == "" || adress == nil) {
        adressBtn.setTitle("Escolha aqui um endereço", for: .normal)
        }else{
        adressBtn.setTitle(adress, for: .normal)
        }
        searchIcon.title = "Descobrir"
        profTableView.isEmptyRowsHidden = true
        profTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner = UIViewController.displaySpinner(onView:self.view )
        notFoundView.isHidden = true
        viewModel.getProf(nextPage)
        currentProfList = profs
        searchProfDS.reload(currentProfList)
        profTableView.reloadData()
        let adress = UserDefaults.standard.object(forKey: "SolAdress") as? String
        if(adress == "" || adress == nil) {
            adressBtn.setTitle("Escolha aqui um endereço", for: .normal)
        }else{
            adressBtn.setTitle(adress, for: .normal)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        currentProfList = []
        profs = []
        nextPage = 1
    }
}
