import Foundation

protocol ChangeSolicAdressViewModelDelegate{
    
    func didSaveAdress(_ success:String)
    
    func didGetAdress(_ success:SolicAdress)
}

protocol ChangeSolicAdressViewModelProtocol {
    
    var delegate:ChangeSolicAdressViewModelDelegate?{get set}
    
    func save(_ adress:SolicAdress)
    
    func get()
}

class ChangeSolicAdressViewModel:ChangeSolicAdressViewModelProtocol{
    var delegate: ChangeSolicAdressViewModelDelegate?
    var service:ChangeSolicAdressServiceProtocol?
    
    init(_ service:ChangeSolicAdressServiceProtocol){
        self.service = service
    }
    
    func save(_ adress: SolicAdress) {
        service!.saveAdress(adress) { (success) in
            self.delegate?.didSaveAdress(success)
        }
    }
    
    func get() {
        service!.getAdress { (adress) in
            self.delegate?.didGetAdress(adress)
        }
    }
}
