import Foundation

struct SolicAdress{
    var latitude:Double?
    var longitude:Double?
    var adress:String?
    var reference:String?
}

protocol ChangeSolicAdressServiceProtocol{
    typealias OnSave = ((String) -> Void)
    typealias OnGet = ((SolicAdress) -> Void)
    
    func saveAdress(_ adress:SolicAdress,
                    _ save:@escaping OnSave)
    
    func getAdress(_ get:@escaping OnGet)
}

class ChangeSolicAdress:ChangeSolicAdressServiceProtocol{
    
    func saveAdress(_ adress:SolicAdress,
                    _ save:@escaping OnSave) {
        
        UserDefaults.standard.set(adress.adress, forKey: "SolAdress")
        UserDefaults.standard.set(adress.latitude, forKey: "SolLatitude")
        UserDefaults.standard.set(adress.longitude, forKey: "SolLongitude")
        UserDefaults.standard.set(adress.reference, forKey: "SolReference")
        save("salvo com sucesso")
    }
    
    func getAdress(_ get:@escaping OnGet){
        
        let SolAdress = SolicAdress(
            latitude:UserDefaults.standard.object(forKey: "SolLatitude") as? Double,
            longitude:UserDefaults.standard.object(forKey: "SolLongitude") as? Double,
            adress:UserDefaults.standard.object(forKey: "SolAdress") as? String,
            reference:UserDefaults.standard.object(forKey: "SolReference") as? String)
        get(SolAdress)
    }
}
