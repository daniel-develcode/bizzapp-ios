import UIKit
import GoogleMaps
import GooglePlaces

class ChangeSolicAdressViewController:UIViewController,ChangeSolicAdressViewModelDelegate,UISearchBarDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,
GMSAutocompleteViewControllerDelegate  {
    
    @IBOutlet weak var mapContainer: GMSMapView!
    @IBOutlet weak var referenceTxt: UITextField!
    var solAdress:SolicAdress?
    var currentAdress = SolicAdress(latitude: 0.0,longitude: 0.0,adress: "",reference:"")
    var locationManager = CLLocationManager()
    var viewModel:ChangeSolicAdressViewModelProtocol!
    @IBOutlet weak var AdressLabel: UINavigationItem!
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.locationManager.startUpdatingLocation()
        self.present(autocompleteController, animated:true, completion:nil)
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        currentAdress.reference = referenceTxt.text
        viewModel.save(currentAdress)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didSaveAdress(_ success: String) {
        print(success)
    }
    
    func didGetAdress(_ success: SolicAdress) {
        self.solAdress = success
        self.currentAdress = success
        self.referenceTxt.text = currentAdress.reference
        self.AdressLabel.title = currentAdress.adress
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16.0)
        mapContainer.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        marker.title = place.name
        marker.map = mapContainer
        marker.icon = self.getMarkerImage()
        AdressLabel.title = place.formattedAddress
        currentAdress.adress = place.formattedAddress
        currentAdress.latitude = place.coordinate.latitude
        currentAdress.longitude = place.coordinate.longitude
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("falhou ao completar\(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func iniGoogleMaps(){
        let camera = GMSCameraPosition.camera(withLatitude: currentAdress.latitude ?? 0, longitude: currentAdress.longitude ?? 0, zoom: 16.0)
        mapContainer.camera = camera
        self.mapContainer.delegate = self
        self.mapContainer.isMyLocationEnabled = true
        self.mapContainer.settings.myLocationButton = true
        if (currentAdress.adress != "") {let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: currentAdress.latitude ?? 0, longitude: currentAdress.longitude ?? 0)
            marker.title = currentAdress.adress
            marker.map = mapContainer
            marker.icon = self.getMarkerImage()
        }
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapContainer.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (currentAdress.adress == ""){
            let location = locations.last
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 16.0)
            self.mapContainer.animate(to: camera)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("erro ao conseguir a localização\(error)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
        let viewModel = ChangeSolicAdressViewModel(AppDelegate.container.resolve(ChangeSolicAdressServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        viewModel.get()
        iniGoogleMaps()
    }
}
