import Foundation

protocol SaveSolicitationViewModelDelegate{
    
    func didSaveWithSuccess(_ successMessage:String)
    func didSaveWithError(_ errorMessage:String)
    
    func didGetSeqSuccess(_ seq:Int)
    func didGetSeqError(_ error:String)
    
}

protocol SaveSolicitationViewModelProtocol{
    
    var delegate:SaveSolicitationViewModelDelegate?{get set}
    
    func save(_ params:SolicitationParams)
    func getSeq()
}

class SaveSolicitationViewModel:SaveSolicitationViewModelProtocol{
    
    var delegate: SaveSolicitationViewModelDelegate?
    var service:SaveSolicitationServiceProtocol
    
    init(_ saveServ: SaveSolicitationServiceProtocol) {
        service = saveServ
    }
    
    func getSeq(){
        service.getSeqSol({ (seq) in
            self.delegate?.didGetSeqSuccess(seq)
        }) { (err) in
            self.delegate?.didGetSeqError(err)
        }
    }
    
    func save(_ params: SolicitationParams) {
        
        service.saveSolicitationService(params, { (successMessage) in
            self.delegate?.didSaveWithSuccess(successMessage)
        }) { (errorMessage) in
            self.delegate?.didSaveWithError(errorMessage)
        }
    }
    
}
