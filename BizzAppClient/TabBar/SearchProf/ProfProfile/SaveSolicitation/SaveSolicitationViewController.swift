import UIKit
import Foundation
import AVFoundation
import AVKit

struct solDates{
    var schedDate:String
    var solDate:String
}

class SaveSolicitationViewController: UIViewController,SaveSolicitationViewModelDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ImageReceiverDelegate,VideoReceiverDelegate {
    var importedImage: UIImage?
    var viewModel:SaveSolicitationViewModelProtocol!
    var params:SolicitationParams!
    var outputSubCat:ProfServParam?
    var param:getWithIdParams?
    var day:String?
    var hour:String?
    var selectServiceDS:SelectServiceDataSource!
    var imageUrl:[String] = []
    var image:[UIImage] = []
    var progressIsHidden:Bool = true
    var progress:Double = 0
    var seqSol:Int?
    var isUploading:Bool = false
    var uploadedVideoUrl:String?
    var videoLocalUrl:URL?
    var solAdressParams:SolicAdress!
    @IBOutlet weak var complementText: UITextField!
    @IBOutlet weak var schedGalery: UICollectionView!
    @IBOutlet weak var galeryCollection: UICollectionView!
    @IBOutlet weak var watchVideoBtn: UIButton!
    @IBOutlet weak var recordVideoBtn: UIButton!
    @IBOutlet weak var deleteVideoBtn: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    
    func didSaveWithSuccess(_ successMessage: String) {
        print(successMessage)
    }
    
    func didSaveWithError(_ errorMessage: String) {
        print(errorMessage)
        self.alertMessage(titleAlert: "Error", messageAlert: "Erro ao salvar", buttonAlert: "OK")
    }
    
    func didGetSeqSuccess(_ seq: Int) {
        print(seq)
        seqSol = seq
        galeryCollection.reloadData()
    }
    
    func didGetSeqError(_ error: String) {
        print(error)
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        if (progressBar.isHidden == true){
            if(isUploading == false) {
                let sDates = dateFormatter()
                let validUserCode = UserDefaults.standard.object(forKey:"userCode")
                let userCodeInt = validUserCode as! Int
                
                params = SolicitationParams(
                    userCode:userCodeInt,
                    profCode:outputSubCat?.profCode,
                    seqServ:outputSubCat?.numServ,
                    seqSol:seqSol,
                    solicDate:sDates.schedDate,
                    schedDate:sDates.solDate,
                    txtObs:complementText.text,
                    indStatus:outputSubCat?.indInac,
                    price:outputSubCat?.price,
                    adress:solAdressParams.adress,
                    reference:solAdressParams.reference,
                    lat:solAdressParams.latitude,
                    lon:solAdressParams.longitude,
                    token:param?.token,
                    imageUrl:imageUrl,
                    videoUrl:uploadedVideoUrl
                )
                viewModel.save(params)
                
                let storyboard: UIStoryboard = UIStoryboard(name: "ProfProfile", bundle: nil)
                let vc: TransitionViewController = storyboard.instantiateViewController(withIdentifier: "TransitionViewController") as! TransitionViewController
                self.present(vc, animated: true, completion: nil)
            }else{
                self.alertMessage(titleAlert: "Aguarde", messageAlert: "carregando foto.", buttonAlert: "OK")
            }
        }else{
            self.alertMessage(titleAlert: "Aguarde", messageAlert: "carregando video.", buttonAlert: "OK")
        }
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    private func dateFormatter() -> solDates{
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result1 = formatter.string(from: date)
        formatter.dateFormat = "HH:mm:ss"
        let result2 = formatter.string(from: date)
        let solDay = (day!.components(separatedBy:"-")[2])+"-"+(day!.components(separatedBy:"-")[1])+"-"+(day!.components(separatedBy:"-")[0])
        let sDates = solDates(schedDate: solDay+"T"+hour!+"-00",solDate: result1+"T"+result2+"-00")
        return sDates
    }
    ////////////////////////////////////////////////
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (section == 0){
            return 1
        }
        else{
            return imageUrl.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 100, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.section == 0){
            guard let schedUploadCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SchedUploadCell", for: indexPath) as? SchedUploadCell else { return UICollectionViewCell()}
            schedUploadCell.imageReceiver = self
            schedUploadCell.viewController = self
            if (progressIsHidden == false){
                schedUploadCell.progressBar.isHidden = false
                schedUploadCell.progressBar.progress = Float(progress)
                schedUploadCell.uploadBtn.setImage(importedImage, for: .normal)
            }else{
                schedUploadCell.progressBar.isHidden = true
                schedUploadCell.progressBar.progress = 0
                schedUploadCell.uploadBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            }
            schedUploadCell.layer.borderWidth = 1
            schedUploadCell.thisView = self
            schedUploadCell.seqSol = seqSol
            return schedUploadCell
        }else{
            guard let schedGaleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SchedGaleryCell", for: indexPath) as? SchedGaleryCell else { return UICollectionViewCell()}
            schedGaleryCell.photoBtn.setImage(image[indexPath.row], for: .normal)
            schedGaleryCell.viewController = self
            schedGaleryCell.thisView = self
            schedGaleryCell.imageIndex = indexPath.row
            schedGaleryCell.layer.borderWidth = 1
            return schedGaleryCell
        }
    }
    ////////////////////////////////////////////////////////
    @IBAction func uploadVideo(_ sender: Any) {
        if(seqSol != nil){
            let vc = UIStoryboard(name: "VideoPicker", bundle: nil).instantiateViewController(withIdentifier: "VideoPickerViewController") as! VideoPickerViewController
            vc.seqSol = seqSol
            vc.videoReceiverDelegate = self
            vc.view.frame = self.view.bounds
            self.addChild(vc)
            UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                              animations: {self.view.addSubview(vc.view)}, completion: nil)
        }else{
            self.viewModel.getSeq()
            self.alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado, tente novamente.", buttonAlert: "OK")
        }
    }
    
    func videoUploadProgress(_ progress: Double, _ videoReference: String) {
        print(progress)
        progressBar.isHidden = false
        progressBar.progress = Float(progress)
    }
    
    func videoUploadSucess(_ downloadURL: String, _ videoReference: String) {
        progressBar.isHidden = true
        uploadedVideoUrl = downloadURL
        alertMessage(titleAlert: "Sucesso", messageAlert: "Video adicionado com sucesso.", buttonAlert: "OK!")
    }
    
    func videoUploadError(_ errorMessage: String) {
        progressBar.isHidden = true
        print(errorMessage)
        alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado.", buttonAlert: "OK!")
    }
    
    @IBAction func watchVideo(_ sender: Any) {
        if (seqSol != nil){
            let paths = NSSearchPathForDirectoriesInDomains(
                FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory: URL = URL(fileURLWithPath: paths[0])
            let dataPath = documentsDirectory.appendingPathComponent("/video_"+String(seqSol!)+".mp4")
            print(dataPath.absoluteString)
            let videoAsset = (AVAsset(url: dataPath))
            let playerItem = AVPlayerItem(asset: videoAsset)
            let player = AVPlayer(playerItem: playerItem)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    private func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    @IBAction func deleteVideo(_ sender: Any) {
        watchVideoBtn.isHidden = true
        deleteVideoBtn.isHidden = true
        recordVideoBtn.isHidden = false
        uploadedVideoUrl = nil
    }
    
    ////////////////////////////////////////////////////////
    
    func imageUploadProgress(_ progress: Double, _ imageReference: String) {
        isUploading = true
        self.progress = progress
        progressIsHidden = false
        galeryCollection.reloadData()
    }
    
    func imageUploadSucess(_ downloadURL: String, _ imageReference: String) {
        isUploading = false
        self.progress = 0
        image.append(importedImage!)
        imageUrl.append(downloadURL)
        progressIsHidden = true
        galeryCollection.reloadData()
        alertMessage(titleAlert: "Sucesso", messageAlert: "Imagem adicionada com sucesso.", buttonAlert: "OK!")
    }
    
    func imageUploadError(_ errorMessage: String) {
        isUploading = false
        galeryCollection.reloadData()
        alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado.", buttonAlert: "OK!")
    }
    ////////////////////////////////////////////////////////////////
    override func viewDidAppear(_ animated: Bool) {
        if let url = videoLocalUrl{
            var image = thumbnailForVideoAtURL(url: url)
            image = UIImage(cgImage: image!.cgImage!, scale: CGFloat(1.0), orientation: .right)
            watchVideoBtn.setBackgroundImage(image, for: .normal)
            watchVideoBtn.isHidden = false
            recordVideoBtn.isHidden = true
            deleteVideoBtn.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image = #imageLiteral(resourceName: "cam")
        let size = CGSize(width: 50, height: 50)
        let resizedImage = image.resize(size: size)
        recordVideoBtn.setImage(resizedImage, for: .normal)
        var imagePlay = #imageLiteral(resourceName: "play")
        let cgsize:CGSize = CGSize(width: 50, height: 50)
        imagePlay = imagePlay.resize(size: cgsize)
        watchVideoBtn.setImage(imagePlay, for: .normal)
        recordVideoBtn.layer.borderColor = UIColor.black.cgColor
        recordVideoBtn.layer.borderWidth = 1
        watchVideoBtn.layer.borderColor = UIColor.black.cgColor
        watchVideoBtn.layer.borderWidth = 1
        let viewModel = SaveSolicitationViewModel(AppDelegate.container.resolve(SaveSolicitationServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        viewModel.getSeq()
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SchedUploadCell:UICollectionViewCell{
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    var imageReceiver:ImageReceiverDelegate?
    var viewController:UIViewController?
    var thisView:SaveSolicitationViewController?
    var seqSol:Int?
    var path:String!
    
    @IBAction func uploadImage(_ sender: Any) {
        if(seqSol != nil){
            if (progressBar.isHidden == true){
                if((thisView?.imageUrl.count)! < 4){
                    let vc = UIStoryboard(name: "ImagePicker", bundle: nil).instantiateViewController(withIdentifier: "ImagePickerViewController") as! ImagePickerViewController
                    vc.imageReceiverDelegate = imageReceiver
                    vc.view.frame = viewController!.view.bounds
                    vc.seqSol = seqSol
                    viewController!.addChild(vc)
                    UIView.transition(with: viewController!.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                                      animations: {self.viewController!.view.addSubview(vc.view)}, completion: nil)
                }else{
                    viewController!.alertMessage(titleAlert: "Erro", messageAlert: "Quantidade máxima de fotos atingida.", buttonAlert: "OK!")
                }
            }else{
                viewController!.alertMessage(titleAlert: "Erro", messageAlert: "Aguarde, carregando foto...", buttonAlert: "OK!")
            }
        }else{
            thisView?.viewModel.getSeq()
            viewController!.alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado, tente novamente.", buttonAlert: "OK")
        }
    }
}

class SchedGaleryCell:UICollectionViewCell{
    @IBOutlet weak var photoBtn: UIButton!
    var viewController:UIViewController?
    var thisView:SaveSolicitationViewController?
    var imageIndex:Int?
    
    @IBAction func removeImage(_ sender: Any) {
        thisView?.imageUrl.remove(at: imageIndex!)
        thisView?.image.remove(at: imageIndex!)
        thisView?.galeryCollection.reloadData()
    }
    
    @IBAction func Fullscreen(_ sender: Any) {
        
        let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
        vc.receivedImage = photoBtn.currentImage
        vc.view.frame = (viewController?.view.bounds)!
        viewController!.addChild(vc)
        UIView.transition(with: (viewController?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.viewController!.view.addSubview(vc.view)}, completion: nil)
    }
}

// formatter.dateFormat = "x"
// let gmt = formatter.string(from: date)
