import Foundation
import Alamofire

protocol SaveSolicitationServiceProtocol {
    typealias didSaveSolicitationSuccess = ((String) -> Void)
    typealias didSaveSolicitationError = ((String) -> Void)
    
    typealias didGetSeqSuccess = ((Int) -> ())
    typealias didGetSeqError = ((String) -> ())
    
    func saveSolicitationService(_ params:SolicitationParams,
                                 _ success:@escaping didSaveSolicitationSuccess,
                                 _ error:@escaping didSaveSolicitationError)
    func getSeqSol(_ sucess:@escaping didGetSeqSuccess,
                   _ error:@escaping didGetSeqError)
    
}

class SaveSolicitationService:SaveSolicitationServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol){
        self.service = service
    }
    
    func getSeqSol(_ sucess:@escaping didGetSeqSuccess,
                   _ error:@escaping didGetSeqError){
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"solicitation/find-serial-sequence-solicitation"
        let token = UserDefaults.standard.object(forKey:"userToken") as? String
        var validToken:String = ""
        if (token != nil){
            validToken = token!
        }else{
            error("erro ao recuperar o token")
        }
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":validToken,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        service.request(url, .get, nil, header, { (data) in
            let validData = Decoders.decodeData(data!) as GetSeqParams
            sucess(validData.seqSol)
        }) { (err) in
            error(err)
        }
    }
    
    func saveSolicitationService(_ params: SolicitationParams,
                                 _ success: @escaping didSaveSolicitationSuccess,
                                 _ error: @escaping didSaveSolicitationError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"solicitation/save-solicitation"
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token!,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        var images:[Parameters] = []
        for image in params.imageUrl!{
            let lsImages:Parameters = [
                "desImagem":image,
                "indInativo":0,
                "tipo":1
            ]
            images.append(lsImages)
        }
        
        if let video = params.videoUrl{
            let lsImages:Parameters = [
                "desImagem":video,
                "indInativo":0,
                "tipo":2
            ]
            images.append(lsImages)
        }
        
        let seqSol = params.seqSol!
        let userCode = params.userCode!
        let profCode = params.profCode!
        let seqServ = params.seqServ!
        let schedDate = params.schedDate!
        let solicDate = params.solicDate!
        let txtObs = params.txtObs!
        let price = params.price!
        let adress = params.adress ?? ""
        let lat = params.lat ?? 0
        let lon = params.lon ?? 0
        let reference = params.reference ?? ""
        
        
        let parameters:Parameters = [
            "seqSolicitacao":seqSol,
            "codUsuario":userCode,
            "codProfissional":profCode,
            "seqServico":seqServ,
            "dtaSolicitacao":schedDate,
            "dtaAgendamento":solicDate,
            "txtObs":txtObs,
            "indStatus":1,
            "vlrPreco":price,
            "desStringEndereco":adress,
            "numLatitude":lat,
            "numLongitude":lon,
            "lstImagens":images,
            "desStringReferencia": reference
            ] as [String : Any]
        
        print(parameters)
        
        service.request(url, .post, parameters, header, { (data) in
            success("sucesso ao salvar")
        }) { (errorMessage) in
            error(errorMessage)
        }
    }
}

