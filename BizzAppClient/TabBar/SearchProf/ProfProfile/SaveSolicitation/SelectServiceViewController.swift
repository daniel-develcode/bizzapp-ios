import UIKit
import Foundation

class SelectServiceViewController: UIViewController,UITableViewDelegate {
    var outputSubCats:[ProfServParam]?
    var params:getWithIdParams?
    var day:String?
    var hour:String?
    var subCat:ProfServParam?
    var selectServiceDS:SelectServiceDataSource!
    var profile:GetProfProfile?
    var selectedAdress:SolicAdress!
    @IBOutlet weak var selectBtnLbl: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var selectServiceTable: UITableView!
    @IBOutlet weak var ownResidenceBtn: UIButton!
    @IBOutlet weak var profResidenceBtn: UIButton!
    @IBOutlet weak var selectPlaceLbl: UILabel!
    @IBOutlet weak var ownResidenceLbl: UILabel!
    @IBOutlet weak var profResidenceLbl: UILabel!
    @IBOutlet weak var ownResidenceImage: UIImageView!
    @IBOutlet weak var profResidenceImage: UIImageView!
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectServiceDS.reloadData(outputSubCats!, indexPath.row, true)
        subCat = outputSubCats![indexPath.row]
        btnIsEnabled(true)
        table.reloadData()
    }
    
    @IBAction func selectBtn(_ sender: Any) {
        
        if (outputSubCats!.count != 0){
            let vc = UIStoryboard(name: "ProfProfile", bundle: nil).instantiateViewController(withIdentifier: "SaveSolicitationViewController") as! SaveSolicitationViewController
            vc.day = day
            vc.hour = hour
            vc.param = params
            vc.outputSubCat = subCat
            vc.solAdressParams = selectedAdress
            vc.view.frame = self.view.bounds
            self.addChild(vc)
            UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,animations:{
                self.view.addSubview(vc.view)}, completion: nil)
        } else{
            print("não existem horários disponíveis neste dia")
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectServiceDS.reloadData(outputSubCats!, 0, false)
        btnIsEnabled(false)
        table.reloadData()
    }
    
    private func btnIsEnabled(_ isEnabled:Bool){
        selectBtnLbl.isEnabled = isEnabled
        if (isEnabled == true){
            showSelectBtn()
        }else{
            hiddenSelectBtn()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    private func getUserSolAdress() -> SolicAdress{
        let latitude = UserDefaults.standard.object(forKey: "SolLatitude") as? Double
        let longitude = UserDefaults.standard.object(forKey: "SolLongitude") as? Double
        let adress = UserDefaults.standard.object(forKey: "SolAdress") as? String
        let reference = UserDefaults.standard.object(forKey: "SolReference") as? String
        let validLatitude = latitude ?? 0
        let validLongitude = longitude ?? 0
        let validAdress = adress ?? ""
        let validReference = reference ?? ""
        return SolicAdress(
            latitude:validLatitude,
            longitude:validLongitude,
            adress:validAdress,
            reference:validReference
        )
    }
    
    @IBAction func ownResidenceAction(_ sender: Any) {
        ownResidenceBtn.setImage(#imageLiteral(resourceName: "radio-on"), for: .normal)
        profResidenceBtn.setImage(#imageLiteral(resourceName: "radio-off"), for: .normal)
        selectedAdress = getUserSolAdress()
    }
    @IBAction func profResidenceAction(_ sender: Any) {
        profResidenceBtn.setImage(#imageLiteral(resourceName: "radio-on"), for: .normal)
        ownResidenceBtn.setImage(#imageLiteral(resourceName: "radio-off"), for: .normal)
        selectedAdress = SolicAdress(
            latitude:profile?.profLat ?? 0,
            longitude:profile?.profLon ?? 0,
            adress:profile?.profAdress ?? "",
            reference:profile?.profReference ?? ""
        )
    }
    
    private func setSelectPlace(){
        if ((profile?.indAttEstab == 1)&&(profile?.indAttResidence == 1)) {
            ownResidenceBtn.isHidden = false
            profResidenceBtn.isHidden = false
            selectPlaceLbl.isHidden = false
            ownResidenceLbl.isHidden = false
            profResidenceLbl.isHidden = false
            profResidenceImage.isHidden = false
            ownResidenceImage.isHidden = false
            selectedAdress = getUserSolAdress()
        }else if((profile?.indAttEstab == 0)&&(profile?.indAttResidence == 1)){
            selectedAdress = getUserSolAdress()
        }else if ((profile?.indAttEstab == 1)&&(profile?.indAttResidence == 0)){
            selectedAdress = SolicAdress(
                latitude:profile?.profLat ?? 0,
                longitude:profile?.profLon ?? 0,
                adress:profile?.profAdress ?? "",
                reference:profile?.profReference ?? ""
            )
        }
    }
    
    private func setServices(){
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as! Int
        let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as! Int
        var servs:[ProfServParam] = []
        for serv in outputSubCats!{
            if (catCode == serv.codeCat)&&(subcatCode == serv.codeSubCat){
                servs.append(serv)
            }
        }
        outputSubCats = servs
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setServices()
        hiddenSelectBtn()
        selectServiceDS = SelectServiceDataSource(outputSubCats!,0,false)
        selectServiceTable.dataSource = selectServiceDS
        table.isEmptyRowsHidden = true
        setSelectPlace()
    }
    
    private func hiddenSelectBtn(){
        let defaultColor =  UIColor(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
        selectBtnLbl.setTitleColor(defaultColor, for: .normal)
        selectBtnLbl.isEnabled = false
    }
    
    private func showSelectBtn(){
        let defaultColor =  UIColor.black
        selectBtnLbl.setTitleColor(defaultColor, for: .normal)
        selectBtnLbl.isEnabled = true
    }
}

class SelectServiceCell:UITableViewCell{
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var hexagonoImage: UIImageView!
}
