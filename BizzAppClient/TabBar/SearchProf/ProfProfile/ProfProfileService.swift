import Foundation
import Alamofire

protocol ProfProfileServiceProtocol{
    typealias didGetProfSuccess = ((GetProfProfile) -> Void)
    typealias didGetProfError = ((String) -> Void)
    
    func getProf(_ params: getWithIdParams,
                 _ success:@escaping didGetProfSuccess,
                 _ error:@escaping didGetProfError)
}

class ProfProfileService:ProfProfileServiceProtocol {
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol){
        self.service = service
    }
    
    func getProf(_ params: getWithIdParams,
                 _ success: @escaping didGetProfSuccess,
                 _ error: @escaping didGetProfError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"professional/find-by-id?codProfissional="+params.userCode
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(url, .get, nil, header, { data in
            success(Decoders.decodeData(data!))
        }) { (errorMessage) in
            error(errorMessage)
        }
    }
}
