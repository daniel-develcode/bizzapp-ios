import UIKit
import Foundation

class ProfProfileViewController: UIViewController,ProfProfileViewModelDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var profCode:Int?
    var spinner:UIView!
    var indDisp:Int?
    var distance:String?
    var subCategoryID:Int?
    var viewModel:ProfProfileViewModelProtocol!
    var paramToken:String!
    var paramCode:String!
    var outputSubCats:[ProfServParam] = []
    var profData:profProfileData!
    var profile:GetProfProfile?
    var receiveInd:Bool = false
    var services:[ProfServ] = []
    var facebook:String!
    var googlePlus:String!
    var instagram:String!
    var site:String!
    var twitter:String!
    @IBOutlet var thisView: UIView!
    @IBOutlet weak var table: UITableView!
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (receiveInd == false){
            return 0
        }else if (section == 4){
            return services.count
        }else if (section == 6 && profile?.profImages?.count == 0){
            return 0
        }else if (section == 9 && profile?.profEval?.count == 0){
            return 0
        }else if (section == 9 && (profile?.profEval?.count)! > 0){
            return (profile?.profEval?.count)!
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if (indexPath.section == 0){
            guard let profileCell = tableView.dequeueReusableCell(withIdentifier: "ProfProfileCell")! as? ProfProfileCell else { return UITableViewCell()}
            profileCell.profDistance.text = distance
            profileCell.profGrade.text = String(profile!.profGrade ?? 0)
            profileCell.profName.text = profile!.profName
            if (indDisp == 0){
                profileCell.indDispLbl.isHidden = true
            }
            profileCell.view = self
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(profile!.profImage ?? "", { (image,url) in
                profileCell.profImage.setImage(image, for: .normal)
            }) { (error) in
                print(error)
            }
            return profileCell
            
        }else if (indexPath.section == 1) {
            guard let aboutCell = tableView.dequeueReusableCell(withIdentifier: "ProfAboutCell")! as? ProfAboutCell else { return UITableViewCell()}
            aboutCell.about.text = profile?.descTxt
            return aboutCell
            
        }else if (indexPath.section == 2){
           
            guard let socialMediaCell = tableView.dequeueReusableCell(withIdentifier: "SocialMediaCell")! as? SocialMediaCell else {return UITableViewCell()}
            if (site == ""){
                socialMediaCell.siteLbl.isHidden = true
            }else{
                socialMediaCell.siteURL = site
            }
            if (twitter == ""){
                socialMediaCell.twitterLbl.isHidden = true
            }else{
                socialMediaCell.twitterURL = twitter
            }
            if (instagram == ""){
                socialMediaCell.instagramLbl.isHidden = true
            }else{
                socialMediaCell.instagramURL = instagram
            }
            if (facebook == ""){
                socialMediaCell.facebookLbl.isHidden = true
            }else{
               socialMediaCell.facebookURL = facebook
            }
            if (googlePlus == ""){
                socialMediaCell.googlePlusLbl.isHidden = true
            }else{
                socialMediaCell.googleplusURL = googlePlus
            }
            return socialMediaCell
            
        }else if (indexPath.section == 3){
            guard let serviceLabelCell = tableView.dequeueReusableCell(withIdentifier: "ServiceLabelCell")! as? ServiceLabelCell else {return UITableViewCell()}
            return serviceLabelCell
            
        }else if (indexPath.section == 4){
            guard let serviceCell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell")! as? ServiceCell else {return UITableViewCell()}
            serviceCell.desServ.text = services[indexPath.row].desServ
            let priceNbr = services[indexPath.row].price ?? 0
            let formatter = NumberFormatter()
            formatter.locale = Locale.current
            formatter.numberStyle = .currency
            let formattedPriceStr = formatter.string(from: priceNbr as NSNumber) ?? "0"
            let codeUmStr = services[indexPath.row].codeUm ?? "un"
            serviceCell.price.text = formattedPriceStr + " / " + codeUmStr
            if(indexPath.row%2 == 1){
                serviceCell.backgroundColor = UIColor.white
            }else{
                serviceCell.backgroundColor = UIColor.init(red: 250/264, green: 250/264, blue: 250/264, alpha: 1)
            }
            return serviceCell
            
        }else if (indexPath.section == 5) {
            
            guard let whereCell = tableView.dequeueReusableCell(withIdentifier: "WhereCell")! as? WhereCell else { return UITableViewCell()}
            if((profile?.indAttResidence) == 0 ){
                whereCell.inResidence.isHidden = true
            }
            if((profile?.indAttEstab) == 0 ){
                whereCell.inResidence.isHidden = true
                whereCell.inOwnPlaceLbl.text = "Em Domicílio"
                whereCell.inOwnPlaceImage.image = #imageLiteral(resourceName: "ic_house")
            }
            return whereCell
        }else if (indexPath.section == 6){
            guard let galeryCell = tableView.dequeueReusableCell(withIdentifier: "GaleryCell")! as? GaleryCell else {return UITableViewCell()}
            return galeryCell
            
        }else if (indexPath.section == 7){
            guard let serviceNumberCell = tableView.dequeueReusableCell(withIdentifier: "ServiceNumberCell")! as? ServiceNumberCell else {return UITableViewCell()}
            serviceNumberCell.serviceNumber.text = "Este profissional já realizou " + String(profile?.servicesDone ?? 0) + " serviço/s"
            return serviceNumberCell
            
        }else if (indexPath.section == 8 && (profile?.profEval?.count ?? 0) > 0){
            guard let evaluationsNumberCell = tableView.dequeueReusableCell(withIdentifier: "EvaluationsNumberCell")! as? EvaluationsNumberCell else {return UITableViewCell()}
            evaluationsNumberCell.evaluationsNumber.text = "Avaliações (" + String(profile?.profEval?.count ?? 0) + ")"
            return evaluationsNumberCell
            
        }else if (indexPath.section == 9 && (profile?.profEval?.count ?? 0) > 0){
            guard let clientEvaluationsCell = tableView.dequeueReusableCell(withIdentifier: "ClientEvaluationsCell")! as? ClientEvaluationsCell else {return UITableViewCell()}
            clientEvaluationsCell.clientName.text = profile?.profEval![indexPath.row].userName
            clientEvaluationsCell.clientOBs.text = profile?.profEval![indexPath.row].txtObs
            clientEvaluationsCell.clientGrade.text = String(profile?.profEval![indexPath.row].grade ?? 0.0).components(separatedBy: ".")[0]
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(profile?.profEval![indexPath.row].userImage ?? "", { (image,url) in
                UIView.transition(with: clientEvaluationsCell, duration: 0.3, options: .transitionCrossDissolve, animations: {clientEvaluationsCell.clientImage.image = image}, completion: nil)
            }) { (error) in
                print(error)
            }
            return clientEvaluationsCell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if (indexPath.section == 6){
            return 155
        }else if ((indexPath.section == 9)&&(profile?.profEval![indexPath.row].txtObs == "")){
            return 86
         }else if (indexPath.section == 2) && ((facebook  == "")&&(googlePlus  == "")&&(instagram  == "")&&(site  == "")&&(twitter  == "")){
            return 0
         }else{
            return UITableView.automaticDimension
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (profile == nil){
            return 0
        }else {
            let images =  profile!.profImages ?? []
            return images.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(profile!.profImages![indexPath.row], { (image,url) in
                cell.photo.setImage(image, for: .normal)
            }) { (error) in
                print(error)
            }
        cell.vcView = self
        return cell
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @IBAction func ScheduleBtn(_ sender: Any) {
        let vc = UIStoryboard(name: "ProfProfile", bundle: nil).instantiateViewController(withIdentifier: "GetDaysViewController") as! GetDaysViewController
        let str = getWithIdParams(
            token: paramToken,
            userCode: paramCode)
        vc.params = str
        vc.outputSubCats = self.outputSubCats
        vc.view.frame = self.view.bounds
        vc.profile = profile
        self.addChild(vc)
        UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.view.addSubview(vc.view)}, completion: nil)
    }
    
    func didGetWithSuccess(_ profile:GetProfProfile, _ data:profProfileData,_ subCats:[ProfServParam]) {
        profData = data
        paramToken = data.token
        paramCode = data.profCode
        outputSubCats = subCats
        self.profile = profile
        receiveInd = true
        table.reloadData()
        UIViewController.removeSpinner(spinner)
        facebook = profile.faceUrl ?? ""
        googlePlus = profile.googUrl ?? ""
        instagram = profile.instUrl ?? ""
        site = profile.portUrl ?? ""
        twitter = profile.twitUrl ?? ""
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as! Int
        for serv in profile.profServices!{
            if(catCode == serv.codeCat){
                
            services.append(serv)
            }
        }
        table.reloadData()
    }
    
    func didGetWithError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.isEmptyRowsHidden = true
        table.allowsSelection = false
        let viewModel = ProfProfileViewModel(AppDelegate.container.resolve(ProfProfileServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner = UIViewController.displaySpinner(onView:self.view )
        viewModel.getProfile(profCode!,subCategoryID!)
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

class ProfProfileCell:UITableViewCell{
    @IBOutlet weak var profImage: UIButton!
    @IBOutlet weak var profName: UILabel!
    @IBOutlet weak var profGrade: UILabel!
    @IBOutlet weak var profDistance: UILabel!
    @IBOutlet weak var indDispLbl: UILabel!
    var view:UIViewController!
    
    @IBAction func profImageFullscreen(_ sender: Any) {
        let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
        vc.receivedImage = profImage.currentImage
        vc.view.frame = (view?.view.bounds)!
        view!.addChild(vc)
        UIView.transition(with: (view?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.view!.view.addSubview(vc.view)}, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        view.dismiss(animated: true, completion: nil)
    }
}

class ProfAboutCell:UITableViewCell{
    @IBOutlet weak var about: UILabel!
}

class SocialMediaCell:UITableViewCell{
    var siteURL:String!
    var twitterURL:String!
    var instagramURL:String!
    var facebookURL:String!
    var googleplusURL:String!
    @IBOutlet weak var siteLbl: UIButton!
    @IBOutlet weak var twitterLbl: UIButton!
    @IBOutlet weak var instagramLbl: UIButton!
    @IBOutlet weak var facebookLbl: UIButton!
    @IBOutlet weak var googlePlusLbl: UIButton!
    
    @IBAction func siteBtn(_ sender: Any) {
        guard let url = URL(string: siteURL) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func twitterBtn(_ sender: Any) {
        guard let url = URL(string: "https://twitter.com/"+twitterURL) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func instagramBtn(_ sender: Any) {
        guard let url = URL(string: "https://www.instagram.com/"+instagramURL) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func facebookBtn(_ sender: Any) {
        guard let url = URL(string: "https://www.facebook.com/"+facebookURL) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func googleplusBtn(_ sender: Any) {
        guard let url = URL(string: "https://plus.google.com/"+googleplusURL) else { return }
        UIApplication.shared.open(url)
    }
}

class ServiceLabelCell:UITableViewCell{
}

class ServiceCell:UITableViewCell{
    @IBOutlet weak var desServ: UILabel!
    @IBOutlet weak var price: UILabel!
}

class WhereCell:UITableViewCell{
    @IBOutlet weak var inResidence: UIStackView!
    @IBOutlet weak var inOwnPlaceLbl: UILabel!
    @IBOutlet weak var inOwnPlaceImage: UIImageView!
}

class GaleryCell:UITableViewCell{
    @IBOutlet weak var galeryColletion: UICollectionView!
}

class PhotoCell:UICollectionViewCell{
    @IBOutlet weak var photo: UIButton!
    var vcView:UIViewController?
    
    @IBAction func fullscreen(_ sender: Any) {
        let vc = UIStoryboard(name: "ImageVisualizer", bundle: nil).instantiateViewController(withIdentifier: "GaleryImageViewController") as! GaleryImageViewController
        vc.receivedImage = photo.currentImage
        vc.view.frame = (vcView?.view.bounds)!
        vcView!.addChild(vc)
        UIView.transition(with: (vcView?.view)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {self.vcView!.view.addSubview(vc.view)}, completion: nil)
    }
}

extension GaleryCell {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDelegate & UICollectionViewDataSource>(_ dataSourceDelegate: D, forRow row:Int){
        galeryColletion.delegate = dataSourceDelegate
        galeryColletion.dataSource = dataSourceDelegate
        galeryColletion.reloadData()
    }
}

class ServiceNumberCell:UITableViewCell{
    @IBOutlet weak var serviceNumber: UILabel!
}

class EvaluationsNumberCell:UITableViewCell{
    @IBOutlet weak var evaluationsNumber: UILabel!
}

class ClientEvaluationsCell:UITableViewCell{
    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var clientOBs: UILabel!
    @IBOutlet weak var clientGrade: UILabel!
}
