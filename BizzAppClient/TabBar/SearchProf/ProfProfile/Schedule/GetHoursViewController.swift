import UIKit

class GetHoursViewController: UIViewController,GetHoursDelegate,UIPickerViewDelegate {
    
    var viewModel:ScheduleViewModelProtocol!
    var params:getWithIdParams?
    var day:String?
    var hours:[String] = []
    var formattedHours:[String] = []
    var hourPickerDS:HourPickerDataSource!
    var outputSubCats:[ProfServParam]?
    var profile:GetProfProfile?
    @IBOutlet weak var hourPicker: UIPickerView!
    
    @IBAction func didPressSelectBtn(_ sender: Any) {
        if (hours.count != 0){
            let vc = UIStoryboard(name: "ProfProfile", bundle: nil).instantiateViewController(withIdentifier: "SelectServiceViewController") as! SelectServiceViewController
            vc.hour = hours[hourPicker.selectedRow(inComponent: 0)]
            vc.day = day
            vc.params = params
            vc.outputSubCats = self.outputSubCats
            vc.profile = profile
            vc.view.frame = self.view.bounds
            self.addChild(vc)
            UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                              animations: {self.view.addSubview(vc.view)}, completion: nil)
        } else{
            print("não existem horários disponíveis neste dia")
        }
    }
    
    @IBAction func didPressCancelBrn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    func didGetHoursSuccess(_ hours: [String],_ formattedHour:[String]) {
        self.hours = hours
        hourPickerDS.reloadData(hours)
        self.formattedHours = formattedHour
        hourPicker.reloadAllComponents()
    }
    
    func didGetHoursError(_ errorMessage: String) {
        print("")
        print(errorMessage)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return formattedHours [row]
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let viewModel = ScheduleViewModel(AppDelegate.container.resolve(ScheduleServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.hoursDelegate = self
        let param = getWithIdParams(
            token:params!.token,
            userCode:params!.userCode)
        viewModel.getHours(param,day!)
        hourPickerDS = HourPickerDataSource(hours)
        hourPicker.dataSource = hourPickerDS
    }
}
