import Foundation
import Alamofire

protocol ScheduleServiceProtocol{
    typealias didGetScheduleSuccess = ((ScheduleDates) -> Void)
    typealias didGetScheduleError = ((String) -> Void)
    
    func getScheduleService(_ date:String,
                            _ params: getWithIdParams,
                            _ success:@escaping didGetScheduleSuccess,
                            _ error:@escaping didGetScheduleError)
}

class ScheduleService:ScheduleServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol){
        self.service = service
    }
    
    func getScheduleService(_ date:String,
                            _ params: getWithIdParams,
                            _ success: @escaping didGetScheduleSuccess,
                            _ error: @escaping didGetScheduleError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"schedule/professional"
       
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = formatter.string(from: now)
        let parameters:Parameters = [
            "monthYear": date,
            "codProfissional":params.userCode,
            "dataAtual":dateString+":00"
            ] as [String : Any]
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        service.request(url, .post, parameters, header, { (data) in
            let dates = Decoders.decodeData(data!) as ScheduleDates
            success(dates)
        }) { (requestError) in
            error(requestError)
        }
    }
}
