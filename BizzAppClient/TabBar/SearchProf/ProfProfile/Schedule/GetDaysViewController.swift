import UIKit

class GetDaysViewController: UIViewController,GetDaysDelegate,UIPickerViewDelegate {
    
    var datePickerDS:DatePickerDataSource!
    var viewModel:ScheduleViewModelProtocol!
    var params:getWithIdParams?
    var uIDates:[String] = []
    var dates:[String] = []
    var selectedMonth:Int!
    var selectedYear:Int!
    var limitDate:Int!
    var outputSubCats:[ProfServParam]?
    var profile:GetProfProfile?
    @IBOutlet weak var datePicker: UIPickerView!
    
    @IBAction func didPressSelectBtn(_ sender: Any) {
        if (dates.count != 0){
            let vc = UIStoryboard(name: "ProfProfile", bundle: nil).instantiateViewController(withIdentifier: "GetHoursViewController") as! GetHoursViewController
            vc.day = dates[datePicker.selectedRow(inComponent: 0)]
            vc.params = params
            vc.outputSubCats = self.outputSubCats
            vc.view.frame = self.view.bounds
            vc.profile = profile
            self.addChild(vc)
            UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                              animations: {self.view.addSubview(vc.view)}, completion: nil)
        } else{
            print("não existem horários disponíveis neste dia")
        }
    }
    
    @IBAction func didPressCancelBtn(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    private func getDayOfWeekString(today:String)->String {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            switch weekDay {
            case 1:
                return "Dom"
            case 2:
                return "Seg"
            case 3:
                return "Ter"
            case 4:
                return "Qua"
            case 5:
                return "Qui"
            case 6:
                return "Sex"
            case 7:
                return "Sab"
            default:
                print("erro ao indentificar o dia da semana")
                return "Day"
            }
        }else{
            print("erro ao indentificar o dia da semana")
            return "day"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return uIDates[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (selectedYear < limitDate){
            if ((row > 0) && (row > uIDates.count/2)){
                    reloadDates()
            }
        }
    }
    
    func didGetDaysSuccess(_ days: [String]) {
        
        if ((days.count <= 1) && (selectedYear < limitDate)){
            reloadDates()
        }
        for day in days{
            let currentDay = day.components(separatedBy: "-")[2] + " de " + getMonth(day.components(separatedBy: "-")[1]) + " de " + day.components(separatedBy: "-")[0]
            uIDates.append(currentDay + " " + getDayOfWeekString(today:day))
            dates.append(day)
        }
        datePickerDS.reloadData(uIDates)
        datePicker.reloadAllComponents()
    }
    
    private func getMonth(_ month:String) -> String{
        
        switch month {
        case "01":
            return "janeiro"
        case "02":
            return "feveiro"
        case "03":
            return "março"
        case "04":
            return "abril"
        case "05":
            return "maio"
        case "06":
            return "junho"
        case "07":
            return "julho"
        case "08":
            return "agosto"
        case "09":
            return "setembro"
        case "10":
            return "outubro"
        case "11":
            return "novembro"
        case "12":
            return "dezembro"
        default:
            return month
        }
    }
    
    func didGetDaysError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    private func formatInput() ->String {
        let dateString = (String(selectedMonth)+"-"+String(selectedYear))
        return dateString
    }
    
    private func addDateCounter(){
        
        if (selectedMonth < 12){
            selectedMonth = selectedMonth + 1
        } else {
            selectedMonth = 1
            selectedYear = selectedYear + 1
        }
    }
    
    private func reloadDates(){
        addDateCounter()
        let result = formatInput()
        let param = getWithIdParams(
            token:params!.token,
            userCode:params!.userCode
        )
        viewModel.getDays(param,result)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let viewModel = ScheduleViewModel(AppDelegate.container.resolve(ScheduleServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.daysDelegate = self
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-yyyy"
        let result = formatter.string(from: date)
        formatter.dateFormat = "MM"
        selectedMonth = Int(formatter.string(from: date))
        formatter.dateFormat = "yyyy"
        selectedYear = Int(formatter.string(from: date))
        limitDate = selectedYear+3
        let param = getWithIdParams(
            token:params!.token,
            userCode:params!.userCode
        )
        viewModel.getDays(param,result)
        datePickerDS = DatePickerDataSource(uIDates)
        datePicker.dataSource = datePickerDS
    }
}
