import Foundation

protocol GetDaysDelegate{
    func didGetDaysSuccess(_ days:[String])
    
    func didGetDaysError(_ errorMessage:String)
}

protocol GetHoursDelegate{
    func didGetHoursSuccess(_ hours:[String],_ formattedHours:[String])
    
    func didGetHoursError(_ errorMessage:String)
}

protocol ScheduleViewModelProtocol {
    var daysDelegate:GetDaysDelegate?{get set}
    var hoursDelegate:GetHoursDelegate?{get set}
    
    func getDays(_ params: getWithIdParams,_ date:String)
    
    func getHours(_ params: getWithIdParams,_ day:String)
}

class ScheduleViewModel:ScheduleViewModelProtocol{
    
    var hoursDelegate: GetHoursDelegate?
    var daysDelegate: GetDaysDelegate?
    var service:ScheduleServiceProtocol
    
    init(_ scheduleServ: ScheduleServiceProtocol) {
        service = scheduleServ
    }
    
    func getDays(_ params: getWithIdParams,_ date:String) {
        service.getScheduleService(date, params, { (ScheduleDates) in
            var days:[String] = []
            if ScheduleDates.dates.count > 0{
                var day = ScheduleDates.dates[0].components(separatedBy:" ").first
                days.append(day!)
                for date in ScheduleDates.dates{
                    if ((date.components(separatedBy:" ").first) != (days.last)){
                        day = date.components(separatedBy:" ").first
                        days.append(day!)
                    }
                }
            }
            self.daysDelegate?.didGetDaysSuccess(days)
        }) { (errorMessage) in
            self.daysDelegate?.didGetDaysError(errorMessage)
        }
    }
    
    func getHours(_ params: getWithIdParams,_ day:String){
        var hours:[String] = []
        var formattedHours:[String] = []
        var date:String
        date = (day.components(separatedBy:"-")[1])+"-"+(day.components(separatedBy:"-")[0])
        
        service.getScheduleService(date, params, { (ScheduleDates) in
            if ScheduleDates.dates.count > 0{
                for date in ScheduleDates.dates{
                    if ((date.components(separatedBy:" ").first) == (day)){
                        let hour = date.components(separatedBy:" ").last
                        let formattedHour = (hour?.components(separatedBy:":")[0])! + ":" + (hour?.components(separatedBy:":")[1])!
                        hours.append(hour!)
                        formattedHours.append(formattedHour)
                    }
                }
            }
            self.hoursDelegate?.didGetHoursSuccess(hours,formattedHours)
        }) { (errorMessage) in
            self.hoursDelegate?.didGetHoursError(errorMessage)
        }
    }
}
