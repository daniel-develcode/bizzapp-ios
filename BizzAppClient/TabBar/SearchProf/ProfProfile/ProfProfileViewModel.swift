import Foundation

struct profProfileData{
    var profName:String
    var profGrade:String
    var profCode:String
    var token:String
}

protocol ProfProfileViewModelDelegate{
    
    func didGetWithSuccess(_ profile:GetProfProfile, _ data:profProfileData,_ subCats:[ProfServParam])
    
    func didGetWithError(_ errorMessage:String)
}

protocol ProfProfileViewModelProtocol{
    
    func getProfile(_ profCode: Int,_ subCategoryID:Int)
    
    var delegate:ProfProfileViewModelDelegate?{get set}
}

class ProfProfileViewModel:ProfProfileViewModelProtocol{
    
    var delegate: ProfProfileViewModelDelegate?
    var service:ProfProfileServiceProtocol
    
    init(_ profProfileServ: ProfProfileServiceProtocol) {
        service = profProfileServ
    }
    
    func getProfile(_ profCode: Int,_ subCategoryID:Int) {
        
        guard let validToken = UserDefaults.standard.object(forKey:"userToken") else {delegate?.didGetWithError("erro ao recuperar token.");return}
        
        
        let params = getWithIdParams(
            token:validToken as! String,
            userCode:String(profCode)
        )
        
        service.getProf(params, { (data) in
            let subcats = data.profServParams ?? []
            var outputSubCats:[ProfServParam] = []
            
            for subcat in subcats {
                if (subcat.codeSubCat == subCategoryID){
                    outputSubCats.append(subcat)
                }
            }
            let profData = profProfileData(
                profName:data.profName!,
                profGrade:String(data.profGrade ?? 0),
                profCode: String(data.profId!),
                token:params.token)
            
            self.delegate?.didGetWithSuccess(data,profData,outputSubCats)
        }) { (error) in
            self.delegate?.didGetWithError(error)
        }
    }
}
