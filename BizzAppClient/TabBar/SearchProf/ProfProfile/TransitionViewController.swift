import UIKit

class TransitionViewController: UIViewController {
    
    @IBAction func presentSolicitationsBtn(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabBarVC: TabBarViewController = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        tabBarVC.selectedIndex = 1
        self.present(tabBarVC, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
