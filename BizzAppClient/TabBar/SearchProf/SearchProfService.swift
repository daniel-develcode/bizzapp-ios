import Foundation
import Alamofire
import UIKit

struct FindByIdParam{
    var token:String
    var long: Double
    var lat: Double
    var categories: [Int]
    var date:String
    var page:Int
}

protocol SearchProfServiceProtocol{
    
    typealias OnGetProfSuccess = ((GetProfessional) -> Void)
    typealias OnGetProfError = ((String) -> Void)
    
    func getProfService(_ param:FindByIdParam,
                        _ success:@escaping OnGetProfSuccess,
                        _ error:@escaping OnGetProfError)
}

class SearchProfService:SearchProfServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol) {
        self.service = service
    }
    
    func getProfService(_ param:FindByIdParam,
                        _ successS: @escaping OnGetProfSuccess,
                        _ errorS: @escaping OnGetProfError) {
        
       let catCod = UserDefaults.standard.object(forKey:"categoryCod") as! Int
       let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as! Int
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"professional/find"
        print(param.date)
        let date = getFormattedDate(param.date.components(separatedBy: " ")[0])
        let hour = getFormattedHour(param.date.components(separatedBy: " ")[1])
        let day = date+"T"+hour
        let parameters:Parameters = [
            "numLatitude":param.lat,
            "numLongitude":param.long,
            "codCategoria":catCod,
            "codSubcategoria":subcatCode,
            "dataConsulta":day,
            "page":param.page
            ] as [String : Any]
        
        let header:HTTPHeaders = [
            "Authorization":param.token,
            "country":"BR",
            "language":"pt",
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
            ] as [String : String]
        
        service.request(url, .post, parameters, header, { (data) in
            successS(Decoders.decodeData(data!))
        }) { (error) in
            errorS(error)
        }
    }
    //2019-5-1
    private func getFormattedDate(_ date:String) -> String{
        let day = Int(date.components(separatedBy: "-")[2])!
        let month = Int(date.components(separatedBy: "-")[1])
        let year = date.components(separatedBy: "-")[0]
        return String(format:"%02d", day)+"-"+String(format: "%02d",month!)+"-"+year
    }
    
    private func getFormattedHour(_ hour:String) -> String{
        let hora = Int(hour.components(separatedBy: ":")[0])!
        let minute = Int(hour.components(separatedBy: ":")[1])!
        let sec = Int(hour.components(separatedBy: ":")[2])!
        return String(format:"%02d", hora)+":"+String(format: "%02d",minute)+":"+String(format: "%02d",sec)
    }
}
