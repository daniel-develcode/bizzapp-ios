import Foundation
import CoreLocation
import UIKit

struct ProfCellData{
    var name:String
    var distance:String
    var grade:String
    var profCode:Int
    var indDisp:Int
    var profCat:String
    var subCatID:Int
    var profImageURL:String
}

protocol SearchProfViewModelDelegate{
    
    func didSearchWithSuccess(_ professionals:[ProfCellData],_ PageCount:Int)
    
    func didSearchWithError(_ error:String)
}

protocol SearchProfViewModelProtocol {
    
    var delegate:SearchProfViewModelDelegate?{get set}
    
    func getProf(_ page:Int)
}

class SearchProfViewModel:SearchProfViewModelProtocol{
    
    var delegate:SearchProfViewModelDelegate?
    var service:SearchProfServiceProtocol
    
    init(_ SearchProfService: SearchProfServiceProtocol) {
        service = SearchProfService
    }
    
    func getProf(_ page:Int) {
        
        guard let param = getParam(page) else {delegate?.didSearchWithError("erro ao recuperar os dados");return}
        service.getProfService(param, { (profs) in
            self.delegate?.didSearchWithSuccess(self.setupList(profs.lsProfessional),profs.pageCount)
        }) { (error) in
            self.delegate?.didSearchWithError(error)
        }
    }
    
    private func setupList(_ profs:[Professional]) -> [ProfCellData]{
        var profCellList:[ProfCellData] = []
        let lati = UserDefaults.standard.object(forKey: "SolLatitude") as? Double
        let longi = UserDefaults.standard.object(forKey: "SolLongitude") as? Double
        let subcatCode = UserDefaults.standard.object(forKey:"subCategoryCod") as! Int
        let catCode = UserDefaults.standard.object(forKey:"categoryCod") as! Int
        for prof in profs{
            for cats in prof.subCategories{
                if((cats.subcategoryId == subcatCode)&&(cats.id == catCode)){
                let profCord = CLLocation(latitude: CLLocationDegrees(prof.latitude!), longitude: CLLocationDegrees(prof.longitude!))
                let userLoc = CLLocation(latitude: CLLocationDegrees( lati ?? 37.332050), longitude: CLLocationDegrees(longi ?? -122.031201))
                let profKm = (String(format:"%.2f",(userLoc.distance(from: profCord)/1000))+" km")
                let cell = ProfCellData(
                    name:prof.name,
                    distance:(profKm),
                    grade:String(prof.nota ?? 0),
                    profCode:prof.id,
                    indDisp:prof.availableNow,
                    profCat:cats.description,
                    subCatID:cats.subcategoryId,
                    profImageURL: prof.image ?? "")
                    profCellList.append(cell)
                }
            }
        }
        return profCellList
    }
    
    private func getParam(_ page:Int)-> FindByIdParam?{
        
        guard let validToken = UserDefaults.standard.object(forKey: "userToken")  else {return nil}
        let validLong = UserDefaults.standard.object(forKey: "SolLongitude") as? Double
        let validLat = UserDefaults.standard.object(forKey: "SolLatitude") as? Double
        let Catcod = UserDefaults.standard.object(forKey:"categoryCod") as! Int
        var ValidCategories:[Int] = []
        ValidCategories.append(Catcod)
        
        let param = FindByIdParam(
            token:validToken as! String,
            long:validLong ?? 10,
            lat:validLat ?? 10,
            categories:ValidCategories,
            date:self.date(),
            page:page
        )
        return param
    }
    
    fileprivate func date () ->String{
        let date:String
        let currentDateTime = Date()
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        date = (String(dateTimeComponents.year!)+"-"+String(dateTimeComponents.month!)+"-"+String(dateTimeComponents.day!)+" "+String(dateTimeComponents.hour!)+":"+String(dateTimeComponents.minute!)+":"+String(dateTimeComponents.second!))
        return date
    }
}
