import Foundation
import Firebase

protocol ChangePasswordServiceProtocol{
    typealias OnChangePasswordSuccess = ((String) -> Void)
    typealias OnChangePasswordError = ((String) -> Void)
    
    func changePasswordService(_ newPassword:String,
                               _ success:@escaping OnChangePasswordSuccess,
                               _ error:@escaping OnChangePasswordError)
}

class ChangePasswordService:ChangePasswordServiceProtocol{
    func changePasswordService(_ newPassword:String,
                               _ success: @escaping OnChangePasswordSuccess,
                               _ error: @escaping OnChangePasswordError) {
        
        Auth.auth().currentUser?.updatePassword(to: newPassword) { changeError in
            if (changeError == nil){
                success("Senha modificada com sucesso")
            }else{
                error((changeError?.localizedDescription)!)
            }
        }
    }
}
