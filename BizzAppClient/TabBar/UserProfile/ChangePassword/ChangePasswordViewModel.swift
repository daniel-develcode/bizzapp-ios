import Foundation
import Firebase
import FirebaseAuth

struct ChangePasswordTxt {
    var currentPassword:String?
    var newPassword:String?
    var conNewPassword:String?
}

protocol ChangePasswordViewModelDelegate {
    
    func didChangePasswordSuccess(_ success:String)
    
    func didChangePasswordError(_ error:[String])
    
}

protocol ChangePasswordViewModelProtocol {
    
    var delegate:ChangePasswordViewModelDelegate?{get set}
    
    func changePassword(_ changeParams:ChangePasswordTxt)
}

class ChangePasswordViewModel:ChangePasswordViewModelProtocol{
    var delegate: ChangePasswordViewModelDelegate?
    var service:ChangePasswordServiceProtocol
    
    init(_ userProfileService: ChangePasswordServiceProtocol) {
        service = userProfileService
    }
    
    func changePassword(_ changeParams: ChangePasswordTxt) {
        
        var errorBundle:[String] = []
        let user = Auth.auth().currentUser
        let credential = EmailAuthProvider.credential(withEmail: (user?.email)!, password: (changeParams.currentPassword)!)
        
        user?.reauthenticateAndRetrieveData(with: credential, completion: {(authResult, error) in
            if (error != nil) {
                
                if (changeParams.newPassword != changeParams.conNewPassword){
                    errorBundle.append("As senhas são diferentes.")
                }
                if (changeParams.currentPassword == ""){
                    errorBundle.append("Senha atual requerida")
                }else {
                  errorBundle.append((error?.localizedDescription)!)
                }
                if (changeParams.newPassword == ""){
                    errorBundle.append("É preciso preencher o campo de nova senha")
                }
                if (changeParams.conNewPassword == ""){
                    errorBundle.append("É preciso preencher o campo de confirmar nova senha")
                }
                if ((changeParams.newPassword?.count)! < 6){
                    errorBundle.append("A senha deve ter no mínimo 6 digitos")
                }
                self.delegate?.didChangePasswordError(errorBundle)
                
            }else{
                if (changeParams.newPassword != changeParams.conNewPassword){
                    errorBundle.append("As senhas são diferentes.")
                }
                if (changeParams.currentPassword == ""){
                    errorBundle.append("Senha atual requerida")
                }
                if (changeParams.newPassword == ""){
                    errorBundle.append("É preciso preencher o campo de nova senha")
                }
                if (changeParams.conNewPassword == ""){
                    errorBundle.append("É preciso preencher o campo de confirmar nova senha")
                }
                if ((changeParams.newPassword?.count)! < 6){
                    errorBundle.append("A senha deve ter no mínimo 6 digitos")
                }
                if (errorBundle.count > 0){
                    self.delegate?.didChangePasswordError(errorBundle)
                }else{
                self.service.changePasswordService(changeParams.newPassword!, { (success) in
                    self.delegate?.didChangePasswordSuccess(success)
                }, { (error) in
                    errorBundle.append(error)
                    self.delegate?.didChangePasswordError(errorBundle)
                })}
            }
        })
    }
}

