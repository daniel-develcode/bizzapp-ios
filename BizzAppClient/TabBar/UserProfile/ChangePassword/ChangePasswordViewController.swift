import UIKit

class ChangePasswordViewController: UIViewController,ChangePasswordViewModelDelegate {
    
    var viewModel:ChangePasswordViewModelProtocol!
    var superView:UIViewController!
    @IBOutlet weak var currentPasswordTxt: UITextField!
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var conNewPasswordTxt: UITextField!
    @IBOutlet weak var currentPasswordError: UILabel!
    @IBOutlet weak var newPasswordError: UILabel!
    @IBOutlet weak var conNewPasswordError: UILabel!
    var securePassBtn = UIButton(type: .custom)
    var secureNewPassBtn = UIButton(type: .custom)
    var secureConPass = UIButton(type: .custom)
    
    @IBAction func returnBtn(_ sender: Any) {
        if((currentPasswordTxt.text == "")&&(newPasswordTxt.text == "")&&(conNewPasswordTxt.text == "")){
            self.dismiss(animated: true, completion: nil)
        }else{
            let params = ChangePasswordTxt (
                currentPassword:currentPasswordTxt.text,
                newPassword:newPasswordTxt.text,
                conNewPassword:conNewPasswordTxt.text
            )
            viewModel.changePassword(params)
        }
    }
    
    func didChangePasswordSuccess(_ success: String) {
        print(success)
        self.dismiss(animated: true, completion: nil)
        superView.alertMessage(titleAlert: "Sucesso", messageAlert: "Senha atualizada.", buttonAlert:"OK")
    }
    
    func didChangePasswordError(_ error: [String]) {
        
        conNewPasswordError.isHidden = true
        newPasswordError.isHidden = true
        currentPasswordError.isHidden = true
        conNewPasswordTxt.layer.borderColor = UIColor.lightGray.cgColor
        newPasswordTxt.layer.borderColor = UIColor.lightGray.cgColor
        currentPasswordTxt.layer.borderColor = UIColor.lightGray.cgColor
        
        for err in error{
            if(err == "Senha atual requerida"){
                currentPasswordTxt.layer.borderColor = UIColor.red.cgColor
                currentPasswordError.text = "Senha requerida."
                currentPasswordError.isHidden = false}
            else if(err == "The password is invalid or the user does not have a password."){
                currentPasswordTxt.layer.borderColor = UIColor.red.cgColor
                currentPasswordError.text = "Falha ao atualizar senha."
                currentPasswordError.isHidden = false
            }
            
            if(err == "É preciso preencher o campo de nova senha"){
                newPasswordTxt.layer.borderColor = UIColor.red.cgColor
                newPasswordError.text = "Senha requerida."
                newPasswordError.isHidden = false
            }
            
            if(err == "É preciso preencher o campo de confirmar nova senha"){
                conNewPasswordTxt.layer.borderColor = UIColor.red.cgColor
                conNewPasswordError.text = "Senha requerida."
                conNewPasswordError.isHidden = false
            }else if(err == "A senha deve ter no mínimo 6 digitos"){
                conNewPasswordTxt.layer.borderColor = UIColor.red.cgColor
                conNewPasswordError.text = "A senha deve ter no mínimo 6 caracteres."
                conNewPasswordError.isHidden = false
                newPasswordTxt.layer.borderColor = UIColor.red.cgColor
                newPasswordError.text = "A senha deve ter no mínimo 6 caracteres."
                newPasswordError.isHidden = false
            }else if(err == "As senhas são diferentes."){
                conNewPasswordTxt.layer.borderColor = UIColor.red.cgColor
                conNewPasswordError.text = "As senhas devem ser iguais."
                conNewPasswordError.isHidden = false
                newPasswordTxt.layer.borderColor = UIColor.red.cgColor
                newPasswordError.text = "As senhas devem ser iguais."
                newPasswordError.isHidden = false
            }
        }
    }
    
    @IBAction func currentPasswordTxtRefresh(_ sender: Any) {
        if (currentPasswordTxt.isSecureTextEntry ==  true){
            currentPasswordTxt.isSecureTextEntry =  false
            securePassBtn.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            currentPasswordTxt.isSecureTextEntry =  true
            securePassBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    @IBAction func newPasswordTxtRefresh(_ sender: Any) {
        if (newPasswordTxt.isSecureTextEntry ==  true){
            newPasswordTxt.isSecureTextEntry =  false
            secureNewPassBtn.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            newPasswordTxt.isSecureTextEntry =  true
            secureNewPassBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    @IBAction func conNewPasswordTxtRefresh(_ sender: Any) {
        if (conNewPasswordTxt.isSecureTextEntry ==  true){
            conNewPasswordTxt.isSecureTextEntry =  false
            secureConPass.setImage(#imageLiteral(resourceName: "baseline-remove_red_eye-24px"), for: .normal)
        }else{
            conNewPasswordTxt.isSecureTextEntry =  true
            secureConPass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        }
    }
    
    private func configSecurePassword(_ txt:UITextField){
        securePassBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        securePassBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        securePassBtn.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        securePassBtn.addTarget(self, action: #selector(self.currentPasswordTxtRefresh), for: .touchUpInside)
        txt.rightView = securePassBtn
        txt.rightViewMode = .always
    }
    
    private func configSecureNewPassword(_ txt:UITextField){
        secureNewPassBtn.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        secureNewPassBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        secureNewPassBtn.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        secureNewPassBtn.addTarget(self, action: #selector(self.newPasswordTxtRefresh), for: .touchUpInside)
        txt.rightView = secureNewPassBtn
        txt.rightViewMode = .always
    }
    
    private func configSecureConPassword(_ txt:UITextField){
        secureConPass.setImage(#imageLiteral(resourceName: "baseline-visibility_off-24px"), for: .normal)
        secureConPass.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        secureConPass.frame = CGRect(x: CGFloat(txt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        secureConPass.addTarget(self, action: #selector(self.conNewPasswordTxtRefresh), for: .touchUpInside)
        txt.rightView = secureConPass
        txt.rightViewMode = .always
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = ChangePasswordViewModel(AppDelegate.container.resolve(ChangePasswordServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        configSecurePassword(currentPasswordTxt)
        configSecureNewPassword(newPasswordTxt)
        configSecureConPassword(conNewPasswordTxt)
    }
}
