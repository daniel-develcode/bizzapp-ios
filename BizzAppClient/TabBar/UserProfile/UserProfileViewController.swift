import UIKit
import Firebase
import Foundation
import FirebaseStorage
import FirebaseAuth
import Alamofire

class UserProfileViewController: UIViewController,UserProfileViewModelDelegate,ImageReceiverDelegate {
 
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var biLbl: UILabel!
    @IBOutlet weak var gradeLbl: UILabel!
    @IBOutlet weak var adressLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var changePasswordLbl: UIButton!
    @IBOutlet weak var userImage: UIButton!
    @IBOutlet weak var exitLbl: UIButton!
    var importedImage: UIImage?
    var spinner:UIView!
    var viewModel:UserProfileViewModelProtocol!
    var user:UserData!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBAction func didPressChangeNameBiBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "UpdateProfile", bundle: nil)
        let updateNameBi: UpdateNameBiViewController = storyboard.instantiateViewController(withIdentifier: "UpdateNameBiViewController") as! UpdateNameBiViewController
        updateNameBi.user = user
        self.present(updateNameBi, animated: true, completion: nil)
    }
    
    @IBAction func didPressChangeAdressBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "UpdateProfile", bundle: nil)
        let updateAdress: UpdateAdressViewController = storyboard.instantiateViewController(withIdentifier: "UpdateAdressViewController") as! UpdateAdressViewController
        updateAdress.user = user
        let navigationController = UINavigationController(rootViewController: updateAdress)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func didPressChangePhoneBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "UpdateProfile", bundle: nil)
        let updatePhone: UpdatePhoneViewController = storyboard.instantiateViewController(withIdentifier: "UpdatePhoneViewController") as! UpdatePhoneViewController
        updatePhone.user = user
        self.present(updatePhone, animated: true, completion: nil)
    }
    
    @IBAction func didPressChangePasswordBtn(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "ChangePassword", bundle: nil)
        let vc: ChangePasswordViewController = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        vc.superView = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func didPressChangePhotoBtn(_ sender: Any) {
        if(user.userCode != nil){
            if(progressView.isHidden == true){
                progressView.progress = 0
                let vc = UIStoryboard(name: "ImagePicker", bundle: nil).instantiateViewController(withIdentifier: "ImagePickerViewController") as! ImagePickerViewController
                vc.imageReceiverDelegate = self
                vc.userCode = user.userCode
                vc.view.frame = self.view.bounds
                self.addChild(vc)
                UIView.transition(with: self.view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve,
                                  animations: {self.view.addSubview(vc.view)}, completion: nil)
            }else{
                alertMessage(titleAlert: "Erro", messageAlert: "Aguarde, carregando foto...", buttonAlert: "OK!")
            }
        }else{
            self.viewModel.getProfile()
            self.alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado, tente novamente.", buttonAlert: "OK")
        }
    }
    
    func imageUploadProgress(_ progress: Double, _ imageReference: String) {
        print("Progresso do download \(progress)")
        progressView.isHidden = false
        progressView.progress = Float(progress)
    }
    
     func imageUploadSucess(_ downloadURL: String, _ imageReference: String) {
        print("Url para download: \(downloadURL)")
        viewModel.updateProfileImage(downloadURL)
    }
    
    func imageUploadError(_ errorMessage: String) {
        print((errorMessage))
        progressView.isHidden = true
        alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado.", buttonAlert: "OK!")
    }
    
    func didUpdateImageSuccess(_ successMessage: String) {
        progressView.isHidden = true
        alertMessage(titleAlert: "Sucesso", messageAlert: "A foto de perfil foi alterada com sucesso", buttonAlert: "OK!")
    }
    
    func didUpdateImageError(_ errorMessage: String) {
        progressView.isHidden = true
        alertMessage(titleAlert: "Erro", messageAlert: "Ocorreu um erro inesperado.", buttonAlert: "OK!")
    }
    
    @IBAction func exitBtn(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        UserDefaults.standard.removeObject(forKey: "userLatitude")
        UserDefaults.standard.removeObject(forKey: "userLongitude")
        UserDefaults.standard.removeObject(forKey: "userToken")
        UserDefaults.standard.removeObject(forKey:"userCode")
        UserDefaults.standard.removeObject(forKey: "userUid")
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func didGetUserProfileWithSuccess(_ userD: UserData) {
        user = userD
        nameLbl.text = userD.name
        emailLbl.text = userD.email
        biLbl.text = userD.bi ?? ""
        gradeLbl.text = String(userD.grade!) + "/5"
        adressLbl.text = userD.adress
        phoneLbl.text = userD.phone
        if (importedImage != nil){
            userImage.setImage(importedImage, for: .normal)
        }else {
            let application = UIApplication.shared.delegate as! AppDelegate
            let downloader = application.getImageDownloader()
            downloader.downloadImage(user.image ?? "", { (image,url) in
                UIView.transition(with: self.userImage, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.userImage.setImage(image, for: .normal)
                }, completion: nil)
            }) { (error) in
                print(error)
            }
        }
        UIViewController.removeSpinner(spinner)
    }
    
    func didGetUserProfileWithError(_ error: String) {
        print(error)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner = UIViewController.displaySpinner(onView:self.view)
        viewModel.getProfile()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = UserProfileViewModel(AppDelegate.container.resolve(UserProfileServiceProtocol.self)!, AppDelegate.container.resolve(UpdateProfileServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.delegate = self
        changePasswordLbl.layer.borderWidth = 1
        changePasswordLbl.layer.borderColor = UIColor.gray.cgColor
        exitLbl.layer.borderWidth = 1
        exitLbl.layer.borderColor = UIColor.gray.cgColor
        version.text = self.getVersion()
    }
}
