import Foundation
import Alamofire

protocol UserProfileServiceProtocol {
    typealias OnGetProfileSuccess = ((UserData) -> Void)
    typealias OnGetProfileError = ((String) -> Void)
    
    func getProfileService(_ params:getWithUidParams,
                           _ success:@escaping OnGetProfileSuccess,
                           _ error:@escaping OnGetProfileError)
}

class UserProfileService:UserProfileServiceProtocol {
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol){
        self.service = service
    }
    
    func getProfileService(_ params: getWithUidParams,
                           _ success: @escaping OnGetProfileSuccess,
                           _ error: @escaping OnGetProfileError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"auth/find-by-id?chvUsuario="+params.uid
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":params.token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(url, .get, nil, header, { data in
            success(Decoders.decodeData(data!))
        }) { (errorMessage) in
            error(errorMessage)
        }
    }
}
