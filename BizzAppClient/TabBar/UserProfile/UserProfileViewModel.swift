import Foundation
import CoreLocation

struct userProf{
    var name:String
    var email:String
    var bi:String
    var grade:String
    var adress:String
    var phone:String
    var distance:String
}

protocol UserProfileViewModelDelegate {
    
    func didGetUserProfileWithSuccess(_ userD:UserData)
    
    func didGetUserProfileWithError(_ error:String)
    
    func didPressChangePasswordBtn(_ sender:Any)
    
    func didPressChangeNameBiBtn(_ sender: Any)
    
    func didPressChangeAdressBtn(_ sender: Any)
    
    func didPressChangePhoneBtn(_ sender: Any)
    
    func didUpdateImageSuccess(_ successMessage:String)
    
    func didUpdateImageError(_ errorMessage:String)
}

protocol UserProfileViewModelProtocol {
    var delegate:UserProfileViewModelDelegate?{get set}
    
    func getProfile()
    
    func updateProfileImage(_ imageUrl:String)
}

class UserProfileViewModel:UserProfileViewModelProtocol{
    
    var user:UserData!
    var delegate: UserProfileViewModelDelegate?
    var userProfileService:UserProfileServiceProtocol
    var updateProfileService:UpdateProfileServiceProtocol
    
    init(_ userProfileService: UserProfileServiceProtocol,_ updateProfileService:UpdateProfileServiceProtocol) {
        self.userProfileService = userProfileService
        self.updateProfileService = updateProfileService
    }
    
    func updateProfileImage(_ imageUrl:String){
        user.image = imageUrl
        updateProfileService.save(user, { (successMessage) in
            self.delegate?.didUpdateImageSuccess(successMessage)
        }) { (errorMessage) in
            self.delegate?.didUpdateImageError(errorMessage)
        }
    }
    
    func getProfile() {
        guard let param = GetParams.WithUidParams() else {delegate?.didGetUserProfileWithError("error ao recuperar os dados");return}
        userProfileService.getProfileService(param, { userD in
            self.delegate?.didGetUserProfileWithSuccess(userD)
            self.user = userD
        }) { (errorMessage) in
            self.delegate?.didGetUserProfileWithError(errorMessage)
        }
    }
}
