import Foundation
import Alamofire

protocol UpdateProfileServiceProtocol {
    
    typealias OnSaveSuccess = (String) -> Void
    typealias OnSaveError = (String) -> Void
    
    func save(_ userData:UserData,
              _ SaveSuccess:@escaping OnSaveSuccess,
              _ SaveError:@escaping OnSaveError)
}

class UpdateProfileService:UpdateProfileServiceProtocol{
    var service:RequestServiceProtocol
    init(_ requestService: RequestServiceProtocol) {
        service = requestService
    }
    
    func save(_ userData: UserData,
              _ SaveSuccess: @escaping OnSaveSuccess,
              _ SaveError: @escaping OnSaveError){
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"auth/update-user"
        
        let header = getHeader(userData)
        let param = getParam(userData)
        
        service.request(url, .post, param, header, { (Data) in
            SaveSuccess("Salvo com sucesso")
        }) { (error) in
            SaveError("Erro ao salvar")
        }
    }
    
    private func getHeader(_ user:UserData) -> HTTPHeaders{
        let header:HTTPHeaders = [
            "Authorization":(GetParams.WithIdParams()?.token)!,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        return header
    }
    
    private func getParam(_ user:UserData) -> Parameters{
        let application = UIApplication.shared.delegate as! AppDelegate
        let token = application.getNotificationToken()!
        let parameters:Parameters = [
            "chvUsuario":user.uid!,
            "desEmail":user.email!,
            "desNome":user.name!,
            "desNif":"tstdev",
            "desBi":user.bi ?? 0,
            "desFoto":user.image ?? "",
            "desTelefonePrin":user.phone!,
            "numLatitude": user.latitude!,
            "numLongitude": user.longitude!,
            "desStringEndereco":user.adress!,
            "desReferencia":user.reference ?? "",
            "indAceite":1,
            "device":[
                "numImei":"123456789",
                "numId":token,
                "tipApp":1,
                "tipSystem":2]
            ] as [String : Any]
        return parameters
    }
}
