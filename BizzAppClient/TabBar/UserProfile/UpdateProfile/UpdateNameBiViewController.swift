import UIKit

class UpdateNameBiViewController: UIViewController,UpdateNameBiViewModelDelegate{
    
    @IBOutlet weak var updateNameTxt: UITextField!
    @IBOutlet weak var updateBiTxt: UITextField!
    var viewModel:UpdateProfileViewModelProtocol!
    var user:UserData?
    @IBOutlet weak var saveBtnLbl: UIButton!
    
    func didUpdateNameBiSuccess(_ successMessage: String) {
        print(successMessage)
    }
    
    func didUpdateNameBiError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    @IBAction func didPressSaveBtm(_ sender: Any) {
        user?.name = updateNameTxt.text
        user?.bi = updateBiTxt.text
        viewModel.updateNameBi(user!)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = UpdateProfileViewModel(AppDelegate.container.resolve(UpdateProfileServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.updateNameBiDelegate = self
        updateNameTxt.text = user?.name
        updateBiTxt.text = user?.bi
    }
}
