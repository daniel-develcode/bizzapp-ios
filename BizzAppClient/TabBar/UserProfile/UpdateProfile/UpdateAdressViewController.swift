import UIKit
import GooglePlaces
import GoogleMaps

class UpdateAdressViewController: UIViewController,UpdateAdressViewModelDelegate,UISearchBarDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,
GMSAutocompleteViewControllerDelegate {
    
    @IBOutlet weak var mapContainer: GMSMapView!
    @IBOutlet weak var updateReferenceTxt: UITextField!
    var locationManager = CLLocationManager()
    var viewModel:UpdateProfileViewModelProtocol!
    var user:UserData?
    var adress:String = ""
    var lati:Double = 0
    var long:Double = 0
    @IBOutlet weak var adressLbl: UINavigationItem!
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.locationManager.startUpdatingLocation()
        self.present(autocompleteController, animated:true, completion:nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16.0)
        mapContainer.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        marker.title = place.name
        marker.map = mapContainer
        marker.icon = self.getMarkerImage()
        adress = place.formattedAddress!
        adressLbl.title = adress
        lati = place.coordinate.latitude
        long = place.coordinate.longitude
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("erro ao conseguir a localização\(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didUpdateAdressSuccess(_ successMessage: String) {
        print(successMessage)
    }
    
    func didUpdateAdressError(_ errorMessage: String) {
        print(errorMessage)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapContainer.isMyLocationEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (user!.adress == ""){
            let location = locations.last
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 16.0)
            self.mapContainer.animate(to: camera)
        }
    }
    
    func iniGoogleMaps(){
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 16)
        mapContainer.camera = camera
        self.mapContainer.delegate = self
        self.mapContainer.isMyLocationEnabled = true
        self.mapContainer.settings.myLocationButton = true
        if (adress != "") {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long )
            marker.title = adress
            marker.map = mapContainer
            marker.icon = self.getMarkerImage()
        }
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    @IBAction func didPressSaveBtm(_ sender: Any) {
        user!.adress = adress
        user!.reference = updateReferenceTxt.text
        user!.latitude = lati
        user!.longitude = long
        viewModel.updateAdress(user)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adress = user?.adress ?? ""
        updateReferenceTxt.text = user?.reference ?? ""
        lati = Double(user?.latitude ?? 0)
        long = Double(user?.longitude ?? 0)
        adressLbl.title = adress
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 255/255, green: 195/255, blue: 25/255, alpha: 1)
        let viewModel = UpdateProfileViewModel(AppDelegate.container.resolve(UpdateProfileServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.updateAdressDelegate = self
        updateReferenceTxt.text = user?.reference
        iniGoogleMaps()
    }
}
