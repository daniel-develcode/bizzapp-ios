import Foundation

protocol UpdatePhoneViewModelDelegate{
    func didUpdatePhoneSuccess(_ successMessage:String)
    func didUpdatePhoneError(_ errorMessage:String)
    func didPressSaveBtm(_ sender:Any)
}

protocol UpdateAdressViewModelDelegate {
    func didUpdateAdressSuccess(_ successMessage:String)
    func didUpdateAdressError(_ errorMessage:String)
    func didPressSaveBtm(_ sender:Any)
}

protocol UpdateNameBiViewModelDelegate {
    func didUpdateNameBiSuccess(_ successMessage:String)
    func didUpdateNameBiError(_ errorMessage:String)
    func didPressSaveBtm(_ sender:Any)
}

protocol UpdateProfileViewModelProtocol {
    
    var updatePhoneDelegate:UpdatePhoneViewModelDelegate?{get set}
    var updateAdressDelegate:UpdateAdressViewModelDelegate?{get set}
    var updateNameBiDelegate:UpdateNameBiViewModelDelegate?{get set}
    
    func updatePhone(_ userData:UserData?)
    
    func updateAdress(_ userData:UserData?)
    
    func updateNameBi(_ userData:UserData?)
}

class UpdateProfileViewModel:UpdateProfileViewModelProtocol{
    
    var updatePhoneDelegate: UpdatePhoneViewModelDelegate?
    var updateAdressDelegate: UpdateAdressViewModelDelegate?
    var updateNameBiDelegate: UpdateNameBiViewModelDelegate?
    var service:UpdateProfileServiceProtocol
    
    init(_ service:UpdateProfileServiceProtocol){
        self.service = service
    }
    
    func updatePhone(_ userData: UserData?) {
        guard let validUserData = validateInput(invalidData: userData) else {updatePhoneDelegate?.didUpdatePhoneError("erro ao validar os dados");return}
        if (validUserData.phone!.count >= 12){
            service.save(validUserData, { (saveSuccess) in
                self.updatePhoneDelegate?.didUpdatePhoneSuccess(saveSuccess)
            }) { (saveError) in
                self.updatePhoneDelegate?.didUpdatePhoneError(saveError)
            }
        }else{
            updatePhoneDelegate?.didUpdatePhoneError("Número inválido")
        }
    }
    
    func updateAdress(_ userData: UserData?) {
        guard let validUserData = validateInput(invalidData: userData) else {updateAdressDelegate?.didUpdateAdressError("erro ao validar os dados");return}
        service.save(validUserData, { (saveSuccess) in
            self.updateAdressDelegate?.didUpdateAdressSuccess(saveSuccess)
        }) { (saveError) in
            self.updateAdressDelegate?.didUpdateAdressError(saveError)
        }
    }
    
    func updateNameBi(_ userData: UserData?) {
        guard let validUserData = validateInput(invalidData: userData) else {updateNameBiDelegate?.didUpdateNameBiError("erro ao validar os dados");return}
        service.save(validUserData, { (saveSuccess) in
            self.updateNameBiDelegate?.didUpdateNameBiSuccess(saveSuccess)
        }) { (saveError) in
            self.updateNameBiDelegate?.didUpdateNameBiError(saveError)
        }
    }
    
    fileprivate func validateInput(invalidData: UserData?)-> UserData?{
        
        guard let validName = invalidData!.name else {return nil}
        guard let validEmail = invalidData!.email else {return nil}
        let validAdress = invalidData?.adress ?? ""
        let validPhone = invalidData?.phone ?? ""
        let validLat = invalidData?.latitude ?? 0
        let validLong =  invalidData?.longitude ?? 0
        let validReference = invalidData?.reference ?? ""
        let validBi = invalidData?.bi ?? ""
        
        return UserData(
            userCode:invalidData!.userCode,
            uid:invalidData!.uid,
            email:validEmail,
            name:validName,
            reference:validReference,
            bi:validBi,
            image:invalidData!.image,
            adress:validAdress,
            phone:validPhone,
            termsAccept: invalidData!.termsAccept,
            inactive:invalidData!.inactive,
            token:invalidData!.token,
            grade:invalidData!.grade,
            latitude:validLat,
            longitude:validLong
        )
    }
}
