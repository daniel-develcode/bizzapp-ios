import UIKit

class UpdatePhoneViewController: UIViewController,UpdatePhoneViewModelDelegate {
    
    @IBOutlet weak var updatePhoneTxt: UITextField!
    var viewModel:UpdateProfileViewModelProtocol!
    var user:UserData?
    @IBOutlet weak var phoneError: UILabel!
    
    func didUpdatePhoneSuccess(_ successMessage: String) {
        print(successMessage)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didUpdatePhoneError(_ errorMessage: String) {
        print(errorMessage)
        if (errorMessage == "Número inválido"){
            updatePhoneTxt.layer.borderColor = UIColor.red.cgColor
            phoneError.text = errorMessage
            phoneError.isHidden = false
        }
    }
    
    @IBAction func didPressSaveBtm(_ sender: Any) {
        user!.phone = updatePhoneTxt.text
        viewModel.updatePhone(user)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = UpdateProfileViewModel(AppDelegate.container.resolve(UpdateProfileServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.updatePhoneDelegate = self
        updatePhoneTxt.text = user?.phone
    }
}
