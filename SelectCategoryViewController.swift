import UIKit

class SelectCategoryViewController: UIViewController,UITableViewDelegate,SelectCategoryViewModelDelegate,UISearchBarDelegate {
    
    var categories:[GetCategory] = []
    var viewModel:SelectCategoryViewModelProtocol!
    var selectCatDS:SelectCategoryDataSource!
    @IBOutlet weak var table: UITableView!
    
    func didGetCategoriesSuccess(_ success: [GetCategory]) {
        categories = success
        selectCatDS.reloadData(categories)
        table.reloadData()
    }
    
    func didGetCategoriesError(_ error: String) {
        print(error)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "SelectCategory", bundle: nil)
        let vc: SelectSubcategoryViewController = storyboard.instantiateViewController(withIdentifier: "SelectSubcategoryViewController") as! SelectSubcategoryViewController
        vc.catCode = categories[indexPath.row].catCode
        vc.catName = categories[indexPath.row].desCat
        vc.categoryImage = categories[indexPath.row].imageCat
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = SelectCategoryViewModel(AppDelegate.container.resolve(SelectCategoryServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.catDelegate = self
        viewModel.getCategory()
        selectCatDS = SelectCategoryDataSource(categories)
        table.dataSource = selectCatDS
        table.isEmptyRowsHidden = true
    }
}

class CategoryCell:UITableViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
}
