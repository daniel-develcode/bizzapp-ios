import Foundation
import Alamofire

protocol SelectCategoryServiceProtocol {
    
    typealias OnGetCategoriesSuccess = (([GetCategory]) -> Void)
    typealias OnGetCategoriesError = ((String) -> Void)
    
    typealias OnGetSubCategoriesSuccess = ((GetSubCategory) -> Void)
    typealias OnGetSubCategoriesError = ((String) -> Void)
    
    typealias saveCategory = ((String) -> Void)
 
    
    func getCatServ(_ token:String,
                    _ sucess:@escaping OnGetCategoriesSuccess,
                    _ error:@escaping OnGetCategoriesError)
    
    func getSubCatServ(_ catCod:Int,_ token:String,
                   _ sucess:@escaping OnGetSubCategoriesSuccess,
                   _ error:@escaping OnGetSubCategoriesError)
    
    func saveCategory(_ categoryCod:Int ,_ subCategoryCod:Int,
                      _ categoryName:String ,_ subCategoryName:String,
                      _ categoryImage:String,
                      _ success:@escaping saveCategory)
}

class SelectCategoryService:SelectCategoryServiceProtocol{
    
    var service:RequestServiceProtocol
    
    init(_ service:RequestServiceProtocol) {
        self.service = service
    }
    
    func getCatServ(_ token: String,
                    _ sucess: @escaping OnGetCategoriesSuccess,
                    _ error: @escaping OnGetCategoriesError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"category/public/list"
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        service.request(url, .get, nil, header, { (categories) in
            sucess(Decoders.decodeDataArray(categories!))
        }) { (err) in
            error(err)
        }
    }
    
    func getSubCatServ(_ catCod: Int, _ token: String,
                   _ sucess: @escaping OnGetSubCategoriesSuccess,
                   _ error: @escaping OnGetSubCategoriesError) {
        
        let bundle = Bundle.main.infoDictionary
        let Api_Url = bundle!["API_URL"] as! String? ?? ""
        let url = Api_Url+"subcategory/find"
        
        let header:HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":token,
            "Accept-Encoding":"gzip",
            "User-Agent":"gzip"
        ]
        
        let parameters:Parameters = [
            "page":"",
            "codCategoria":catCod
            ] as [String : Any]
        
        service.request(url, .post, parameters, header, { (subCats) in
            sucess(Decoders.decodeData(subCats!))
        }) { (err) in
            error(err)
        }
    }
    
    func saveCategory(_ categoryCod:Int ,_ subCategoryCod:Int,
                      _ categoryName:String ,_ subCategoryName:String,
                      _ categoryImage:String,
                      _ success:@escaping saveCategory) {
        UserDefaults.standard.set(categoryCod, forKey: "categoryCod")
        UserDefaults.standard.set(subCategoryCod, forKey: "subCategoryCod")
        UserDefaults.standard.set(categoryName, forKey: "categoryName")
        UserDefaults.standard.set(subCategoryName, forKey: "subCategoryName")
        UserDefaults.standard.set(categoryImage, forKey: "categoryImage")
        success("salvo com sucesso")
    }
}
