import UIKit
import Foundation

class SelectSubcategoryViewController: UIViewController,UITableViewDelegate,SelectSubCategoryViewModelDelegate,UISearchBarDelegate {
    
    var catCode:Int?
    var catName:String?
    var subcatCode:Int?
    var subCatName:String?
    var categoryImage:String?
    var viewModel:SelectCategoryViewModelProtocol!
    var subCategories:[SubCategories] = []
    var currentSubcategories:[SubCategories] = []
    var subCategoryDS:SelectSubcategoryDataSource!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backBtnLbl: UILabel!
    
    func didSaveCategory(_ successMessage: String) {
        print(successMessage)
        let storyboard: UIStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabBarVC: TabBarViewController = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.present(tabBarVC, animated: true, completion: nil)
    }
    
    func didGetSubCategoriesSuccess(_ success: GetSubCategory) {
        subCategories = success.cats
        currentSubcategories = success.cats
        subCategoryDS.reloadData(subCategories, 0, false)
        table.reloadData()
    }
    
    func didGetSubCategoriesError(_ error: String) {
        print(error)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentSubcategories = subCategories
            subCategoryDS.reloadData(currentSubcategories, 0, false)
            table.reloadData()
            return
        }
        currentSubcategories = subCategories.filter({ subcategory -> Bool in
            return subcategory.desCat.lowercased().contains(searchText.lowercased())
        })
        subCategoryDS.reloadData(currentSubcategories, 0, false)
        table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        subCategoryDS.reloadData(currentSubcategories, indexPath.row, true)
        subcatCode = currentSubcategories[indexPath.row].codSubCat
        subCatName = currentSubcategories[indexPath.row].desCat
        table.reloadData()
        viewModel.saveCategory(catCode!, subcatCode!, catName!, subCatName!,categoryImage!)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        subCategoryDS.reloadData(currentSubcategories, 0, false)
        table.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = SelectCategoryViewModel(AppDelegate.container.resolve(SelectCategoryServiceProtocol.self)!)
        self.viewModel = viewModel
        viewModel.subCatDelegate = self
        viewModel.getSubCategory(catCode!)
        currentSubcategories = subCategories
        subCategoryDS = SelectSubcategoryDataSource(subCategories, 0, false)
        table.dataSource = subCategoryDS
        backBtnLbl.text = catName?.uppercased() ?? "CATEGORIA"
        table.isEmptyRowsHidden = true
    }
}

class SubcategoryCell:UITableViewCell{
    @IBOutlet weak var subCategory: UILabel!
    @IBOutlet weak var hexagonoImage: UIImageView!
    override func prepareForReuse() {
    }
}
